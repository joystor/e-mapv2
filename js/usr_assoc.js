var tblUserAssoc = undefined;
var tblUserAssocLoc = undefined;
var tblUserAssocAcc = undefined;

var tblListAcc = undefined;
var tblListLocs = undefined;

/*Account*/
function fAddUserAsocAccount(){
    if( tblListAcc == undefined ){
        tblListAcc = makeDatTable('tblUsrAccAccoList',
                (function(d){
                    d.tb = "gtAccounts";
                }));
    }else{
        tblListAcc.fnDraw();
    }
    $('#divUsrAssoListAcco').fadeIn();
}
function fAddUserAsocDelAccount(){
    bootbox.confirm("Are you sure to delete this User association?", function(result) {
        if(result == true){
            var dat = getDTselected(tblUserAssocAcc);
            if( dat.length == 0){
                return false;
            }
            var id = dat[0];
            $.post("bk/sUsr.php",
                {   func:"uDelAsoc",
                    idB: id,
                    idA: getDTselected(tblUserAssoc)[6],
                    ty:"acc"
                },function(j){
                    tblUserAssocAcc.fnDraw();
            });
        }
    }); 
}

/*LOCATION*/
function fAddUserAsocLocation(){
    if( tblListLocs == undefined ){
        tblListLocs = makeDatTable('tblUsrAccLocsList',
                (function(d){
                    d.tb = "gtAdmOnlLocs";
                }));
    }else{
        tblListLocs.fnDraw();
    }
    $('#divUsrAssoListLocs').fadeIn();
}
function fAddUserAsocDelLocation(){
    bootbox.confirm("Are you sure to delete this Location association?", function(result) {
        if(result == true){
            var dat = getDTselected(tblUserAssocLoc);
            if( dat.length == 0){
                return false;
            }
            var id = dat[1];
            $.post("bk/sUsr.php",
                {   func:"uDelAsoc",
                    idB: id,
                    idA: getDTselected(tblUserAssoc)[6],
                    ty:"loc"
                },function(j){
                    tblUserAssocLoc.fnDraw();
            });
        }
    }); 
}

$().ready(function(){
    $('#modUsrAssoc').on('shown.bs.modal', function() {
        $("#divUserAssocs").hide();
        if(tblUserAssoc == undefined){
            tblUserAssoc = makeDatTable('tblUserAssoc',
                (function(d){
                    d.tb = "gtUsrAssoc";
                }));
            //TBL User click
            $('#tblUserAssoc tbody').on( 'click', 'tr', function () {
                var dat = getDTselected(tblUserAssoc);
                if(dat.length > 1){
                    $("#divUserAssocs").fadeIn();


                    //TBL LOCS
                    //TBL LOCS
                    //TBL LOCS
                    if(tblUserAssocLoc==undefined){
                        tblUserAssocLoc = makeDatTable('tblUserAssocLoc',
                                        (function(d){
                                            d.tb = "gtUsrAssocLoc";
                                            d.idU = function(){ return getDTselected(tblUserAssoc)[6]};
                                        }));
                        //ON CLICK
                        $('#tblUserAssocLoc tbody').on( 'click', 'tr', function () {
                            var dLoc = getDTselected(tblUserAssocLoc);
                        });
                    }else{
                        tblUserAssocLoc.fnDraw();
                    }



                    //TBL ACC
                    //TBL ACC
                    //TBL ACC
                    if(tblUserAssocAcc==undefined){
                        tblUserAssocAcc = makeDatTable('tblUserAssocAcc',
                                        (function(d){
                                            d.tb = "gtUsrAssocAcc";
                                            d.idU = function(){ return getDTselected(tblUserAssoc)[6]};
                                        }));
                        //ON CLICK
                        $('#tblUserAssocAcc tbody').on( 'click', 'tr', function () {
                            var dAcc = getDTselected(tblUserAssocAcc);
                        });
                    }else{
                        tblUserAssocAcc.fnDraw();
                    }
                }else{
                    $("#divUserAssocs").fadeOut();
                }
            });
        }else{
            tblUserAssoc.fnDraw();
        }
    });

    $("#btnAddUsrAsocLoc").on("click",function(){
        var dat = getDTselected(tblListLocs);
        if( dat.length == 0){
            return false;
        }
        var id = dat[1];
        $.post("bk/sUsr.php",
            {   func:"uAsoc",
                idB: id,
                idA: getDTselected(tblUserAssoc)[6],
                ty:"loc"
            },function(j){
                tblUserAssocLoc.fnDraw();
                $('#divUsrAssoListLocs').fadeOut();
        });
    });
    $("#btnAddUsrAsocAcc").on("click",function(){
        var dat = getDTselected(tblListAcc);
        if( dat.length == 0){
            return false;
        }
        var id = dat[0];
        $.post("bk/sUsr.php",
            {   func:"uAsoc",
                idB: id,
                idA: getDTselected(tblUserAssoc)[6],
                ty:"acc"
            },function(j){
                tblUserAssocAcc.fnDraw();
                $('#divUsrAssoListAcco').fadeOut();
        });
    });

});