<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
    
$fc = isset($_POST['fc'])?$_POST['fc']:"";

if($fc == ""){
    echo json_encode(array("res" => "NoParms"));
    exit(0);
}

include_once('cDBCon.php');
include_once('cfgVars.php');
include_once('sMs.php');
include_once('sMm.php');
include_once('intFunctions.php');

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

switch ($fc){
    /*#################################################################*/
    case "recMl": //Sent to mail User and password
            $ms = $_POST['ms'];
            $q = "SELECT U.user, U.f_name, U.l_name ".
                " FROM ".$VR->tbl_prefix."user_contact UC LEFT JOIN ".$VR->tbl_prefix."users U ON U.id_user=UC.id_user ".
                " WHERE UC.user_mail='".$ms."' or UC.personal_mail='".$ms."'";
            $rs = $DB->query( $q );
            $finds = 0;
            while( $row = mysqli_fetch_assoc($rs) ){
                $sub = "ELGMAP Account";
                $mes = "Dear: <strong>" . $row['f_name'] ." ".$row['l_name']."</strong><br>".
                    " Here is your account information:<br>".
                    " user:<strong>".$row['user']."</strong><br>".
                    "";
                sendMail( $ms, $row['f_name'] ." ".$row['l_name'] , $sub, $mes );
                $finds++;
            }
            if($finds == 0){
                $arr['res'] = "noFound";
            }else{
                $arr['res'] = "snd2Mail";
            }
            echo json_encode( $arr );
            exit(0);
        break;
    /*#################################################################*/
    case "chkMl": //Check if Mail exist
            $ms = $_POST['ms'];
            $q = "SELECT U.user, U.f_name, U.l_name ".
                " FROM ".$VR->tbl_prefix."user_contact UC LEFT JOIN ".$VR->tbl_prefix."users U ON U.id_user=UC.id_user ".
                " WHERE UC.user_mail='".$ms."' or UC.personal_mail='".$ms."'";
            $rs = $DB->query( $q );
            $finds = 0;
            while( $row = mysqli_fetch_assoc($rs) ){
                $finds++;
            }
            if($finds == 0){
                $arr['res'] = "noFound";
            }else{
                $arr['res'] = "Found";
            }
            echo json_encode( $arr );
            exit(0);
        break;
    /*#################################################################*/
    case "chgPw": //Change password
            $ms = $_POST['ms'];
            $pw = $_POST['pw'];
            
            
            $q = "SELECT U.id_user, U.user, U.f_name, U.l_name ".
                " FROM ".$VR->tbl_prefix."user_contact UC LEFT JOIN ".$VR->tbl_prefix."users U ON U.id_user=UC.id_user ".
                " WHERE UC.user_mail='".$ms."' or UC.personal_mail='".$ms."'";
            $rs = $DB->query( $q );
            $row1 = mysqli_fetch_assoc($rs);

            
            $q = "Update ".
                " ".$VR->tbl_prefix."users ".
                " set pass='".$pw."' ".
                " WHERE id_user=".$row1['id_user'];
            $rs = $DB->query( $q );

            
            $sub = "ELGMAP Password reset";
            $mes = "Dear: <strong>" . $row1['f_name'] ." ".$row1['l_name']."</strong><br>".
                    " Your Password was reset";
            sendMail( $ms, $row1['f_name'] ." ".$row1['l_name'] , $sub, $mes );
            
            $arr['res'] = "ready";
            
            echo json_encode( $arr );
            exit(0);
        break;
    /*#################################################################*/
    case "nwAcc": //Add new Account
            $ms = $_POST['ms'];
            $us = $_POST['uS'];
            $pw = $_POST['pw'];
            $uF = $_POST['uN'];
            $ul = $_POST['lN'];
            $us = strtolower($us);
            
            /*Username exist*/
            $rs  = $DB->query( "select count(*) as cc from ".$VR->tbl_prefix."users where user='".$us."'" );
            $row = mysqli_fetch_assoc($rs);
            if( $row['cc'] != "0"){
                echo json_encode( array("res" => "usrExist".$row['cc']) );
                exit(0);
            }
            /*Mail exist*/
            $rs  = $DB->query( "select count(*) as cc from ".$VR->tbl_prefix."user_contact where user_mail='".$ms."' or personal_mail='".$ms."'" );
            $row = mysqli_fetch_assoc($rs);
            if( $row['cc'] != "0"){
                echo json_encode( array("res" => "mailExist") );
                exit(0);
            }
            
            
            /*Account*/
            $rs  = $DB->query( "select (count(*)+1) as cu, DATE_FORMAT(NOW(),'%Y%m%d') as dd from ".$VR->tbl_prefix."accounts where substr(id_account,1,8)=DATE_FORMAT(NOW(),'%Y%m%d')" );
            $row = mysqli_fetch_assoc($rs);
            $conse = sprintf("%04d", $row['cu']);
            $dd = sprintf("%04d", $row['dd']);
            $ins = "insert into ".$VR->tbl_prefix."accounts(id_account, name, d_creation, d_update, user_creation) " .
                   " values( concat( '$dd', '-', '$conse'), '".$uF ." ".$uL".', now(), now(), 'NEW' );";
            $rs  = $DB->query( $ins );
            $acc = "$dd-$conse";
            
            /*User*/
            $in_U = "insert into " . $VR->tbl_prefix . "users ("."
                `id_account`, `user`, f_name, l_name, `d_creation`, `pass`, id_u_type) ".
                " VALUES ('".$acc."', '".$us."', '".$uF."', '".$uL."', now(), '".$pw."', 'NoREG' );";
            $rs = $DB->query( $in_U );
            $rs = $DB->query( 
                "select id_user from " . $VR->tbl_prefix . "users ".
                " where id_account='".$acc."' and user='".$us."'" );
            $id = "";
            while( $row = mysqli_fetch_assoc($rs) ) {
                $id = $row['id_user'];
                $in_UA = "insert into " . $VR->tbl_prefix . "user_address (`id_user`) VALUES ('$id');";
                $in_UC = "insert into " . $VR->tbl_prefix . "user_contact (`id_user`) VALUES ('$id');";
                $rs = $DB->query( $in_UA );
                $rs = $DB->query( $in_UC );
            }
            
            $sub = "ELGMAP Registration";
            $mes = "Dear: <strong>" . $uF ." ".$uL."</strong><br>".
                    " Your Registration on ELGMAP is almost ready please confirm your registration with the next link".
                    "";
            sendMail( $ms, $row1['f_name'] ." ".$row1['l_name'] , $sub, $mes );
            
            
            echo json_encode( array("res"=>"good") );
            exit(0);
        break;
    /*#################################################################*/
    default:
            echo json_encode(array("res" => "NoParms"));
            exit(0);
        break;
}

?>
