function showUserCFG(){

	$.post("bk/gtNot.php",{f1:"gtUcfg"},function(json){
		v = json.rows[0];
		var sndMail = "";
		var sndSMS = "";
		if(v.send_mail==1){
			sndMail = "checked";
		}
		if(v.send_sms==1){
			sndSMS = "checked";
		}
		var html ='<div role="form">'+
				  '<div class="checkbox">' +
				  '  <label>' +
				  '    <input type="checkbox" id="sndMail" '+sndMail+'> Send mail notifications' +
				  '  </label>' +
				  '</div>';
		html+='<div class="checkbox">' +
			  '  <label>' +
			  '    <input type="checkbox" id="sndSMS" '+sndSMS+'> Send SMS notifications' +
			  '  </label>' +
			  '</div>'+
			  '</div>';
		bootbox.dialog({
			message: html,
			title: "Configurations",
			buttons: {
				success: {
					label: "Save!",
					className: "btn-success",
					callback: function() {
						$.post("bk/stNot.php",{
									f1:"sCFG",
									mail:$("#sndMail").is(":checked")==true?1:0,
									sms:$("#sndSMS").is(":checked")==true?1:0,
								},function(json){
								});
						bootbox.hideAll();
					}
				},
				danger: {
					label: "Close!",
					className: "btn-danger",
					callback: function() {
						bootbox.hideAll();
					}
				}
			}
		});
	});
}


function readImage2DOC(input, Delem) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#'+Delem).attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}


$().ready(function () {
	
    
    $("#inAcPhoUsr").change(function(){
        readImage2DOC(this, "photUser");
    });
});