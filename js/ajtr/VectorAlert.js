

OpenLayers.Layer.VectorAlert = OpenLayers.Class(OpenLayers.Layer.Vector, {
	IntervalMS: 80,
	
	animatePoint: function(point, color, maxRadius, maxMilSec){
		/*
		var p = new OpenLayers.Geometry.Point( lonlat.lon, lonlat.lat );*/
		var feature = new OpenLayers.Feature.Vector(
			point, {
					maxRadius: maxRadius,
					actualRadius: 0,
					maxMilliSeconds: maxMilSec, 
					actualMilliSeconds: 0,
					color : color,
					strokeWidth: 1,
					interval: undefined
				});
				
		
		this.addFeatures([ feature ]);
				
		feature.data.interval = window.setInterval( function(){
			animate( feature );
		}, feature.layer.IntervalMS );

		var animate = function( feature )
		{
			feature.data.actualRadius += 1;
			feature.data.actualMilliSeconds += feature.layer.IntervalMS;
			feature.data.strokeWidth += 1;

			feature.style = {
				pointRadius: feature.data.actualRadius,  // I will change only the size of the feature
				fillColor: "#ffcc66",
				fillOpacity: 0,
				strokeColor: feature.data.color,
				strokeWidth: 2, //feature.data.strokeWidth,
				graphicZIndex: 1
			};
			
			if(feature.data.strokeWidth > 5){
				feature.data.strokeWidth = 1;
			}

		    feature.layer.redraw();
			if(feature.data.actualRadius == feature.data.maxRadius){
				feature.data.actualRadius = 0;
				//console.info("clear");
				//window.clearInterval(interval);
			}
			
			if(feature.data.maxMilliSeconds < feature.data.actualMilliSeconds ){
				window.clearInterval( feature.data.interval );
				feature.layer.removeFeatures([ feature ]);
			}
		}
		
	},
	
	CLASS_NAME: "OpenLayers.Layer.VectorAlert"
});