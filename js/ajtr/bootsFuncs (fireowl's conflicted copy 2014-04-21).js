function createLabLab(labName, labVal1, labVal2){
	var tCont = "";
	tCont += '<div class="row"><div class="col-md-12">';
	tCont += ' <div class="form-group">';
	tCont += '  <label class="col-md-4 control-label">' + labVal1 + '</label>';
	tCont += '  <div class="col-md-6">'
	tCont += '   <label class="control-label">'+labVal2+'</label>';
	tCont += '  </div>';
	tCont += ' </div>';
	tCont += '</div></div>';
	return tCont;
}


function createLabInp(labName, labVal, labInp){
	var tCont = "";
	tCont += '<div class="row"><div class="col-md-12">';
	tCont += ' <div class="form-group">';
	tCont += '  <label class="col-md-4 control-label" for="inp' + labName + '">' + labVal + '</label>';
	tCont += '  <div class="col-md-6">'
	tCont += '   <input class="form-control" id="inp' + labName + '" value="'+labInp+'"/>';
	tCont += '  </div>';
	tCont += ' </div>';
	tCont += '</div></div>';
	return tCont;
}

function createLabInpColor(labName, labVal, labInp){
	var tCont = "";
	tCont += '<div class="row"><div class="col-md-12">';
	tCont += ' <div class="form-group">';
	tCont += '  <label class="col-md-4 control-label" for="inp' + labName + '">' + labVal + '</label>';
	tCont += '  <div class="col-md-6">'
	tCont += '   <input class="form-control inpBootColorPick" id="inp' + labName + '" value="'+labInp+'"/>';
	tCont += '  </div>';
	tCont += ' </div>';
	tCont += '</div></div>';
	return tCont;
}