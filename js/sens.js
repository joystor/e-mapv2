function addNewSensor(){
    $("#modAdmAccounts").modal("hide");
	$("#btnSensDo").show();
    $("#divInsSensName").show();
    $("#btnSensUpd").hide();
    $("#divShowSensName").hide();
    $("#SensACCOUNT").html("Account: new Account");
    $("#SensSubACCOUNT").html("Sub Account: new");
    $("#SensSTATUS").html("State: new");
    $("#aTabLocName").html("New location");
    var inp = $("#modSensDial").find('[id^="InpSenT"]');
    $.each(inp, function(i,v){
        $(v).val("");
    });
    $('[href="#sTLocINf"]').trigger("click");
    $('[data-target="cnLILa"]').trigger("click");
}


function loadSensInModal(oVal){
    $("#btnSensDo").hide();
    $("#divInsSensName").hide();
    $("#btnSensUpd").show();
    $("#divShowSensName").show();
    $('#modSensDial').modal('show');
    $.each( oVal, function( key, value ) {
        if(typeof value === 'object' ){
            //console.log( key + ": " + value );
            $.each( value, function( k, v ) {
                //console.log("InpSenT"+key+"_"+k+" v:"+v);
                $("#InpSenT"+key+"_"+k).val(v);
            });
        }
    });
    $("#aTabLocName").html( oVal.L.accname );
    $("#SensACCOUNT").val( oVal.L.id_account );
    $("#aTabLocName").html(oVal.L.location_name);
    $("#InpSenTL_id_location").val( oVal.id );
    $("#LocCODE").val(oVal.id);
    sTout=setTimeout(function(){
        $("#InpSenTLA_state").val(oVal.LA.state);
        $("#InpSenTLA_state").trigger( "click" );
        sTout2=setTimeout(function(){
            $("#InpSenTLA_city").val(oVal.LA.city);
        }, 3000);
    }, 2000);
    
    sMapInf=setTimeout(function(){
        $("#InpSenTL_id_icon_type").val(oVal.L.id_icon_type);
    }, 2000);
    $('[href="#sTLocINf"]').trigger("click");
    $('[data-target="cnLILa"]').trigger("click");
}




$().ready(function(){
    /*
    NAV MENU
    */
    $("a[class~='ActivDeActivShow']").on("click", function(event){
        event.preventDefault();
        var p = $(this).parent().parent();
        var li = p.find("li");
        $.each(li, function(i, v){
            if($(li).hasClass("active")){
                $(li).removeClass("active");
            }
        });
        $(this).parent().addClass("active");
        var t = $(this).data("target");
        $("div[id^=cnL").hide();
        $("#"+t).show();
    });

    /*$("input[id^='InpSen']").editable({
        mode: "inline",
    });*/
    
    
    $("#btnSensDo").on("click",function(){
        var inp = $("#modSensDial").find('[id^="InpSenT"]');
        var urlP = "";
        $.each(inp, function(i,v){
            urlP += $(v).attr("id")+"="+$(v).val()+"&";
        });
        urlP += "&func=ins";
        $.post("bk/sSens.php",urlP,function(json){
            getLocationsSens();
            //console.log("resp:" + json);
            $('#modSensDial').modal('hide');
        });
    });

    $("#btnSensUpd").on("click",function(){
        var inp = $("#modSensDial").find('[id^="InpSenT"]');
        var urlP = "";
        $.each(inp, function(i,v){
            urlP += $(v).attr("id")+"="+$(v).val()+"&";
        });
        urlP += "&func=upd&acode="+$("#LocCODE").val();
        $.post("bk/sSens.php",urlP,function(json){
            getLocationsSens();
            //console.log("resp:" + json);
            $('#modSensDial').modal('hide');
        });
    });


    $("#btnGetClickMap").on("click",function(){
        MAPVARS.statusGetClickOnMap = true;
        MAPVARS.coordClickMap = "";
        $('#modSensDial').modal('hide');
    });



    $.post("bk/funs.php",{pG:"gStates"}, function(data){
        $("#InpSenTLA_state").html(data);
    },"html");
    
    /*$.post("bk/funs.php",{pG:"gMapFlgs"}, function(data){
        $("#InpSenTL_id_icon_type").html(data);
    },"html");*/

    $("#InpSenTLA_state").on("change", function(){
        $.post("bk/funs.php",{
                pG:"gCities",
                state: $("#InpSenTLA_state").val()
            }, function(data){
            $("#InpSenTLA_city").html(data);
        },"html");
    });
});



var NotifHistory = undefined;

function loadNotsHistory( type ){
    // alert notif
    $.post("bk/gtNot.php",{
            f1:"gtNotifHISTOR",
            lid: $("#InpSenTL_id_location").val()
        }, 
        function(j){
            //NotifHistory
            //console.log(j);
            var html = '<div class="table-responsive"><table class="table table-hover">';
            html += '<tr>'
                    +'<th>DATE</th>'
                    +'<th>MESSAGE</th>'
                    +'</tr>';
            $.each(j.rows, function(i, v){
                html += '<tr>'
                        +'<td>'+v.d_creation+'</td>'
                        +'<td>'+v.msgs+'</td>'
                        +'</tr>';
            });
            html += '</table></div>';
            if(type == "alert"){
                $("#divSecAlerts").html(html);
            }else if(type == "notif"){
                $("#divSecNotifs").html(html);
            }
            
        });
}
