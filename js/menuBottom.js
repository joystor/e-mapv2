var dataBtn1 = [
];

var menChart1;
var menChartALL;

var gChDOM1;
var gChDOMALL;
var gChDOMALL_DATA;

$().ready(function () {

    classie.remove( document.getElementById("cbpBottommenu"), 'cbp-spmenu-open' );
    $("#cbpBottommenu").show();

    
    
    if( $("#cbpLeftmenu").hasClass("cbp-spmenu-open") ){
		$("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width()-240)  +"px");
		$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "240px");
	}else{
		$("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width() )  +"px");
		$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "0px");
	}

    gChDOM1 = document.getElementById("chartLoc1").getContext("2d");
    gChDOMALL = document.getElementById("chartLocAll").getContext("2d");
    
    
    $.post("bk/eStat.php",{
            pG:"gStaAllLoc"
        },function(j){
            var d;
            if( j.rows.length == 0){
                d = {
                    alert:0, cnt_not:0, notif:0, warnings:0
                };
            }else{
                d = j.rows[0];
            }
            
            gChDOMALL_DATA = /*{
                labels: ["OK", "Alerts", "Notifications", "Warnings"],
                datasets: [
                    {
                        label: "All locations Status",
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [d.cnt_not, d.alert, d.notif, d.warnings]
                    }
                ]
            };*/
                        [
                        {
                            value: d.alert,
                            color:"#F7464A",
                            highlight: "#FF5A5E",
                            label: "Alerts"
                        },
                        {
                            value: d.cnt_not,
                            color: "#46BFBD",
                            highlight: "#5AD3D1",
                            label: "OK"
                        },
                        {
                            value: d.notif,
                            color: "#FDB45C",
                            highlight: "#FFC870",
                            label: "Notifications"
                        },
                        {
                            value: d.warnings,
                            color: "#FDB45C",
                            highlight: "#FFC870",
                            label: "Warnings"
                        }
                    ];
            $('#cbpBottommenu').css('height','200px');
            classie.add( document.getElementById("cbpBottommenu"), 'cbp-spmenu-open' );
            CreateChartALL();
        });


});







function loadFeatureStats(id){
    $.post("bk/eStat.php",{
            pG:"gStaLoc",
            loc: id
        },function(j){
            var d;
            if( j.rows.length == 0){
                d = {
                    alert:0, cnt_not:0, notif:0, warnings:0
                };
            }else{
                d = j.rows[0];
            }
            dataBtn1 = [
                        {
                            value: d.alert,
                            color:"#F7464A",
                            highlight: "#FF5A5E",
                            label: "Alerts"
                        },
                        {
                            value: d.cnt_not,
                            color: "#46BFBD",
                            highlight: "#5AD3D1",
                            label: "OK"
                        },
                        {
                            value: d.notif,
                            color: "#FDB45C",
                            highlight: "#FFC870",
                            label: "Notifications"
                        },
                        {
                            value: d.warnings,
                            color: "#FDB45C",
                            highlight: "#FFC870",
                            label: "Warnings"
                        }
                    ];
            CreateChart1();
        });
}

function createCOLOR(){
    return '#'+(Math.random().toString(16) + '000000').slice(2, 8);
}



function CreateChart1(){
	if(menChart1 !== undefined){
		menChart1.destroy();
		menChart1.chart.height = menChartALL.chart.height;
		menChart1.chart.width = menChartALL.chart.width;
	}
    menChart1 = new Chart(gChDOM1).PolarArea(dataBtn1, {
        //Boolean - Show a backdrop to the scale label
        scaleShowLabelBackdrop : true,
		//height: gChDOMALL.canvas.height,
		//width: gChDOMALL.canvas.width,
        //String - The colour of the label backdrop
        scaleBackdropColor : "rgba(255,255,255,0.75)",

        // Boolean - Whether the scale should begin at zero
        scaleBeginAtZero : true,

        //Number - The backdrop padding above & below the label in pixels
        scaleBackdropPaddingY : 2,

        //Number - The backdrop padding to the side of the label in pixels
        scaleBackdropPaddingX : 2,

        //Boolean - Show line for each value in the scale
        scaleShowLine : true,

        //Boolean - Stroke a line around each segment in the chart
        segmentShowStroke : true,

        //String - The colour of the stroke on each segement.
        segmentStrokeColor : "#fff",

        //Number - The width of the stroke value in pixels
        segmentStrokeWidth : 2,

        //Number - Amount of animation steps
        animationSteps : 100,

        //String - Animation easing effect.
        animationEasing : "easeOutBounce",

        //Boolean - Whether to animate the rotation of the chart
        animateRotate : true,

        //Boolean - Whether to animate scaling the chart from the centre
        animateScale : false,
        


        //String - A legend template
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    });
}






function CreateChartALL(){
    menChartALL = new Chart(gChDOMALL).PolarArea(gChDOMALL_DATA, {
        scaleShowLabelBackdrop : true,
        scaleBackdropColor : "rgba(255,255,255,0.75)",
        scaleBeginAtZero : true,
        scaleBackdropPaddingY : 2,
        scaleBackdropPaddingX : 2,
        scaleShowLine : true,
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 2,
        animationSteps : 100,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    });
}






var randomColorGeneator = function () { 
    return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
};
