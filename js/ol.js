var olMap;
var measureControls;
var csvData;
var popup;
var chaserCtrl;
var vLstations;
var vLAlerts;
var lastFound = "";
var lastFoundTitle = "";
var pNotifs = [];
var intervNotif;
var timeIntervalNotif = 100000;
var projWGS = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
var projSpM   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

var MAPVARS = {
    statusGetClickOnMap : false,
    coordClickMap : ""
};


function get_my_url(bounds) {
    var res = this.map.getResolution();
    var x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w));
    var y = Math.round((this.maxExtent.top - bounds.top) / (res * this.tileSize.h));
    var z = this.map.getZoom();

    var path = z + "/" + x + "/" + y + "." + this.type + "?" + parseInt(Math.random() * 9999);
    var url = this.url;
    if (url instanceof Array) {
        url = this.selectUrl(path, url);
    }
    return url + this.service + "/" + this.layername + "/" + path;
}

OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {                
                defaultHandlerOptions: {
                    'single': true,
                    'double': false,
                    'pixelTolerance': 0,
                    'stopSingle': false,
                    'stopDouble': false
                },

                initialize: function(options) {
                    this.handlerOptions = OpenLayers.Util.extend(
                        {}, this.defaultHandlerOptions
                    );
                    OpenLayers.Control.prototype.initialize.apply(
                        this, arguments
                    ); 
                    this.handler = new OpenLayers.Handler.Click(
                        this, {
                            'click': this.trigger
                        }, this.handlerOptions
                    );
                }, 

                trigger: function(e) {
                    var lonlat = olMap.getLonLatFromPixel(e.xy);
                    lonlat = lonlat.transform(
                                olMap.getProjectionObject(),
                                new OpenLayers.Projection("EPSG:4326")
                                );
                    if(MAPVARS.statusGetClickOnMap == true){
                        MAPVARS.coordClickMap = lonlat;
                        $('#modSensDial').modal('show');
                        $("#InpSenTLA_lat").val(lonlat.lat);
                        $("#InpSenTLA_lon").val(lonlat.lon);
                    }
                    /*alert("You clicked near " + lonlat.lat + " N, " +
                                              + lonlat.lon + " E");*/
                }

            });


function iniOLMap() {
    var maxExtent = new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508),
        restrictedExtent = maxExtent.clone(),
        maxResolution = 156543.0339;

    var options = {
        projection: new OpenLayers.Projection("EPSG:900913"),
        displayProjection: new OpenLayers.Projection("EPSG:4326"),
        units: "m",
        numZoomLevels: 18,
        maxResolution: maxResolution,
        //maxExtent: maxExtent,
        restrictedExtent: restrictedExtent,
        center: new OpenLayers.LonLat(-11075418.628867, 4772115.5292358),
        zoom: 5
    };
    olMap = new OpenLayers.Map();
    olMap.options = options;



    var OSM = new OpenLayers.Layer.OSM("OSM Map");
    var opencyclemap = new OpenLayers.Layer.XYZ(
        "opencyclemap",
        "http://a.tile3.opencyclemap.org/landscape/${z}/${x}/${y}.png",
        {
            numZoomLevels: 18, 
            sphericalMercator: true
        }
    );
	
	var tmsServers = ["http://mesonet.agron.iastate.edu/cache/tile.py/",
                      "http://mesonet1.agron.iastate.edu/cache/tile.py/",
                      "http://mesonet2.agron.iastate.edu/cache/tile.py/",
                      "http://mesonet3.agron.iastate.edu/cache/tile.py/",
                      "http://www.mesonet.agron.iastate.edu/cache/tile.py/"];

    var n0q = new OpenLayers.Layer.TMS(
        'NEXRAD Base Reflectivity',
        tmsServers,
        {
            layername: 'nexrad-n0q-900913',
            service: '1.0.0',
            type: 'png',
            visibility: false,
            getURL: get_my_url,
            isBaseLayer: false,
			transparent: 'true',
			opacity: 0.7
        }
    );

	var tm2Servers = ["http://mesonet.agron.iastate.edu/c/tile.py/",
                      "http://mesonet1.agron.iastate.edu/c/tile.py/",
                      "http://mesonet2.agron.iastate.edu/c/tile.py/",
                      "http://mesonet3.agron.iastate.edu/c/tile.py/",
                      "http://www.mesonet.agron.iastate.edu/c/tile.py/"];
    var states = new OpenLayers.Layer.TMS(
        'US State Borders',
        tm2Servers, {
            layername: 's-900913',
            service: '1.0.0',
            type: 'png',
            visibility: false,
            getURL: get_my_url,
            isBaseLayer: false
        }
    );
	

	
	var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
	
	

    mrkLayer = new OpenLayers.Layer.Markers("Markers", {
        displayInLayerSwitcher: false
    });
	
	
	
	var btnOLshowKeyMarks = new OpenLayers.Control.Button({displayClass: 'btnInfo ', 
			  type: OpenLayers.Control.TYPE_TOGGLE, 
			  eventListeners: {
				  'activate': function(){
					alet("active");
				  },
				  'deactivate': function(){
				    alet("deactive");
				  }
			  }}); 
	var panel = new OpenLayers.Control.Panel();
	panel.addControls([btnOLshowKeyMarks]);
	olMap.addControl(panel);
	
    /* 7b826f0da46c1313a7695e95fac960d3 */
    /*  imperial - Fahrenheit, metric - Celsius */

    var StyleWeather = new OpenLayers.Style({
            fontColor: "black",
            fontSize: "12px",
            fontFamily: "Arial, Courier New",
            labelAlign: "lt",
            labelXOffset: "-15",
            labelYOffset: "-17",
            labelOutlineColor: "white",
            labelOutlineWidth: 3,
            externalGraphic: "${icon}",
            graphicWidth: 60,
                    label : "${myCustomLabel}"
            },
            {
            context: 
            {
                icon:  function(feature) {
                    return "http://openweathermap.org/img/w/"+GetWeatherIconDay(feature.attributes.station);
                },
                    myCustomLabel:  function(feature) {
                        return  feature.attributes.station.main.temp.toFixed(1) + ' °F';
                    }
            }
            }
        );
	var oWSstations = new OpenLayers.Layer.Vector.OWMStations("Stations information", {units : 'imperial', styleMap: StyleWeather} );
	oWSstations.setVisibility(false); 
	var oWScity = new OpenLayers.Layer.Vector.OWMWeather("Current weather", {units : 'imperial', styleMap: StyleWeather});
	oWScity.setVisibility(true); 
    var oWPrecipitation = new OpenLayers.Layer.XYZ(
        "Precipitation forecasts",
        "https://${s}.tile.openweathermap.org/map/precipitation/${z}/${x}/${y}.png",
        {
            numZoomLevels: 19, 
            isBaseLayer: false,
            opacity: 0.6,
            sphericalMercator: true
        }
    );
    oWPrecipitation.setVisibility(false); 
	
	
	
	// OVI
	var oviStreet = new OpenLayers.Layer.XYZ("OVI Street", ["http://a.maptile.maps.svc.ovi.com/maptiler/maptile/newest/normal.day/${z}/${x}/${y}/256/png8",
										"http://b.maptile.maps.svc.ovi.com/maptiler/maptile/newest/normal.day/${z}/${x}/${y}/256/png8"], {
		transitionEffect: 'resize', sphericalMercator: true, numZoomLevels: 21
	});
	var oviTransit = new OpenLayers.Layer.XYZ("OVI Transit", ["http://c.maptile.maps.svc.ovi.com/maptiler/maptile/newest/normal.day.transit/${z}/${x}/${y}/256/png8",
										"http://d.maptile.maps.svc.ovi.com/maptiler/maptile/newest/normal.day.transit/${z}/${x}/${y}/256/png8"], {
		transitionEffect: 'resize', sphericalMercator: true, numZoomLevels: 21
	});
	var oviSatellite = new OpenLayers.Layer.XYZ("OVI Satellite", ["http://e.maptile.maps.svc.ovi.com/maptiler/maptile/newest/hybrid.day/${z}/${x}/${y}/256/png8",
										   "http://f.maptile.maps.svc.ovi.com/maptiler/maptile/newest/hybrid.day/${z}/${x}/${y}/256/png8"],
		{
		transitionEffect: 'resize', sphericalMercator: true, numZoomLevels: 21
	});
	//   oviStreet, oviTransit, oviSatellite
	
	
	//Mapquest
	var mqStreet = new OpenLayers.Layer.XYZ(
            "MapQuest Street", 
            [
                "http://otile1.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png",
                "http://otile2.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png",
                "http://otile3.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png",
                "http://otile4.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png"
            ],
            {
                attribution: "Data, imagery and map information provided by <a href='http://www.mapquest.com/'  target='_blank'>MapQuest</a>, <a href='http://www.openstreetmap.org/' target='_blank'>Open Street Map</a> and contributors, <a href='http://creativecommons.org/licenses/by-sa/2.0/' target='_blank'>CC-BY-SA</a>  <img src='http://developer.mapquest.com/content/osm/mq_logo.png' border='0'>",
                transitionEffect: "resize"
            }
        );
	var mqImage = new OpenLayers.Layer.XYZ(
            "MapQuest Imagery",
            [
                "http://otile1.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png",
                "http://otile2.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png",
                "http://otile3.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png",
                "http://otile4.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png"
            ],
            {
                attribution: "Tiles Courtesy of <a href='http://open.mapquest.co.uk/' target='_blank'>MapQuest</a>. Portions Courtesy NASA/JPL-Caltech and U.S. Depart. of Agriculture, Farm Service Agency. <img src='http://developer.mapquest.com/content/osm/mq_logo.png' border='0'>",
                transitionEffect: "resize"
            }
        );
	// mqStreet, mqImage, 


    /*
    GEORSS
    */
    //var gRearthquakes = new OpenLayers.Layer.GeoRSS( "bk/olprxy.php", "grsLay=earthquakes");
    //gRearthquakes.setVisibility(false); 
    //map.addLayer(newl);
	
	/*
	Add Layers
	*/
    olMap.addLayers([
			OSM, opencyclemap, 
			oviStreet, oviTransit, oviSatellite, 
			mqStreet, mqImage, 
			states, /*n0q, */
			oWSstations, oWScity, oWPrecipitation,
            //gRearthquakes,
			mrkLayer]);
	
	
    var ls = new OpenLayers.Control.LayerSwitcher();
    olMap.addControl(ls);
	//Open layerswitcher
    //ls.maximizeControl();
    olMap.addControl(new OpenLayers.Control.MousePosition({
         displayProjection: new OpenLayers.Projection('EPSG:4326')
    }));
    //if (!olMap.getCenter()){ olMap.zoomToMaxExtent(); }
    olMap.setCenter(new OpenLayers.LonLat(-11075418.628867, 4772115.5292358), 5);
    
	
	/*#######################################################################################
	Set Cluster Context
	#######################################################################################*/
    var context = {
        getColor: function(feature){
            var color = '#0400FF';
            if (feature.attributes.score && feature.attributes.score === 100) {
                color = '#00FF11';
            } else if (feature.attributes.score && feature.attributes.score !== 100) {
                color = '#FF0000';
            }else if(feature.cluster) {
				if(feature.cluster.length==1){
					color = "#"+feature.cluster[0].attributes.oVal.color;
				}else{
					var withAlert = false;
					for (var i = 0; i < feature.cluster.length; i++) {
						if (!withAlert && feature.cluster[i].attributes.score !== 100) {
							withAlert = true;
						}
					}
					if (withAlert === true) {
						color = '#ee0000';
					}
				}
            }
            return color;
        },
		getRadius: function(feature) {
			var radius = 13;
			if(feature.cluster) {
				if(feature.cluster.length ==1){
					radius = 14;
				}else if(feature.cluster.length <=10){
					radius = 15;
				}else if(feature.cluster.length > 10 && feature.cluster.length <= 50){
					radius = 16;
				}else if(feature.cluster.length > 50){
					radius = 17;
				}
            }
			return radius; //Math.min(feature.attributes.count, 7) + 3;
		},
		getLabel: function(feature){
			var label = "";
			if (feature.attributes.score && feature.attributes.score !== 100) {
                label = feature.attributes.score;
            }/*else if(feature.cluster) {
				var withAlert = 0;
                for (var i = 0; i < feature.cluster.length; i++) {
                    if (!withAlert && feature.cluster[i].attributes.score !== 100) {
                        withAlert = feature.cluster[i].attributes.score;
                    }
                }
                if (withAlert !== 0) {
                    label = withAlert;
                }
			}*/
			return label;
		},
        getICON: function(feature){
            var dL = "";
            if(feature.cluster !== undefined){
                if(feature.cluster.length>=1){
                    dL =  feature.cluster[0].attributes.oVal.L;
                }else{
                    dL =  feature.cluster.attributes.oVal.L;
                }
            }else{
                dL = feature.attributes.oVal.L;
            }
            switch(dL.id_type_of_site){
                case "Tear":
                        return "img/marks/red_tear.png";
                    break;
                case "Cross":
                        return "img/marks/t_cross.png";
                    break;
                case "GasPump":
                        return "img/marks/t_gas.png";
                    break;
                case "Tower":
                        return "img/marks/t_tower.png";
                    break;
                case "Building":
                        return "img/marks/t_buildings.png";
                    break;
            }
        }
    };
    
    var stylemap = new OpenLayers.StyleMap({
        'default': new OpenLayers.Style({
            //pointRadius: "${getRadius}",
            /*fillColor: "${getColor}",
            fillOpacity: 0.9,
            strokeColor: "#666666",
            strokeWidth: 1,
            strokeOpacity: 1,
            graphicZIndex: 2,
			*/
            // IMAGE
            //'pointRadius': 20,
            'externalGraphic': '${getICON}',
            graphicWidth: 28, 
            graphicHeight: 28,
            graphicYOffset: -30,
            
			label: "${getLabel}",
			//fontFamily: "Courier New, monospace",
			//fontWeight: "bold",
			//labelAlign: "cm",
			labelXOffset: "28",
			//labelYOffset: "-1",
			labelOutlineColor: "white",
			labelOutlineWidth: 3
        }, {
            context: context
        }),
        'select' : new OpenLayers.Style({
            //pointRadius: "${getRadius}",
			fillColor: "#ffff00",
            fillOpacity: 1,
            strokeColor: "#ff0000",
            strokeWidth: 2,
            strokeOpacity: 1,
			
			//label: "${count}",
			labelOutlineWidth: 0,
            fontColor: "#000000",
            //fontOpacity: 0.8,
            fontSize: "12px",
			labelXOffset: "0",
			labelYOffset: "0",
			graphicZIndex: 1
        }, {
            context: context
        })
    });
    
    
    	/*##################################################################
	Layer Alerts
	##################################################################*/
	vLAlerts = new OpenLayers.Layer.VectorAlert("Alerts", {
		displayInLayerSwitcher: false
	});
	olMap.addLayers([ vLAlerts ]);
	vLAlerts.IntervalMS = 20;
    
    
    var strategies = [];
    strategies.push(new OpenLayers.Strategy.AttributeCluster({
                    attribute:'oVal.L.file_image'
                }));
                
    vLstations = new OpenLayers.Layer.Vector(
        'Stations', 
        	{
        		styleMap: stylemap, 
        		//renderers: renderer, //['Canvas','SVG'],
        		strategies: [] //strategies
        	});
    
    var ctrSelStations = new OpenLayers.Control.SelectFeature(
        vLstations, 
		{
			hover: true,
			clickFeature: function(feature){
				onClickFeature(feature);
			}
		}
    );
    olMap.addControl(ctrSelStations);
    ctrSelStations.activate();

    olMap.addLayers([vLstations]);

    var click = new OpenLayers.Control.Click();
    olMap.addControl(click);
    click.activate();
	

    getLocationsSens();








    /* TOOLS */
    /* TOOLS */
    /* TOOLS */

    var sketchSymbolizersMeas = {
        "Point": {
            pointRadius: 4,
            graphicName: "square",
            fillColor: "white",
            fillOpacity: 1,
            strokeWidth: 1,
            strokeOpacity: 1,
            strokeColor: "#333333"
        },
        "Line": {
            strokeWidth: 3,
            strokeOpacity: 1,
            strokeColor: "#666666",
            strokeDashstyle: "dash"
        },
        "Polygon": {
            strokeWidth: 2,
            strokeOpacity: 1,
            strokeColor: "#666666",
            fillColor: "white",
            fillOpacity: 0.3
        }
    };
    var styleMeas = new OpenLayers.Style();
    styleMeas.addRules([
        new OpenLayers.Rule({symbolizer: sketchSymbolizersMeas})
    ]);
    var styleMapMeas = new OpenLayers.StyleMap({"default": styleMeas});
    
    // allow testing of specific renderers via "?renderer=Canvas", etc
    var rendererMeas = OpenLayers.Util.getParameters(window.location.href).renderer;
    rendererMeas = (rendererMeas) ? [rendererMeas] : OpenLayers.Layer.Vector.prototype.renderers;

    measureControls = {
            line: new OpenLayers.Control.Measure(
                OpenLayers.Handler.Path, {
                    persist: true,
                    handlerOptions: {
                        layerOptions: {
                            renderers: rendererMeas,
                            styleMap: styleMapMeas
                        }
                    }
                }
            ),
            polygon: new OpenLayers.Control.Measure(
                OpenLayers.Handler.Polygon, {
                    persist: true,
                    handlerOptions: {
                        layerOptions: {
                            renderers: rendererMeas,
                            styleMap: styleMapMeas
                        }
                    }
                }
            )
        };
        
        var controlMeas;
        for(var key in measureControls) {
            controlMeas = measureControls[key];
            controlMeas.events.on({
                "measure": handleMeasurements,
                "measurepartial": handleMeasurements
            });
            olMap.addControl(controlMeas);
        }
    /* TOOLS */
    /* TOOLS */
    
    

    $("div[id^=OpenLayers_Control_Attribution]").hide();

	/*olMap.events.register("moveend", olMap, function(){
		console.log("Map moved");
	});
	olMap.events.register("zoomend", olMap, function(){
		console.log("Map zoomed");
	});*/
}
function handleMeasurements(event) {
    var geometry = event.geometry;
    var units = event.units;
    var order = event.order;
    var measure = event.measure;
    var element = document.getElementById('inpValMeasure');
    var out = "";
    if(order == 1) {
        out += "" + measure.toFixed(3) + " " + units;
    } else {
        out += "" + measure.toFixed(3) + " " + units + "<sup>2</" + "sup>";
    }
    element.innerHTML = out;
    //element.value = out;
}
function toggleControl(tool) {
    for(key in measureControls) {
        var control = measureControls[key];
        if(tool == key) {
            document.getElementById('inpValMeasure').innerHTML = "";
            control.activate();
            if( !$("#divMeasuDial").is(":visible") ){
                $("#divMeasuDial").slideDown();
                //$("#divMeasuDial").draggable();
            }
        } else {
            control.deactivate();
        }
    }

}

function getLocationsSens(){
    $.post("bk/gtNot.php",{"f1":"gtSens"}, function(j){
    //$.post("http://joys.bl.ee/work/Rugged/bk/gtNot.php",{"f1":"gtSens"}, function(j){
        if(j){
            features = [];
            $.each(j.rows, function(i,r){
                if( typeof r.LA === "undefined" ){
                    return true;
                }
                //console.log("id:"+r.L.id_account);
                //console.log("lon:"+r.LA);
                var lonlat = 
                    new OpenLayers.LonLat( parseFloat(r.LA.lon), parseFloat(r.LA.lat) )
                        .transform(
                                new OpenLayers.Projection("EPSG:4326"), 
                                olMap.getProjectionObject()
                                );
                var p = new OpenLayers.Geometry.Point( lonlat.lon, lonlat.lat );
                r['color'] = "0038ff";
                var f = new OpenLayers.Feature.Vector(
                    p, {
                            id: r.id,
                            score: parseFloat(100), 
                            oVal : r,
                            layer: vLstations
                        });
                features.push(f);
            });
            vLstations.destroyFeatures();
            vLstations.addFeatures(features);
            if(vLstations.features.length > 0){
                setTimeout(function(){
                    olMap.zoomToExtent(vLstations.getDataExtent());
                }, 2500);
            }
            setMapLayersMenu();
        }
        
        /*##################################################################
        NOTIFICATIONS
        ##################################################################*/
        intervNotif = setInterval(function(){callReadNotif()}, timeIntervalNotif );
        callReadNotif();
    });
}

function setMapLayersMenu(){
	var laysList = "";
	var firstBase = true;
	var firstLaye = true;
	for(i=0;i<olMap.layers.length;i++){
		var l = olMap.layers[i];
		var visb = "olMap.layers["+i+"].setVisibility( !olMap.layers["+i+"].getVisibility())";
		var chek = "";
		if(l.getVisibility()){
			chek = "checked";
		}
		if(l.isBaseLayer && l.displayInLayerSwitcher){
			if(firstBase){
				firstBase = false;
				laysList += '<strong>Base Layer</strong><br>';
			}
			laysList += '<input type="radio" name="baseMapLayer" onclick="'+visb+'" '+chek+'> '+l.name+'<br>';
		}else if(l.displayInLayerSwitcher){
			if(firstLaye){
				firstLaye = false;
				laysList += '<li class="divider"></li><strong>Overlays</strong><br>';
			}
			laysList += '<input type="checkbox" onclick="'+visb+'" '+chek+'> '+l.name+'<br>';
		}
	}
	$("#lstMapLayers").append( laysList );
}


/*##################################################################
##################################################################
##################################################################
##################################################################
NOTIFICATIONS START
##################################################################*/
function callReadNotif(){
	if( vLAlerts.features.length == 0 ){
		$.post("bk/gtNot.php",{f1:"gtNotif"},function(j){
			if(j.rows){
				pNotifs = [];
				$.each(j.rows,function(i,v){
                    if( v.codes.indexOf("SS1") == -1 ){
                        var message = "<p>";
                        $.each( v.msgs.split(','), function(i, v){
                            message += v+"<br>";
                        } );
                        message += "</p>";
                        var notiType = "";
                        if(v.codes.indexOf("SS3") > -1){ //warning
                            notiType = "warning";
                        }else if(v.codes.indexOf("SS2") > -1){ //"notification"
                            notiType = "notification";
                        }else if(v.codes.indexOf("SS4") > -1){ //"alert"
                            notiType = "alert";
                        }
                        v['message'] = message;
                        v['notification_type'] = notiType;
                        v['account_number'] = v.id_loc_account;
                        pNotifs.push(v);
                    }
				});
				readNotifs();
			}
		});
		//window.clearInterval(intervNotif);
	}
}
function callReadNotifNotMapAlerts(){
	$.post("bk/gtNot.php",{f1:"gtNotif"},function(j){
		if(j.rows){
			pNotifs = [];
			$.each(j.rows,function(i,v){
				pNotifs.push(v);
			});
			readNotifs();
		}
	});
}
function readNotifs(){
    var counter = {
            nNotif: 0,
            nAlert: 0
        }
	$("#listNotifs").html("");
	$("#listAlerts").html("");
	$.each(pNotifs, function(i,v){
		
        var lstVal = CreateListNotification(v, counter);
        if(lstVal == ""){
            return true;
        }
        var color = "";
        if(v.notification_type.toLowerCase() == "warning"){
            $("#listNotifs").append( lstVal );
            color = "#ffff00";
        }else if(v.notification_type.toLowerCase() == "notification"){
            $("#listNotifs").append( lstVal );
            color = "#0000ff";
        }else{
            $("#listAlerts").append( lstVal );
            color = "#ff0000";
        }


		if(v.user_confirm==0){
			var f = getFeature(v.id_account);
            if(f == undefined){
                return "";
            }else{

    			var p = f.geometry.getCentroid().transform( olMap.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));
    			vLAlerts.animatePoint( 
    			   new OpenLayers.Geometry.Point( p.x, p.y )
    						.transform( new OpenLayers.Projection("EPSG:4326"), 
    									olMap.getProjectionObject()
    									),
    			   color, 30, 8000);
            }
		}

	});
	$("#bdgNotif").html(counter.nNotif);
	$("#bdgAlert").html(counter.nAlert);
	$("#divWNotif").slideDown();
	$("#divWAlerts").slideDown();
	if( pNotifs.length > 0 ){
		classie.add( menuLeft, 'cbp-spmenu-open' );
	}
	//setTimeout(readNotifs(),9000);
}

function CreateListNotification(v, counter){
    var color = "#ff0000";
		
    var t = v.d_creation.split(/[- :]/);
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    d = d.toDateString() + " " + d.toLocaleTimeString();
    
    var feat = getFeature( v.id_loc_account );
    if(feat == undefined){
        return "";
    }

    var S = feat.data.oVal;
    
    /*
    SS1=OK
    SS2=Notification
    SS3=Warnign
    SS4=Alert
    */
    var ty = "";
    if(v.codes.indexOf("SS3") > -1 && v.status_web == "0"){ //warning
        ty = "list-group-item-warning";
        /*if(v.user_confirm==1){
            ty ="";
        }else{
            counter.nNotif++;
        }*/
        counter.nNotif++;
    }else if(v.codes.indexOf("SS2") > -1 && v.status_web == "0"){ //"notification"
        ty = "list-group-item-info";
        /*if(v.user_confirm==1){
            ty ="";
        }else{
            counter.nNotif++;
        }*/
        counter.nNotif++;
    }else if(v.codes.indexOf("SS4") > -1 && v.status_web == "0"){ //"alert"
        ty = "list-group-item-danger";
        /*if(v.user_confirm==1){
            ty ="";
        }else{
            counter.nAlert++;
        }*/
        counter.nAlert++;
    }else{
        return false;
    }
    
    if(v.status_web != "0"){
        ty = "list-group-item-success";
    }


    var lstVal = '<a href="#" class="list-group-item '+ty+'" data-idnot="'+v.id_sensor_status+'" onclick="openNotif(\''+v.id_loc_account+'\','+v.id_sensor_status+')">'+
                 '<span class="dateListNotif">'+d+'</span>' +
                 '<span class="stateListNotif">Status <span style="color:#0000FF">open</span></span>' +
                 '<span class="msgListTitle"><strong>'+S.L.location_name+'</strong></span><br/>' +
                 '<span class="msgListNotif">'+v.msgs+'</span></a>';
    return lstVal;

}

function openNotif(id, idNot){
	var f = getFeature(id);
    if(f == undefined){
        return "";
    }
	$('#cbpBottommenu').css('height','200px');
    classie.add( document.getElementById("cbpBottommenu"), 'cbp-spmenu-open' );

    var pLonLat = new OpenLayers.LonLat( f.geometry.x , f.geometry.y ); //.transform( projWGS, projSpM );
	olMap.setCenter( pLonLat , 18 );
	var p = f.geometry.getCentroid().transform( olMap.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));
	vLAlerts.animatePoint( 
		   new OpenLayers.Geometry.Point( p.x, p.y )
					.transform( new OpenLayers.Projection("EPSG:4326"), 
								olMap.getProjectionObject()
								),
		   "#00ff00", 20, 2000);
	var v = getNotificationByID(id);
    loadFeatureStats(id);
    $.post("bk/stNot.php",{
					f1: "stSSStusWB",
					id: idNot,
					st: 1
				},function(json){
                    var a = $("#cbpLeftmenu").find("a[data-idnot='"+idNot+"']")[0];
                    //console.log(a);
                    $(a).removeClass("list-group-item-warning");
                    $(a).removeClass("list-group-item-info");
                    $(a).removeClass("list-group-item-danger");
                    $(a).addClass("list-group-item-success");
					//callReadNotifNotMapAlerts();
				});
	/*if( v.user_confirm==0){ //v.send_mail==0 && v.send_sms==0 &&
		
	}*/
	addPopup(f);
}

var popups = {}; // to be able to handle them later 
function addPopup(feature) {
	var html = getHtmlContent(feature); // handle the content in a separate function.
	var popupId = feature.geometry.x + "," + feature.geometry.y; 
	var popup = popups[popupId];
	if (!popup || !popup.map) {
		popup = new OpenLayers.Popup.FramedCloud(
					popupId,
					new OpenLayers.LonLat( feature.geometry.x , feature.geometry.y ),
					new OpenLayers.Size(450,300),
					html,
					null,
					true,
					function(evt) {
						delete popups[this.id];
						this.hide();
						OpenLayers.Event.stop(evt);
					});
		var offset = {'size':new OpenLayers.Size(0,0),'offset':new OpenLayers.Pixel(5,-5)};
		popup.anchor = offset;
		popup.calculateRelativePosition = function () {
			return 'tr'; //bl
		}
		popup.autoSize = true;
		popup.useInlineStyles = false;
		popups[popupId] = popup;
		vLstations.map.addPopup(popup, true);
	}
	//popup.setContentHTML(popup.contentHTML + html);
	popup.show();
}
function getHtmlContent(feature){
	var d = feature.data.oVal;
	var id = feature.data.id;
	var html = "<h4>"+d.L.location_name+"</h4>";
	html += '<hr class="divider">';
	var v = getNotificationByID(id);
    var t;
    var d;
    var status = ""
    if(v==undefined){
        v={
            d_creation:"",
            message:"whitout notifications to show"
        };
        d = new Date();
    }else{
        t = v.d_creation.split(/[- :]/);
        d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
        status = "open";
    }

    d = d.toDateString() + " " + d.toLocaleTimeString();
	html += "<p class='text-right' style='font-size: 11px;'>"+d+"</p>";
	html += "<p>Status: <span style='color:#0000FF'>"+status+"</span></p>";
	html += "<p style='color:#000000'>"+v.message+"</p>";
	html += '<hr class="divider">';
	/*if((v.send_mail==1 || v.send_sms==1) && (v.user_confirm!=1)){
		html += '<button class="btn btn-primary" onclick="openPinDial('+id+')">Enter code</button>';
	}*/

    html += '<button class="btn btn-primary" onclick=""><span class="glyphicon glyphicon-envelope"></span> Send mail</button>';
    html += '<a onclick="loadSensInModal(getFeature(\''+id+'\').data.oVal);" href="#" class="btn btn-warning btn-circle" style="position: absolute; bottom: 10px; right: 10px;"><span class="glyphicon glyphicon-exclamation-sign"></span></a>';

	return html;
}
function openPinDial(id){
	var v = getNotificationByID(id);
	var html = 
			createLabLab( "msg"+id, "Message:", v.message)+
			createLabLab( "cd"+id, "Date:", v.creation_date)+
			createLabInp( "pinID", "Insert Pin code:", "");
	bootbox.dialog({
		message: html,
		title: "Insert code",
		buttons: {
			success: {
				label: "Save!",
				className: "btn-success",
				callback: function() {
					if(v.code == $("#inppinID").val()){
						$.post("bk/stNot.php",{
									f1:"stPinCOD",
									id:v.id_notification,
									pin: $("#inppinID").val()
								},function(json){
									bootbox.alert("<h4>Code is saved</h4>");
									callReadNotifNotMapAlerts();
								});
						bootbox.hideAll();
					}else{
						bootbox.alert("<h4>The code is not correct<h4>");
					}
				}
			},
			danger: {
				label: "Close!",
				className: "btn-danger",
				callback: function() {
					bootbox.hideAll();
				}
			}
		}
	});
}
function getNotificationByID(id){
	var notif = undefined;
	$.each(pNotifs, function(i,v){
		if(v.id_account == id || v.account_number == id){
			notif = v;
			return false;
		}
	});
	return notif;
}
/*##################################################################
NOTIFICATIONS END
##################################################################*/



/*##################################################################
CLICK FEATURE
##################################################################*/
function onClickFeature( featureCluster ){
	var feature;
	if(featureCluster.cluster){
		var html = "";
		if(featureCluster.cluster.length > 1){
			bootbox.alert( "Are many features in this zoom");
		}else{
			feature = featureCluster.cluster[0];
            var fID = feature.data.id;
            var oVal = feature.data.oVal;
            //loadSensInModal(oVal);
            addPopup(feature);
		}
		
	}else{
        feature = featureCluster;
        var fID = feature.data.id;
        var oVal = feature.data.oVal;
        //loadSensInModal(oVal);
        addPopup(feature);
    }
    //classie.remove( menuLeft, 'cbp-spmenu-open' );
    //classie.toggle( document.getElementById( 'cbpBottommenu' ), 'cbp-spmenu-open' );
    $('#cbpBottommenu').css('height','200px');
    classie.add( document.getElementById( 'cbpBottommenu' ), 'cbp-spmenu-open' );	
    if( $("#cbpLeftmenu" ).hasClass("cbp-spmenu-open") ){
		$("#cbpBottommenu").css("left","240px");
	}else{
		$("#cbpBottommenu").css("left","0px");
	}
    loadFeatureStats(feature.data.oVal.id);
}


function getFeature(id){
	for(var f=0; f < vLstations.features.length; f++){
		var feature = vLstations.features[f];
        if(feature.cluster !== undefined){
            for (var i = 0; i < feature.cluster.length; i++) {
                if ( feature.cluster[i].attributes.id == id) {
                    return feature.cluster[i];
                }
            }
        }else if ( feature.attributes.id == id || feature.attributes.oVal.L.id_account == id) {
            return feature;
        }
	}
}




function addAlertMrk(id){
	$("#nAidS").val(id);
	$("#divNA").modal("show");
}



function updateTime() {
  $.ajax({
    type: 'POST',
    url: 'bk/srvTime.php',
    //timeout: 1000,
    success: function(data) {
      $("#timeSRV").html(data); 
      window.setTimeout( updateTime, 60000);
    }
  });
}
