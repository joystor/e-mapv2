var acNum = "";
var acLoc = "";
var acMac = "";
var acLat = "";
var acLon = "";
var acDate = "";
var acTime = "";

var smPS = "";
var smSI = "";
var smSG = "";
var smGI = "";

$().ready(function() {
    $("a[data-code^='SM']").on("click", function(event) {
        var gr = $(this).data("gr");
        var code = $(this).data("code");
        event.preventDefault();
        var p = $(this).parent().parent();
        var li = p.find("li");
        $.each(li, function(i, v) {
            if ($(li).hasClass("active")) {
                $(li).removeClass("active");
            }
        });
        $(this).parent().addClass("active");
        switch(gr){
            case "PS":
                smPS = code;
            break;
            case "SI":
                smSI = code;
            break;
            case "SG":
                smSG = code;
            break;
            case "GI":
                smGI = code;
            break;
        }
    });

    $('#inSTdate').datepicker();
    $('#inSTtime').timepicker({
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false
    });


    $.post("bk/funs.php", {
        pG: "gLocations"
    }, function(data) {
        $("#InpSenTLA_state").html(data);
    }, "html");


    //Sensor ID
    $("#inpM2acc").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "bk/funs.php",
                dataType: "json",
                type:"POST",
                data: {
                    pG: "gAcc",
                    q: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            //console.log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
            acNum = ui.item.id;
            buildCODE();
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    //Location Name
    $("#inpM2loc").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "bk/funs.php",
                dataType: "json",
                type:"POST",
                data: {
                    pG: "gLoc",
                    ac: acNum,
                    q: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            acLat = ui.item.lat;
            acLon = ui.item.lon;
            acLoc = ui.item.id;
            $("#inpMlat").val( acLat );
            $("#inpMlon").val( acLon );
            buildCODE();
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    $("#inMMac").on("change", function(){
        acMac = $("#inMMac").val();
        buildCODE();
    });

    $("#inSTdate").on("change", function(){
        acDate = $("#inSTdate").val();
        buildCODE();
    });
    $("#inSTtime").on("change", function(){
        acTime = $("#inSTtime").val();
        buildCODE();
    });

});


function buildCODE(){
/*    var code = 
acNum
acLoc
acMac
acLat
acLon
acDate
acTime

smPS
smSI
smSG
smGI*/
}