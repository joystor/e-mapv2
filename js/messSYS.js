
$().ready(function(){
    
    $("#selMSysIds").on("change",function(){
        var opc = $(this).find("option:selected");
        $("#lblMSysVARS").val( $(opc).data("variables") );
        $("#inpMSysTITLE").val( $(opc).data("title") );
        $("#inpMSysMSG").val( $(opc).data("msg") );
    });
    
    $("#btnMessSysSave").on("click",function(e){
        e.preventDefault();
        $.post("bk/funs.php",{
                pG:"sMSGSYS",
                id: $("#selMSysIds").val(),
                msg: $("#inpMSysMSG").val(),
                tet: $("#inpMSysTITLE").val()
            },function(j){
                bootbox.alert("Message saved")
                $("#modMessSYS").modal("hide");
            });
    });
});



function fOpenMESSSYSS(){
    $.post("bk/funs.php",{
            pG: "gMSGSYS"
        },function(j){
            $("#selMSysIds").val("");
            $("#lblMSysVARS").html("");
            $("#inpMSysTITLE").val("");
            $("#inpMSysMSG").val("");
            var opc = "<option value=''>Select one...</option>"
            $("#selMSysIds").html( opc );
            $.each(j.rows, function(i, v){
                var dat = ' data-id_mess_sys="'+v.id_mess_sys+'" ';
                dat += ' data-title="'+v.title+'" ';
                dat += ' data-msg="'+v.msg+'" ';
                dat += ' data-variables="'+v.variables+'" ';
                opc = "<option "+dat+" value='"+v.id_mess_sys+"'>"+v.id_mess_sys+"</option>";
                $("#selMSysIds").append( opc );
            });
        });
}
