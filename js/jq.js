 arNotif = [];
var menuLeft = document.getElementById( 'cbpLeftmenu' );
var autocompleteGG;


$.fn.dataTableExt.sErrMode = 'throw';

$().ready(function () {
	
	iniOLMap();

	if(oUsr.isAdmin){
		$("#btnSimulInsertAlert").show();
	}
	
	$("#divWNotif").dialog({
		autoOpen: false,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		}
	});
	
	
	$("#imgLoading").hide();
    
	
	$("#inpFindLoc").on("keypress",function(e){
		if($("#inpFindLoc").val().length >= 3){
			if(e.keyCode == 13){
				$("#btnFindIt").click();
			}
		}
	});
	
	$("#showLastFound").on("click",function(){
		bootbox.dialog({
					message: lastFound,
					title: lastFoundTitle
		});
	});
	
	
	/*###################################################################
	Busquedas en Nominatim
	#####################################################################*/
	$("#btnFindIt").on("click", function(){
		var q = {
				"q":$("#inpFindLoc").val(),
				"format":"json",
				"limit":"10",
				"polygon_text":"1",
				"countrycodes":"us"
			};
		$("#imgLoading").fadeIn();
		$.get("http://nominatim.openstreetmap.org/search/",q,function(json){
				$("#imgLoading").fadeOut();
                if(json){
    				//console.log(json);
                    var html ="<ul class='nav nav-pills nav-stacked'>";
                    $.each(json, function(i,v){
                        //var zom2 = "olMap.setCenter(new OpenLayers.LonLat("+v.lon+","+v.lat+").transform('EPSG:4326', 'EPSG:900913'),12);";
                        var zom2 = 'olMap.zoomToExtent(new OpenLayers.Bounds('+v.boundingbox[2]+','+v.boundingbox[0]+','+v.boundingbox[3]+','+v.boundingbox[1]+').transform(\'EPSG:4326\', \'EPSG:900913\'));';
						zom2 += 'bootbox.hideAll();';
                        html += "<li onclick=\""+zom2+"\"><a href='#'>"+v.display_name+"</a></li>";
                    });
                    html += "</ul>";
					lastFound = html;
					lastFoundTitle = "Search \"" + $("#inpFindLoc").val() + "\"";
					
					$("#ulLastSearch").fadeIn();
                    bootbox.dialog({
						message: lastFound,
						title: lastFoundTitle
						/*,
						buttons: {
							success: {
								label: "Success!",
								className: "btn-success",
								callback: function() {
									Example.show("great success");
								}
							},
							danger: {
								label: "Danger!",
								className: "btn-danger",
								callback: function() {
									Example.show("uh oh, look out!");
								}
							},
							main: {
								label: "Click ME!",
								className: "btn-primary",
								callback: function() {
									Example.show("Primary button");
								}
							}
						}*/
					});
                }
			});
		return false;
	});


	/*###################################################################
	Busquedas en GOOGLE
	#####################################################################*/
	autocompleteGG = new google.maps.places.Autocomplete(
		(document.getElementById('inpFindLocGG')),
		{
			//types: ['(regions)'],
			componentRestrictions:  { 'country': 'US' }
		});
	google.maps.event.addListener(autocompleteGG, 'place_changed', onPlaceChanged);

	$('.dropdown-menu.notClose').click(function(e) {
        e.stopPropagation();
    });
	
	
	
	$("#lnk2Notif").on("click", function(){
        if( $("#secMLtAlt").is(":visible") ){
            $("#secMLtAlt").hide();
            $("#dividLmenu").hide();
            $("#cbpLeftmenu").css( "bottom", "0px");
            
        }/*else{
            $("#secMLtAlt").show();
            $("#dividLmenu").show();
            $("#cbpLeftmenu").css( "bottom", "0px");
            $("#listNotifs").css("height","40%");
        }*/
        $("#secMLtNot").fadeIn();
        $("#listNotifs").css("height","");
        
		/*if( $("#divWNotif").is(":visible") ){
			$("#divWNotif").slideUp();
		}else{
			$("#divWAlerts").slideUp("fast",function(){
				$("#divWNotif").slideDown();
			});
		}*/
		//$("#divWNotif").slideDown();
		classie.add( menuLeft, 'cbp-spmenu-open' );
		if( $("#cbpLeftmenu").hasClass("cbp-spmenu-open") ){
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width()-240)  +"px");
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "240px");
		}
		//classie.toggle( menuLeft, 'cbp-spmenu-open' );
	});
	$("#lnk2Alerts").on("click", function(){
        if( $("#secMLtNot").is(":visible") ){
            $("#secMLtNot").hide();
            $("#dividLmenu").hide();
            $("#cbpLeftmenu").css( "bottom", "0px");
        }/*else{
            $("#secMLtNot").show();
            $("#dividLmenu").show();
            $("#cbpLeftmenu").css( "bottom", "0px");
            $("#listAlerts").css("height","40%");
        }*/
        $("#secMLtAlt").fadeIn();
         $("#listAlerts").css("height","");
        
        
		/*if( $("#divWAlerts").is(":visible") ){
			$("#divWAlerts").slideUp();
		}else{
			$("#divWNotif").slideUp("fast",function(){
				$("#divWAlerts").slideDown();
			});
		}*/
		//$("#divWAlerts").slideDown();
		classie.add( menuLeft, 'cbp-spmenu-open' );
		if( $("#cbpLeftmenu").hasClass("cbp-spmenu-open") ){
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width()-240)  +"px");
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "240px");
		}
		//classie.toggle( menuLeft, 'cbp-spmenu-open' );
	});
	
	$(".divWClose").on("click", function(e){
		//console.log(e);
		$("#"+e.target.parentElement.id).slideUp();
	});
	
	
	
	/*var AlertsEvents=setInterval(function(){
		var id = Math.floor(Math.random() * 100) + 1;
		var f = getFeature(id);
		var p = f.geometry.getCentroid().transform( olMap.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));
		vLAlerts.animatePoint( 
		   new OpenLayers.Geometry.Point( p.x, p.y )
					.transform( new OpenLayers.Projection("EPSG:4326"), 
								olMap.getProjectionObject()
								),
		   "#ff0000", 30, 8000);
		olMap.zoomToExtent(vLAlerts.getDataExtent());
	},4000);*/
	
	
	$("#btnNAsave").on("click",function(){
		$.post("bk/stNot.php",
			{ f1:"stNeAl",
			  idS:$("#nAidS").val(),
			  sMe:$("#inpNAmess").val(),
			  sTy:$("#selNAmess").val()
			},
			function(j){
				//console.log(j);
			});
	});
	
	
	$("#hideLeftMenuA").on("click", function(){
		classie.toggle( menuLeft, 'cbp-spmenu-open' );
		if( ! $("#cbpLeftmenu").hasClass("cbp-spmenu-open") ){
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width())  +"px");
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "0px");
			
		}
	});
	$("#hideLeftMenuN").on("click", function(){
		classie.toggle( menuLeft, 'cbp-spmenu-open' );
		if( ! $("#cbpLeftmenu").hasClass("cbp-spmenu-open") ){
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width())  +"px");
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "0px");
		}
	});
	
	$("#divWAlerts").draggable({ containment: "parent" });
	$( "#divWAlerts" ).resizable();
	//$("#divWNotif").draggable({ containment: "parent" });
	//$( "#divWNotif" ).resizable();
	


	
	$( window ).resize(function() {
		relocateLOGO();
	});
	relocateLOGO();
	
	updateTime();
    
    
    
    // my account
    $.post("bk/funs.php",{pG:"gUserTypes"}, function(data){
        $("#inAcU_id_u_type").html(data);
    },"html");
    if(oUsr.isAdmin == false){
    	$("#inAcU_id_u_type").prop("disabled",true);
    }



    /*
    SEARCH DRAGABLE
    */
	$("#divSearch").draggable({
		handle: ".modal-header"
	});
	$("#inpFindLocGG").on("click",function(){
		$("#inpFindLocGG").val("");
	});
    
    
    
    
    
    /*
     * SHOW WINDOW
     * */
    $('input[name=rdWView]').on("change",function(){
        var c = $(this).is(":checked");
        var w = $(this).val();
        if( w == "alert" ){
            if( $("#cbpLeftmenu" ).hasClass("cbp-spmenu-open") && $("#secMLtAlt").is(":visible")){
                classie.remove( document.getElementById( 'cbpLeftmenu' ), 'cbp-spmenu-open' );
            }else{
                $("#lnk2Alerts").trigger("click");
            }
        }else if( w == "notif" ){
            if( $("#cbpLeftmenu" ).hasClass("cbp-spmenu-open") && $("#secMLtNot").is(":visible")){
                classie.remove( document.getElementById( 'cbpLeftmenu' ), 'cbp-spmenu-open' );
            }else{
                $("#lnk2Notif").trigger("click");
            }
        }else if( w == "chart" ){
            if(c){
                classie.add( document.getElementById( 'cbpBottommenu' ), 'cbp-spmenu-open' );
                $('#cbpBottommenu').css('height','200px');
            }else{
                classie.remove( document.getElementById( 'cbpBottommenu' ), 'cbp-spmenu-open' );
                $('#cbpBottommenu').css('height','');
            }
        }

        if( $("#cbpLeftmenu").hasClass("cbp-spmenu-open") ){
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width()-240)  +"px");
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "240px");
		}else{
            $("#cbpBottommenu.cbp-spmenu-horizontal").css("width", ($(document).width())  +"px");
			$("#cbpBottommenu.cbp-spmenu-horizontal").css("left", "0px");
        }
    });
    
    
    /*
     * FILTER LOCS
     * */
    $('input[name=rdPermLoc]').on("change",function(){
        var isChk = $(this).is(":checked");
        var opt = $(this).val();
        for(var f=0; f < vLstations.features.length; f++){
            var feature = vLstations.features[f];
            var type = feature.data.oVal.L.id_icon_type;
            if( type== opt){
                if(isChk){
                    feature.style = undefined;
                }else{
                    //feature.style = { visibility: 'hidden' };
                    //feature.style = { fillOpacity:1, strokeOpacity:1 };
                    feature.style = { display:'none' };
                    /*feature.style.fillOpacity = 1;
                    feature.style.strokeOpacity = 1;*/
                }
            }
        }
        vLstations.redraw();
    });

    
});



/*###################################################################
	Busquedas en GOOGLE
	#####################################################################*/
function onPlaceChanged() {
	var place = autocompleteGG.getPlace();
	var loc = place.geometry.location;
	var lat = place.geometry.location.lat();
	var lon = place.geometry.location.lng();
	olMap.setCenter(
			new OpenLayers.LonLat( lon, lat)
				.transform(
		            new OpenLayers.Projection("EPSG:4326"), 
		            olMap.getProjectionObject()
		            )
			, 16);
	$('#menDroSearch').removeClass("open");
}


function relocateLOGO(){
	var w = $( window ).width();
	if(w>=1590){
		$( "#logoLeft" ).hide();
		$( ".LogoText" ).show();
		$( ".LogoFirstLetterR" ).show();
		$( ".LogoTXT" ).css("font-size", "28px");
		$( ".LogoFirstLetterR" ).css("font-size", "34px");
	}else if(w < 1590 && w > 1531){
		$( "#logoLeft" ).hide();
		$( ".LogoText" ).show();
		$( ".LogoFirstLetterR" ).show();
		$( ".LogoTXT" ).css("font-size", "20px");
		$( ".LogoFirstLetterR" ).css("font-size", "28px");
	}else if( w <= 1531 && w > 1259){
		$( "#logoLeft" ).hide();
		$( ".LogoText" ).hide();
		$( ".LogoFirstLetterR" ).show();
		$( ".LogoFirstLetterR" ).css("font-size", "28px");
	}else if( w <= 1259){
		$( "#logoLeft" ).show();
		$( ".LogoText" ).hide();
		$( ".LogoFirstLetterR" ).hide();
	}
	var wL = $( ".LogoTXT" ).width();
	w = w / 2;
	wL = wL / 2;
	w = w - wL;
	$( ".LogoTXT" ).css("margin-left", w+"px");
	$( ".LogoTXT" ).fadeIn();

	var h = $( window ).height();
	var h2 = ((h-60)/2)-60;
	$("#listAlerts").css("height",h2);
	$("#listNotifs").css("height",h2);

	if( $("#navMenusTop").height() > 60){
		$("#cbpLeftmenu").css("top",107);
	}else{
		$("#cbpLeftmenu").css("top",57)
	}
}




function makeDatTable(idTable, datFunc){
	if($('#'+idTable).length == 0){
		return false;
	}
    var oTab = $('#'+idTable).dataTable({
    			//"sPaginationType": "four_button",
            	"processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "bk/gtDT2.php",
                    "type": "POST",
                    "data": datFunc
                },
        });
    $('#'+idTable).addClass("table");
    $('#'+idTable).addClass("table-condensed");
    $('#'+idTable+' tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            $('#'+idTable+' tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    return oTab;
}




function getDTselected(table){
    if(table.DataTable().row('.selected').length > 0){
        return table.DataTable().row('.selected').data();
    }else{
        return [];
    }
}



function modifyMyAccountInfo(){
	var inp = $("#modAccount").find('[id^="inAcU"]');
    $.each(inp, function(i,v){
        $(v).val("");
        $(v).removeAttr("checked");
    });
    //$("#UserGrpPhoto").hide();
    $.post("bk/funs.php",
        {  "pG" : "gUsrAllInfo",
            "id": oUsr.id
        },function(j){
            $.each( j.rows.U[0], function(i,v){
                $("#inAcU_"+i).val(v);
                if($("#inAcU_"+i).is("input[type='checkbox']")){
                	if(v == "1" || v =="true" || v=="t"){
                		$("#inAcU_"+i).prop("checked", true);
                	}else{
                		$("#inAcU_"+i).prop("checked", false);
                	}
                }
            });
            if(j.rows.UA != null){
                $.each( j.rows.UA[0], function(i,v){
                    $("#inAcUA_"+i).val(v);
                    if($("#inAcUA_"+i).is("input[type='checkbox']")){
	                	if(v == "1" || v =="true" || v=="t"){
	                		$("#inAcUA_"+i).prop("checked", true);
	                	}else{
	                		$("#inAcUA_"+i).prop("checked", false);
	                	}
	                }
                });
            }
            if(j.rows.UC != null){
                $.each( j.rows.UC[0], function(i,v){
                    $("#inAcUC_"+i).val(v);
                    if($("#inAcC_"+i).is("input[type='checkbox']")){
	                	if(v == "1" || v =="true" || v=="t"){
	                		$("#inAcC_"+i).prop("checked", true);
	                	}else{
	                		$("#inAcC_"+i).prop("checked", false);
	                	}
	                }
                });
            }
            if(oUsr.isAdmin == false){
		    	$("#inAcU_id_u_type").prop("disabled",true);
		    }
            $("#inAcCompName").val( j.rows.U[0].company_name );
            //$("#inAcCompName").val(oUsr.accName);
            $("#isUpdorIns").val("myAcc");
    		$("#idAccUsr").val( oUsr.id );
    		$("#modAccount").modal("show");
        }
    );
}
