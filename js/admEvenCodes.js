function fLoadEventCodes(){
    $("#modAdmEvenCodes").modal("show");

    $("#lstEvenCodes").html("Loading...");
    $.post("bk/funs.php",{pG:"gEvtCods"},function(j){
        $("#lstEvenCodes").html("");
        //HEADERS
        var html ='<div href="#" class="list-group-item">'+
                    ' <div class ="form-horizontal" role="form" style="margin-bottom: -20px;">'+
                    ' <div class ="form-group">'+
                    '  <label class="col-sm-3 control-label">CODE:DESCRIPTION</label>'+
                    '  <div class="col-sm-3">'+
                    '   <label>ALIAS</label>'+
                    '  </div> '+
                    '  <div class="col-sm-3">'+
                    '    <label>TYPE OF NOTIFICATIONS</label>'+
                    '  </div>'+
                    '  <div  class="col-sm-2">'+
                    '   <label>TIME TO ACTION (mins)</label>'+
                    '  </div>'+
                    '  <div  class="col-sm-1">'+
                    '   <label></label>'+
                    '  </div>'+
                    ' </div>'+
                    '</div>'+
                    '</div>';
        $("#lstEvenCodes").append(html);
        $.each(j.rows,function(i, v){
            var dat = 'data-code="'+v.code+'"';
            dat += 'data-ev_mins="'+v.ev_mins+'"';
            dat += 'data-ev_type="'+v.ev_type+'"';
            var sel = 
                  '<select class="form-control datEvtCdSEL" id="selEvtCode-'+v.code+'">'+
                  '    <option value="none">None</option>'+
                  '    <option value="notification">Notification</option>'+
                  '    <option value="warning">Warning</option>'+
                  '    <option value="alert">Alert</option>'+
                  '</select>';
            html ='<a href="#" class="list-group-item" '+dat+'>'+
                    ' <div class ="form-horizontal" role="form" style="margin-bottom: -10px;">'+
                    ' <div class ="form-group">'+
                    '  <label class="col-sm-3 control-label datEvtCdCODE" data-code="'+v.code+'">'+v.code+':'+v.description+'</label>'+
                    '  <div class="col-sm-3">'+
                    '   <input class="form-control datEvtCdDESC" value="'+v.d_alias+'"></input>'+
                    '  </div> '+
                    '  <div class="col-sm-3">'+
                        sel+
                    '  </div>'+
                    '  <div  class="col-sm-2">'+
                    '   <input class="form-control datEvtCdMINS" placeholder="mins" value="'+v.ev_mins+'"></input>'+
                    '  </div>'+
                    '  <div  class="col-sm-1">'+
                    '   <button class="btn btn-success btnSaveEvnCode" placeholder="mins" value="'+v.ev_mins+'">'+
                    '      <span class="glyphicon glyphicon-floppy-saved"></span>'+
                    '   </button>'+
                    '  </div>'+
                    ' </div>'+
                    '</div>'+
                    '</a>';
            $("#lstEvenCodes").append(html);
            if(v.ev_type != ""){
                $("#selEvtCode-"+v.code).val(v.ev_type);
            }else{
                $("#selEvtCode-"+v.code).val("none");
            }
        });

        $(".btnSaveEvnCode").on("click", function(){
            var code = $( $(this).parent().parent().find(".datEvtCdCODE")[0]).data("code");
            var desc = $( $(this).parent().parent().find(".datEvtCdDESC")[0]).val();
            var even = $( $(this).parent().parent().find(".datEvtCdSEL")[0]).val();
            var mins = $( $(this).parent().parent().find(".datEvtCdMINS")[0]).val();
            $.post("bk/funs.php",{
                    pG: "uEvtCods",
                    cd: code,
                    de: desc,
                    ev: even,
                    mi: mins
                },function(j){
                    bootbox.alert(code +" code saved.");
                });
        });
    });

}