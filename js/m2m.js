var acNum = "";
var acLoc = "";
var acMac = "";
var acLat = "";
var acLon = "";
var acDate = "";
var acTime = "";

var smSS = "";
var smPrS = "";
var smPoS = "";
var smSeS = "";
var smTSG = "";
var smGeS = "";

$().ready(function() {
    $("a[data-gr]").on("click", function(event) {
        var gr = $(this).data("gr");
        var code = $(this).data("code");
        event.preventDefault();
        var p = $(this).parent().parent();
        var li = p.find("li");
        if( $(this).parent().hasClass("active") ){
            $(this).parent().toggleClass("active");
        }else{
            $.each(li, function(i, v) {
                if ($(li).hasClass("active")) {
                    $(li).removeClass("active");
                    $(li).find("input").remove();
                }
            });
            $(this).parent().addClass("active");
        }
        if( $(this).parent().hasClass("slcAddTXVal") ){
            if( $(this).parent().hasClass("active") ){
                if( $(this).parent().find("input").length == 0 ){
                    $(this).parent().append('<input type="number"></input>');
                    $( $(this).parent().find("input")[0] ).on("change",function(){
                        buildCODE();
                    });
                }
            }else{
                $(this).parent().find("input").remove();
            }
        }
        switch(gr){
            case "SS":
                smSS = code;
            break;
            case "PrS":
                smPrS = code;
            break;
            case "PoS":
                smPoS = code;
            break;
            case "SeS":
                smSeS = code;
            break;
            case "TSG":
                smTSG = code;
            break;
            case "GeS":
                smGeS = code;
            break;
        }
        buildCODE();
    });

    $('#inSTdate').datepicker("option", "dateFormat", "mmddccyy");
    $('#inSTtime').timepicker({
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false
    });


    $.post("bk/funs.php", {
        pG: "gLocations"
    }, function(data) {
        $("#InpSenTLA_state").html(data);
    }, "html");


    //Sensor ID
    $("#inSTtime").trigger("change");
    $.post("bk/funs.php",{pG:"gAcc",q:""}, function(j){
        var opc = "";
        opc += '<option value="">Select one ...</option>';
        $.each(j, function(i, v){
            opc += '<option value="'+v.id_account+'">'+v.id_account+" - "+v.name+'</option>';
        });
        $("#inpM2acc").html(opc);
    });

    $("#inpM2acc").on("change", function(){
        if( $(this).val() !=""){
            acNum = $(this).val();
            buildCODE();
            $.post("bk/funs.php",{
                    pG:"gLoc", 
                    ac:$(this).val(), 
                    q:""
                }, 
                function(j){
                    var opc = "";
                    opc += '<option value="">Select one ...</option>';
                    $.each(j, function(i, v){
                        opc += '<option value="'+v.account_number+'" data-lng="'+v.lon+'" data-lat="'+v.lat+'">'+
                                v.account_number+" - "+v.location_name+'</option>';
                    });
                    $("#inpM2loc").html(opc);
                });            
        }
    });

    $("#inpM2loc").on("change", function(){
        if( $(this).val() != "" ){
            opc = $(this).find(":selected")[0];
            acLat = $(opc).data("lat");
            acLon = $(opc).data("lng");
            acLoc = $(this).val();
            $("#inpMlat").val( $(opc).data("lat") );
            $("#inpMlon").val( $(opc).data("lng") );
            buildCODE();
        }
    });


    /*$("#inpM2acc").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "bk/funs.php",
                dataType: "json",
                type:"POST",
                data: {
                    pG: "gAcc",
                    q: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            //console.log(ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
            acNum = ui.item.id;
            buildCODE();
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });*/

    //Location Name
    /*$("#inpM2loc").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "bk/funs.php",
                dataType: "json",
                type:"POST",
                data: {
                    pG: "gLoc",
                    ac: acNum,
                    q: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            acLat = ui.item.lat;
            acLon = ui.item.lon;
            acLoc = ui.item.id;
            $("#inpMlat").val( acLat );
            $("#inpMlon").val( acLon );
            $("#inSTdate").val("");
            $("#inSTtime").val("");
            buildCODE();
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });*/

    $("#inMMac").on("change", function(){
        acMac = $("#inMMac").val();
        buildCODE();
    });

    $("#inSTdate").on("change", function(){
        acDate = $("#inSTdate").val();
        buildCODE();
    });
    $("#inSTtime").on("change", function(){
        acTime = $("#inSTtime").val();
        buildCODE();
    });


    $("#btnM2MClear").on("click", function(){
        var li = $("#divContMenus").find("li");
        $.each(li, function(i, v) {
            if ($(li).hasClass("active")) {
                $(li).removeClass("active");
            }
        });
        $("[id^='in']").val("");
        $("#mesGen").val("");
    });

    $("#btnM2MSend").on("click", function(){
        var msg = $("#mesGen").val() + ",";
        if( msg == "," || msg.indexOf(",,") > -1){
            bootbox.alert("Data is missgin please select one item of every group options");
            return false;
        }
        msg = $("#mesGen").val();
        $.post("bk/eSrv.php",{msg: msg},
                function(j){
                    if(j.res =="good"){
                        bootbox.alert("The String is send it");
                    }else{
                        bootbox.alert("err:"+j.res);
                    }

                });
    });

});


function buildCODE(){
    var code = 
            acNum+'-'+
            acLoc+','+
            acDate.replace("/","").replace("/","")+','+
            acTime+',';

            /*acMac+','+
            acLat+','+
            acLon+','+*/
            

            /*smSS+','+
            smPrS+','+
            smPoS+','+
            smSeS+','+
            smTSG+','+
            smGeS;*/
    //$("a[data-gr]").parent('[class*=active]').each(function(i,v){
    $("a[data-gr]").parent('[class*=active]').find("a[data-gr]").each(function(i,v){
        //if($(v).parent().hasClass("active")){
            var cd = $(v).data("code");
            if( $(this).parent().find("input").length > 0 ){
                cd += ":"+$( $(this).parent().find("input")[0] ).val();
            }
            code += cd+",";
        //}
    });
    code = code.substr(0, code.length-1);
    $("#mesGen").val(code);
}