<?php

include_once 'bkFuns.php';

$TSI = "";
$TLA = "";
$TLCI = "";
$TL = "";
foreach ($_POST as $pname => $pval) {
    if (isset($pname) && $pname != "" 
        && strpos($pname,'_') !== false 
        && strpos($pname,'InpSen') !== false 
        && $pval != "") 
    {
        list($pn1, $pn2) = explode('_', $pname, 2);
        if( isset($pn2) && $pn2 != "" ){
            if( $pn1 == "InpSenTL"){
                $TL .= " ".$pn2."='$pval',";
            }elseif ($pn1 == 'InpSenTLA'){
                $TLA .= " ".$pn2."='$pval',";
            }elseif ($pn1 == 'InpSenTSI'){
                $TSI .= " "." ".$pn2."='$pval',";
            }elseif ($pn1 == 'InpSenTLCI'){
                $TLCI .= " ".$pn2."='$pval',";
            }
        }
    }
}
$TSI = rtrim($TSI, ",");
$TLA = rtrim($TLA, ",");
$TL = rtrim($TL, ",");
$TLCI = rtrim($TLCI, ",");


include_once('cDBCon.php');
include_once('cfgVars.php');

/*#####################################
Delete
#####################################*/
if( isset($_POST['func']) && $_POST['func'] == "del" ) {
    $id = $_POST['id'];
    $rs = $DB->query("delete from  " . $VR->tbl_prefix . "location where account_number='$id'");
    $rs = $DB->query("delete from  " . $VR->tbl_prefix . "location_addres where account_number='$id'");
    $rs = $DB->query("delete from  " . $VR->tbl_prefix . "location_contact_info where account_number='$id'");
    
    $rs = $DB->query("select id_user from  " . $VR->tbl_prefix . "users where account_number='$id'");
    $row = mysqli_fetch_array($rs);
    if( isset($row['id_user']) {
        var $idU = $row['id_user'];
        $rs = $DB->query( "DELETE FROM " . $VR->tbl_prefix . "users WHERE id_user='$idU';");
        $rs = $DB->query( "DELETE FROM " . $VR->tbl_prefix . "user_address WHERE id_user='$idU';");
        $rs = $DB->query( "DELETE FROM " . $VR->tbl_prefix . "user_contact WHERE id_user='$idU';");
    }

    $r = array(
        "res:" => "good",
        "code" => ""
    );
    echo json_encode($r);
}

/*#####################################
INSERT
#####################################*/
if( isset($_POST['func']) && $_POST['func'] == "ins" ) {

    $acode = genAccoutCODE($_POST['InpSenTLA_state']);
    $exist = true;
    while($exist){
        $rs = $DB->query("select count(*) as cnt from  " . $VR->tbl_prefix . "location where account_number='$acode'");
        $row = mysqli_fetch_array($rs);
        if( $row['cnt'] == 0){
            $exist = false;
        }else{
            $acode = genAccoutCODE($_POST['InpSenTLA_state']);
        }
    }

    $in_SI = "insert into " . $VR->tbl_prefix . "sensor_info (`account_number`, `mfg_name`, `mfg_model_name`, `mfg_model_no`, `serv_type`, `serv_desc`, `com1_type`, `com1_service`, `com1_device_mac`, `com1_warning`, `com1_notes`, `com2_type`, `com2_service`, `com2_device_mac`, `com2_warning`, `com2_notes`, `battery_part_no`, `battery_voltage`, `phase_setting`, `phase_setting_1`, `sensor_1_premise_temp`, `sensor_2_premise_warning`, `sensor_3_premise_line_a_current`, `sensor_4_premise_line_b_current`, `sensor_5_premise_line_c_current`, `sensor_6_generator_on`, `sensor_7_gen_rotation_alert`, `sensor_8_gen_temp`, `sensor_9_gen_fuel_level`, `sensor_10_gen_oil_low_warning`, `sensor_11_gen_line_a_voltage`, `sensor_12_gen_line_b_voltage`, `sensor_13_gen_line_c_voltage`, `sensor_14_unit_temp`, `sensor_15_unit_door_open`, `sensor_16_unit_been_striked`, `sensor_17_unit_low_battery`) VALUES ('$acode', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');";
    $in_L = "insert into " . $VR->tbl_prefix . "location (`account_number`, `security_id`, `location_name`, `id_type_of_site`, `id_icon_type`, `buildpow_type`, `buildpow_size_amp`, `build_voltage`, `site_emergency_equipment`, `emergency_pow`, `emergency_pow_supply_to`, `pow_sup_co_name`, `pow_sup_co_address1`, `pow_sup_co_address2`, `pow_sup_co_city`, `pow_sup_co_state`, `pow_sup_co_zip`, `pow_sup_co_contact_phone`, `gen_located_on_site`, `gen_mfg`, `gen_model`, `gen_mfg_date`, `gen_size_kw`, `gen_engine_site`, `gen_fuel_type`, `gen_fuel_tank_unit`, `gen_fuel_tank_size`, `gen_fuel_tank_level`, `gen_fuel_last_date_filled`, `gen_last_date_inspection`, `gen_hours_ran_from_last_inspection`, `gen_total_run_hours`, `swt_gear_located_onsite`, `swt_gear_mfg`, `swt_gear_model`, `swt_gear_mgf_date`, `swt_gear_size_kw`, `gov_maindated`, `gen_serv_provider1`, `gen_serv_provider2`, `tech_serv_provider1`, `tech_serv_provider2`, `fuel_serv_provider1`, `fuel_serv_provider2`, `notif_location_manager1`, `notif_location_manager2`, `notif3`, `notif4`, `notif5`) VALUES ('$acode', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');";
    $in_LA = "insert into " . $VR->tbl_prefix . "location_addres (`account_number`, `address1`, `address2`, `city`, `state`, `zip`, `zip_4`, `country`, `lat`, `lon`) values ('$acode', '', '', '', '', '', '', '', '', '');";
    $in_LCI = "insert into " . $VR->tbl_prefix . "location_contact_info (`account_number`, `fname`, `lname`, `titlename`, `bus_tel_1`, `bus_tel_2`, `fax_1`, `fax_2`, `misc_1`, `misc_2`, `cell_1`, `cell_2`, `bus_mail`, `misc_mail`, `address1`, `address2`, `city`, `state`, `zip`, `zip_4`, `country`) VALUES ('$acode', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');";
    
    $rs = $DB->query( $in_L );
    $rs = $DB->query( $in_LA );
    $rs = $DB->query( $in_LCI );
    $rs = $DB->query( $in_SI );

    $rs = $DB->query( "update " . $VR->tbl_prefix . "sensor_info set " .$TSI." where account_number='$acode'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "location set " .$TL." where account_number='$acode'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "location_addres set " .$TLA." where account_number='$acode'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "location_contact_info set " .$TLCI." where account_number='$acode'");

    $r = array(
        "res:" => "good",
        "code" => "$acode"
    );
    /*$r = array(
        "tbls:" =>
        array(
            "TL" => "$TL",
            "TLA" => "$TLA",
            "TSI" => "$TSI")
    );*/
    echo json_encode($r);
}






/*#####################################
UPDATE
#####################################*/
if( isset($_POST['func']) && $_POST['func'] == "upd" ) {
    $acode = $_POST['acode'];

    $rs = $DB->query( "update " . $VR->tbl_prefix . "sensor_info set " .$TSI." where account_number='$acode'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "location set " .$TL." where account_number='$acode'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "location_addres set " .$TLA." where account_number='$acode'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "location_contact_info set " .$TLCI." where account_number='$acode'");

    $r = array(
        "res:" => "good",
        "code" => "$acode"
    );
    echo json_encode($r);
}

?>
