<?php
error_reporting(E_ERROR | E_PARSE);
session_start();
if(!isset($_SESSION['uName']) || is_null( $_SESSION['uName'] || $_SESSION['uName'] == "" )){
    header( 'Location: index.php' );
}
include_once "fr/html-head.php";
?>



<div id="contPGBAR" class="container">
    <div class="col-md-12">
        <div id="pgBar" class="progress progress-striped active" style="position:fixed; top:30%; width:80%">
          <div id="pgBarVal" class="progress-bar" role="progressbar" 
                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" 
                style="width: 0%;">
          </div>
        </div>
    </div>
</div>


<?php
include_once "fr/menu.php";
?>


<div id="ELGSYSAPP" style="display:none">

    <div class="container-fluid">
        <div class="row">
    <!--
    <div id="diFind" class="rounded-corners">
    <label>&nbsp;&nbsp;Find:</label><input id="inpFindLoc"></input>
    </div>
-->
<div id="diFainded" class="rounded-corners">
    <div id="Popup_close" class="olPopupCloseBox" style="width: 17px; height: 17px; position: absolute; right: 3px; top: 3px; z-index: 1;" onclick="$('#diFainded').hide()">
    </div>
    <div id="diFaindedCont">
        Press Enter to Search
    </div>
</div>

<div id="olMapDIV" style="top: 57px; left: 0; bottom: 0; right: 0; position: fixed;">
</div>

</div>
</div>


<?php
include_once "fr/notifMenu.php";
?>


    <!--div id="liss" style="position: fixed;bottom: 15px;z-index: 800;font-size: 12px;left: 15px;">
    Maps: openstreetmap.org <a href="http://www.openstreetmap.org" target="_blank">openstreetmap.org</a>
    <br/>
</div-->


<div class="modal fade" id="divNA" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add new Notification</h4>
            </div>
            <div class="modal-body">
                <div id="frmNA" class="form-horizontal" role="form">
                    <input type="hidden" name="idS" id="nAidS"></input>
                    <div class="form-group">
                        <label for="inpNAmess" class="col-sm-2 control-label">Message</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="inpNAmess" placeholder="Message">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="selNAtype" class="col-sm-2 control-label">Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="selNAmess">
                                <option value="urgent">Urgent</option>
                                <option value="warning">Warning</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnNAsave">Add Notification</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php

include_once "fr/admLocations.php";
include_once "fr/myAccount.php";
include_once "fr/admAccounts.php";
include_once "fr/account/admUsers.php";
include_once "fr/account/admLocs.php";
include_once "fr/sens.php";
include_once "fr/acco.php";
include_once "fr/search.php";


include_once "fr/html-foot.php";


include_once "fr/menuBottom.php";

include_once "fr/usr_assoc.php";
include_once "fr/admEvenCodes.php";
include_once "fr/messSYS.php";
?>

<script>
        var pgBar = $("#pgBar");
        var pgBarVal = $("#pgBarVal");
        var pgBarvalue=10;
        pgBARsetVal( 0 );
        function pgBARsetVal( val ){
            pgBarvalue = val;
            pgBarVal.css("width",val+"%");
            //pgBarVal.html(val+"%");
            if(val>=100){
                clearInterval( pgBarInterval );
                 $("#contPGBAR").fadeOut('slow',function(){
                    $('body').css('background-color', '#8CC20A');
                    $("#ELGSYSAPP").show("slow");
                    $('#navMenusTop').show( 'blind', {}, 400, function(){}); 
                    olMap.render("olMapDIV");
                 });
            }
        }
        var pgBarInterval = setInterval( function(){pgBARsetVal( pgBarvalue + 5 );},30);
    </script>
</div>

<?php

include_once "fr/html-foot-close.php";
?>
