<?php
function sendMail( $To, $ToName, $Subject, $Mess){
	require_once('libs/PHPMailer/class.phpmailer.php');

	$mail             = new PHPMailer();
	$mail->IsSMTP(); // telling the class to use SMTP
	//$mail->Host       = "mail.yourdomain.com"; // SMTP server
	//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
											   // 1 = errors and messages
											   // 2 = messages only
	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
	$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
	$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
	$mail->Username   = "elgmap@gmail.com";  // GMAIL username
	$mail->Password   = "elgmap09";            // GMAIL password

	$mail->SetFrom('system@elgmap.com', 'ELGMAP System');
	$mail->AddReplyTo("system@elgmap.com","ELGMAP System");
	$mail->Subject = $Subject; //"ELGMAP System Notification";
	//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

	$mail->MsgHTML($Mess);
	$mail->AddAddress($To, $ToName);

	//$mail->AddAttachment("images/phpmailer.gif");      // attachment
	//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

	if(!$mail->Send()) {
	  return "Error: " . $mail->ErrorInfo;
	} else {
	  return true;
	}
}
?>