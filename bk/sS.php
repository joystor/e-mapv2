<?php
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

error_reporting(E_ALL);
	ini_set('display_errors', 1);
    
include_once('cDBCon.php');
include_once('cfgVars.php');
global $DB;
global $VR;

if(isset($_POST['sT'])){
    switch ($_POST['sT']){
        case 'sAcc':
            $nam = $_POST['nam'];
            $rs  = $DB->query( "select (count(*)+1) as cu, DATE_FORMAT(NOW(),'%Y%m%d') as dd from tbl_elg2_accounts where substr(id_account,1,8)=DATE_FORMAT(NOW(),'%Y%m%d')" );
            $row = mysqli_fetch_assoc($rs);
            $conse = sprintf("%04d", $row['cu']);
            $dd = sprintf("%04d", $row['dd']);
            $ins = "insert into tbl_elg2_accounts(id_account, name, d_creation, d_update, user_creation) " .
                   " values( concat( '$dd', '-', '$conse'), '$nam', now(), now(), '".$_SESSION['user']."' );";
            $rs  = $DB->query( $ins );
            echo json_encode(array(
                "res" => "1",
                "id" => "$dd-$conse"
            ));
            exit(0);
            break;
        case 'dAcc':
            $acc = $_POST['acc'];
            $rs  = $DB->query( "delete from tbl_elg2_accounts where id_account='$acc'" );
            $rs2 = $DB->query( 
                "select id_user from " . $VR->tbl_prefix . "users ".
                " where id_account='".$acc."'" );
            while($row = mysqli_fetch_assoc($rs2)){
                $id = $row['id_user'];
                $rs  = $DB->query( "delete from tbl_elg2_users where id_user='$id'" );
                $rs  = $DB->query( "delete from tbl_elg2_user_address where id_user='$id'" );
                $rs  = $DB->query( "delete from tbl_elg2_user_contact where id_user='$id'" );
            }
            $rs2 = $DB->query( 
                "select account_number from " . $VR->tbl_prefix . "location ".
                " where id_account='".$acc."'" );
            while($row = mysqli_fetch_assoc($rs2)){
                $id = $row['account_number'];
                $rs  = $DB->query( "delete from tbl_elg2_location where account_number='$id'" );
                $rs  = $DB->query( "delete from tbl_elg2_location_addres where account_number='$id'" );
                $rs  = $DB->query( "delete from tbl_elg2_location_contact_info where account_number='$id'" );
                $rs  = $DB->query( "delete from tbl_elg2_sensor_info where account_number='$id'" );
            }
            echo json_encode(array(
                "res" => "1"
            ));
            exit(0);
            break;
        case 'dUs':
            $id = $_POST['iu'];
            $rs  = $DB->query( "delete from tbl_elg2_users where id_user='$id'" );
            $rs  = $DB->query( "delete from tbl_elg2_user_address where id_user='$id'" );
            $rs  = $DB->query( "delete from tbl_elg2_user_contact where id_user='$id'" );
            echo json_encode(array(
                "res" => "1"
            ));
            exit(0);
            break;
        case 'eAcc':
            $acc = $_POST['acc'];
            $nam = $_POST['nam'];
            $rs  = $DB->query( "update tbl_elg2_accounts set name='$nam' where id_account='$acc'" );
            echo json_encode(array(
                "res" => "1"
            ));
            exit(0);
            break;
    }
}
?>
