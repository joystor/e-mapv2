<?php

$TAU = "";
$TAUA = "";
$TAUC = "";
foreach ($_POST as $pname => $pval) {
    if (isset($pname) && $pname != "" 
        && strpos($pname,'_') !== false 
        && strpos($pname,'inAcU') !== false 
        && $pval != "") 
    {
        list($pn1, $pn2) = explode('_', $pname, 2);
        if( isset($pn2) && $pn2 != "" ){
            if( $pn1 == "inAcU"){
                $TAU .= " ".$pn2."='$pval',";
            }elseif ($pn1 == 'inAcUA'){
                $TAUA .= " ".$pn2."='$pval',";
            }elseif ($pn1 == 'inAcUC'){
                $TAUC .= " "." ".$pn2."='$pval',";
            }
        }
    }
}
$TAU = rtrim($TAU, ",");
$TAUA = rtrim($TAUA, ",");
$TAUC = rtrim($TAUC, ",");


include_once('cDBCon.php');
include_once('cfgVars.php');
include_once('sMm.php');

/*#####################################
INSERT
#####################################*/
if( isset($_POST['func']) && $_POST['func'] == "ins" ) {

    //User id_user_acc
    $UsrCode = genAccoutCODE( substr($us,0,1).substr($uF) );
    $exist = true;
    while($exist){
        $rs = $DB->query("select count(*) as cnt from  " . $VR->tbl_prefix . "user where id_user_acc='$UsrCode'");
        $row = mysqli_fetch_array($rs);
        if( $row['cnt'] == 0){
            $exist = false;
        }else{
            $UsrCode = genAccoutCODE( substr($us,0,1).substr($uF) );
        }
    }

    $pass = randomPassword();
    $in_U = "insert into " . $VR->tbl_prefix . "users (`id_account`, `user`, `d_creation`, `pass`, id_user_acc) ".
            "VALUES ('".$_POST['inAcU_id_account']."', '".$_POST['inAcU_user']."', now(), md5('$pass'), '$UsrCode' );";
    $rs = $DB->query( $in_U );
    $rs = $DB->query( "select id_user from " . $VR->tbl_prefix . "users where id_account='".$_POST['inAcU_id_account']."' and user='".$_POST['inAcU_user']."'" );
    $id = "";
    while( $row = mysqli_fetch_assoc($rs) ) {
        $id = $row['id_user'];
    }

    if( $id != "" ){


        $in_UA = "insert into " . $VR->tbl_prefix . "user_address (`id_user`) VALUES ('$id');";
        $in_UC = "insert into " . $VR->tbl_prefix . "user_contact (`id_user`) VALUES ('$id');";
        $rs = $DB->query( $in_UA );
        $rs = $DB->query( $in_UC );

        $rs = $DB->query( "update " . $VR->tbl_prefix . "users set " .$TAU." where id_user='$id'");
        $rs = $DB->query( "update " . $VR->tbl_prefix . "user_address set " .$TAUA." where id_user='$id'");
        $rs = $DB->query( "update " . $VR->tbl_prefix . "user_contact set " .$TAUC." where id_user='$id'");
    
        sendMail( $_POST['inAcUC_user_mail'], 
            $_POST['inAcU_f_name']." ".$_POST['inAcU_l_name'], 
            "ELG System Login", 
            "Dear: <strong>".$_POST['inAcU_f_name']." ".$_POST['inAcU_l_name']."</strong><br/>
            You have an account here are you user data to login:<br>
            user: ".$_POST['inAcU_user']."
            pass: ".$pass."
            ");
    }
//id_user
    $r = array(
        "res:" => "good",
        "code" => "$id"
    );
    echo json_encode($r);
}


/*#####################################
DELETE
#####################################*/
if( isset($_POST['func']) && $_POST['func'] == "del" ) {
    $id = $_POST['idU'];
    $rs = $DB->query( "DELETE FROM " . $VR->tbl_prefix . "users WHERE id_user='$id';");
    $rs = $DB->query( "DELETE FROM " . $VR->tbl_prefix . "user_address WHERE id_user='$id';");
    $rs = $DB->query( "DELETE FROM " . $VR->tbl_prefix . "user_contact WHERE id_user='$id';");
    $r = array(
        "res:" => "good",
        "code" => ""
    );
    echo json_encode($r);
}



/*#####################################
UPDATE
#####################################*/
if( isset($_POST['func']) && $_POST['func'] == "upd" ) {
    $id = $_POST['idU'];

    $TAU = str_replace("id_account='".$_POST['inAcU_id_account']."',","",$TAU);
    $TAUA = str_replace("id_account='".$_POST['inAcU_id_account']."',","",$TAUA);
    $TAUC = str_replace("id_account='".$_POST['inAcU_id_account']."',","",$TAUC);

    $rs = $DB->query( "update " . $VR->tbl_prefix . "users set " .$TAU." where id_user='$id'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "user_address set " .$TAUA." where id_user='$id'");
    $rs = $DB->query( "update " . $VR->tbl_prefix . "user_contact set " .$TAUC." where id_user='$id'");

    $r = array(
        "res:" => "good",
        "code" => "$id",
        "sql" => "update " . $VR->tbl_prefix . "users set " .$TAU." where id_user='$id'"
    );
    echo json_encode($r);
}




/*#####################################
User assossiations
#####################################*/
if( isset($_POST['func']) && $_POST['func'] == "uAsoc" ) {
    $idA = $_POST['idA'];
    $idB = $_POST['idB'];
    $typ = $_POST['ty'];
    $q = "INSERT INTO " . $VR->tbl_prefix . "user_associations VALUES('$idA','$idB','$typ')";
    $rs = $DB->query( $q );

    $r = array(
        "res:" => "good"
        //,"sql" => "$q"
    );
    echo json_encode($r);
}
if( isset($_POST['func']) && $_POST['func'] == "uDelAsoc" ) {
    $idA = $_POST['idA'];
    $idB = $_POST['idB'];
    $typ = $_POST['ty'];
    $q = "DELETE FROM " . $VR->tbl_prefix . "user_associations WHERE id_user='$idA' and id_assoc='$idB' and usrassoc_type='$typ'";
    $rs = $DB->query( $q );

    $r = array(
        "res:" => "good"
        //,"sql" => "$q"
    );
    echo json_encode($r);
}



function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
?>