<?php
/*$phpErrFile = "php-error.log";
file_put_contents( $phpErrFile , "");
fclose($fh);

ini_set("log_errors", 1);
ini_set("error_log", $phpErrFile);
error_reporting(E_ALL);*/

    include_once('cDBCon.php');
    include_once('cfgVars.php');
    include_once('sMs.php');
    include_once('sMm.php');
    global $DB;
    global $VR;

    
    //CODES TO ANALYSE
    $q = "SELECT code, d_alias, ev_type, ev_mins FROM cat_elg2_event_codes
            WHERE ev_type is not null and ev_type<>'none' and ev_type<>''";
    $rs  = $DB->query( $q );
    $CODES = array();
    while( $row = mysqli_fetch_assoc($rs) ) {
        $COD = array(
                "code" => $row['code'],
                "d_alias" => $row['d_alias'],
                "ev_type" => $row['ev_type'],
                "ev_mins" => $row['ev_mins']
            );
        array_push($CODES, $COD);
    }

    #################################
    #################################
    #################################
    //SAVE STATUS OK SS1
    $q = "INSERT INTO tbl_elg2_sensor_robo_analysis(id_loc_account, id_sensor_status, msg_type, d_creation)
            SELECT S.id_loc_account, S.id_sensor_status, 'OK' msg_type, now( )
                FROM tbl_elg2_sensor_status S
                LEFT JOIN tbl_elg2_sensor_robo_analysis R ON S.id_loc_account = R.id_loc_account
                AND S.id_sensor_status = R.id_sensor_status
                WHERE R.id_loc_account IS NULL
                AND msg_org LIKE '%SS1,%'";
    $rs = $DB->query( $q );

    //Analyse Notifications
    $q = "SELECT S.*, TIMESTAMPDIFF(MINUTE,S.d_creation, now()) mins, L.location_name, L.id_account
            FROM tbl_elg2_sensor_status S 
            LEFT JOIN tbl_elg2_sensor_robo_analysis R 
            ON S.id_loc_account = R.id_loc_account and S.id_sensor_status=R.id_sensor_status
            LEFT JOIN  tbl_elg2_location L
            ON L.account_number =S.id_loc_account
            WHERE R.id_loc_account is null and msg_org not like '%SS1,%'
            ORDER BY S.id_sensor_status";
    $rs = $DB->query( $q );
    $CC = array();
    while( $row = mysqli_fetch_assoc($rs) ) {

        $msg_org = $row['msg_org'];
        $found = false;
        foreach ($CODES as $cod) {
            if( strrpos( $row['msg_org'], $cod[code] ) !== false ){
                array_push( $CC, $cod);
                $found = true;
                //echo "found ". $msg_org . "<br>";
                //echo "time code ". $c['ev_mins'] . "<br>";
                //echo "time msg ". $row['mins'] . "<br><br>";
            }
        }
        if($found == true){
            //NOTIFICATION SS2
            //WARNING SS3
            //ALERT SS4
            if( strrpos( $row['msg_org'], "SS2") !== false ||
                strrpos( $row['msg_org'], "SS3") !== false || 
                strrpos( $row['msg_org'], "SS4") !== false )
            {
                $sendNotif = false;
                //CHECK TIME
                foreach ($CC as $c) {
                    //echo "<br>chekin time: ". $c['ev_mins'] . " <= " . $row['mins'];
                    if((int)$c['ev_mins'] <= (int)$row['mins']){
                        //echo " showNotif";
                        $sendNotif = true;
                    }
                }
                if($sendNotif){

                    $tipNOTIF = "";
                    if( strrpos( $row['msg_org'], "SS2") !== false){
                        $tipNOTIF = "NOTIFICATION";
                    }
                    if( strrpos( $row['msg_org'], "SS3") !== false){
                        $tipNOTIF = "WARNING";
                    }
                    if( strrpos( $row['msg_org'], "SS4") !== false){
                        $tipNOTIF = "ALERT";
                    }
                    $dateNOTIF = $row["d_creation"];

                     // QUERY TO CONSTRUCT MESSAGE INFO
                    $q = "SELECT S.code, E.d_alias, S.value
                            FROM tbl_elg2_sensor_status_details S
                            LEFT JOIN cat_elg2_event_codes E ON S.code = E.code
                            WHERE id_sensor_status =".$row["id_sensor_status"];
                    $rsT = $DB->query( $q );
                    $table = "<table>";
                    $table .= "<tr>";
                    $table .= "<th>CODE</th>";
                    $table .= "<th>DESCRIPCION</th>";
                    $table .= "<th>VALUE</th>";
                    $table .= "</tr>";
                    while( $rU = mysqli_fetch_assoc($rsT) ) {
                        $table .= "<tr>";
                        $table .= "<td>".$rU["code"]."</td>";
                        $table .= "<td>".$rU["d_alias"]."</td>";
                        $table .= "<td>".$rU["value"]."</td>";
                        $table .= "</tr>";
                    }
                    $table .= "</table>";


                    $htmlMSG  = "<br>Notification: <strong>".   $row["location_name"] . "</strong><br/>";
                    $htmlMSG .= "Date: <strong>".  $dateNOTIF . "</strong><br/>";
                    $htmlMSG .= "Type: <strong>". $tipNOTIF ."</strong>";
                    $htmlMSG .= $table;
                    //echo $htmlMSG;
                    $msgSMS = "";
                    
                    //insert to robots analisis
                    $q = "INSERT INTO tbl_elg2_sensor_robo_analysis
                            (   id_loc_account, id_sensor_status, d_creation, 
                                msg_type, msg_plain, msg_sms, msg_mail )
                            VALUES
                            (   '".$row['id_loc_account']."', ".$row['id_sensor_status'].", now(),
                                '$tipNOTIF', '$htmlMSG', '$msgSMS', '$htmlMSG'
                            );";
                    $rsT = $DB->query( $q );
                    $idRobo = $DB->getConx()->insert_id;
                    //echo "<br>ID:".$idRobo;
                    //echo "<br>IDMD5:".md5($idRobo);
                    
                    $url2Shrt = $VR->urlSYS."snot.php?gRtEy=".md5($idRobo);
                    $urlShrt = URL2GoogleShort( $url2Shrt );

                    $msgSMS = "ELGMAP: ". $tipNOTIF. " in ". $row["location_name"] . " more info: ". $urlShrt["id"];



                    $q = "SELECT U.user, U.f_name||' '||U.l_name nom, U.alias , C.user_mail, C.cell_1, C.home_1, U.send_sms, U.send_mail, U.id_user
                            FROM tbl_elg2_users U 
                            LEFT JOIN tbl_elg2_user_contact C
                            ON U.id_user=C.id_user
                            WHERE (U.send_sms=1 or U.send_mail=1)
                                and (id_account='".$row["id_account"]."'
                                or U.id_user in ( SELECT id_user FROM tbl_elg2_user_associations WHERE id_assoc='".$row["id_loc_account"]."'))
                            ";
                    //echo $q . "<br/>";
                    $rsT = $DB->query( $q );
                    $usersSMS = "";
                    $usersMAIL = "";
                    $twilioRESP = "";
                    while( $rU = mysqli_fetch_assoc($rsT) ) {
                        if($rU['send_sms'] == 1){
                            $usersSMS .= $rU['user'].",";
                            if($rU["cell_1"] != "" || $rU["home_1"] != ""){
                                $cel = "";
                                if($rU["cell_1"] != ""){
                                     $cel = $rU["cell_1"];
                                }
                                if($rU["home_1"] != ""){
                                     $cel = $rU["home_1"];
                                }
                                if( $cel != "" ){
                                    echo "smsto:".$cel."<br>";
                                    echo "mes:".$msgSMS."<br>";
                                    $twilioRESP = SendSMS($msgSMS, $cel);
                                    echo $r;
                                }
                            }
                        }
                        echo "<br/>refes<br/>";
                        if($rU['send_mail'] == 1){
                            $usersMAIL .= $rU['user'].",";
                            if($rU["user_mail"] != ""){
                                echo "mailto:".$rU['user_mail']."<br>";
                                echo "mes:".$htmlMSG."<br>";
                                sendMail( $rU['user_mail'], $rU['fnom'] , $tipNOTIF, $htmlMSG );
                            }
                        }
                    }




                    //echo "<br>URL:" . $urlShrt["id"] . "<br>";
                    $q = "UPDATE tbl_elg2_sensor_robo_analysis 
                            SET google_url='".$urlShrt["id"]."',
                                msg_sms='".$msgSMS."',
                                users_sms='".$usersSMS."',
                                users_mail='".$usersMAIL."',
                                twilio_response='".$twilioRESP."'
                            WHERE id_sensor_analysis=".$idRobo;
                    //echo $q;
                    $rsT = $DB->query( $q );
                    //echo "<br>";
                }
            }
        }
    }

    exit(0);








function URL2GoogleShort( $url ){
    //$googleShortURL = "https://www.googleapis.com/urlshortener/v1/url?key=".$key."&shortUrl=".$url2Shrt."&projection=ANALYTICS_TOP_STRINGS";
    $key = "AIzaSyAW_VQb6al2H0KAXcQHanbP5ADLFGSceSY";
    $GOOGLEAPI = "https://www.googleapis.com/urlshortener/v1/url?key=".$key;

    $data2GLE = array("longUrl" => $url);
    $content = json_encode( $data2GLE );

    $curl = curl_init($GOOGLEAPI);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Content-type: application/json"));
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

    $google_response = curl_exec($curl);
    //$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    return json_decode($google_response, true);
    //return $google_response;
}










    /*
    SELECT S.id_sensor_status, id_loc_account, now()
            FROM tbl_elg2_sensor_status S 
            LEFT JOIN tbl_elg2_sensor_robo_analysis R 
            ON S.id_loc_account = R.id_loc_account and S.id_sensor_status=R.id_sensor_status
            WHERE R.id_loc_account is null and msg_org like '%SS1,%'
*/

    //SAVE STATUS OK SS1
    /*$q = "SELECT S.*, TIMESTAMPDIFF(MINUTE,S.d_creation, now()) mins 
            FROM tbl_elg2_sensor_status S 
            LEFT JOIN tbl_elg2_sensor_robo_analysis R 
            ON S.id_loc_account = R.id_loc_account and S.id_sensor_status=R.id_sensor_status
            WHERE R.id_loc_account is null and msg_org like 'SS1,'";
    $rs  = $DB->query( $q );
    while( $row = mysqli_fetch_assoc($rs) ) {
        $EVENTS .= " msg_org LIKE '%".$row['code']."%' OR";
    }*/
?>
