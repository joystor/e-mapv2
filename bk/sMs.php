<?php

function send_sms( $sid, $token, $from, $to, $body ) {
    // resource url & authentication
    $uri = 'https://api.twilio.com/2010-04-01/Accounts/' . $sid . '/SMS/Messages.json';
    $auth = $sid . ':' . $token;
 
    // post string (phone number format= +15554443333 ), case matters
    $fields =
        '&To=' .  urlencode( $to ) .
        '&From=' . urlencode( $from ) .
        '&Body=' . urlencode( $body );
 
    // start cURL
    $res = curl_init();
     
    // set cURL options
    curl_setopt( $res, CURLOPT_URL, $uri );
    curl_setopt( $res, CURLOPT_POST, 3 ); // number of fields
    curl_setopt( $res, CURLOPT_POSTFIELDS, $fields );
    curl_setopt( $res, CURLOPT_USERPWD, $auth ); // authenticate
    curl_setopt( $res, CURLOPT_RETURNTRANSFER, true ); // don't echo
     
    // send cURL
    $result = curl_exec( $res );
    return $result;
}


function SendSMS($mes, $to){
    return send_sms(
        "ACbd947fc19b32fdecc2afaf3735994326",
        "5b7f2c1452418cd89abaa626d2970334",
        "+18324632534",
        "+".trim($to),
        $mes
        );
}


function SendMSGJoys( $mes ){
	return send_sms(
	"AC42785f791be7a69cda3b7c893c42cac7",
	"7397fcef581b15c9bc1010a105c50472",
	"+5214491233459",
	  "+14234549173",
	$mes
	);
}

function SendMSGRobinson( $mes ){
	return send_sms(
	"AC491a2da6dda1e0a7b2a1792441d94127",
	"a80ecf2f7a293abfbbc12aa7d0d39533",
	"+17817716743",
	"+17817803355",
	$mes
	);
}

/*
function SendSMS( $msg ){
	$j = "{\"res1\":".SendMSGJoys( $msg )."},";
	$j.= "\"res2\":{".SendMSGRobinson( $msg );
}*/

function genPinCode(){
	return rand(0,9).rand(0,9).rand(0,9).rand(0,9);
}

/*
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
*/

/*
echo "{\"res1\":{";
echo  SendMSGJoys( "Chiquitita JIJIJIIJJI :)" );

echo "},";
echo "\"res2\":{";
echo  SendMSGRobinson( "Hi, this is a test number 2" );
echo "}}";
*/

?>