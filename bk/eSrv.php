<?php
/*$phpErrFile = "php-error.log";
file_put_contents( $phpErrFile , "");
fclose($fh);

ini_set("log_errors", 1);
ini_set("error_log", $phpErrFile);
error_reporting(E_ALL);*/

include_once('cDBCon.php');
include_once('cfgVars.php');
include_once('sMs.php');
include_once('sMm.php');
global $DB;
global $VR;


header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

if($_REQUEST['msg'] == ""){
    echo json_encode( array("res"=>"Msg dont have data"));
}
$msg = $_REQUEST['msg'];

$gr = explode(",", $msg);

$acco = $gr[0];
$aAc = explode("-", $acco);
$acco = $aAc[0]."-".$aAc[1]; // account
$anum = $aAc[2]; // location

$date = $gr[1];
$date = substr($date, 0,2) . "/" . substr($date, 2,2)."/".substr($date, 4); // mm/dd/yyyy
$date = strtotime( $date );
$date = date('Y-m-d', $date);

$time = $gr[2];

$CODES = str_replace($gr[0].','.$gr[1].','.$gr[2].',', "", $msg);
//20140906-0001-YBags0524,09182014,20:25:15,SS2,PS3,SE2,SE11,TS3,TS14:23,GS8:45,GS11,GS13


$q = "INSERT INTO tbl_elg2_sensor_status( id_loc_account, d_creation, msg_org) ". 
        " VALUES('$anum', '$date $time', '$CODES');";
$rs = $DB->query( $q );
$q = "SELECT max(id_sensor_status) as mxid FROM tbl_elg2_sensor_status WHERE id_loc_account='$anum';";
$rs = $DB->query( $q );
$rowStatus = mysqli_fetch_assoc($rs);
$staID = $rowStatus['mxid'];

$code = explode(',', $CODES);

foreach($code as $cd) {
    $c = "";
    $v = "";
    if(strpos($cd, ':') !== false){
        $z = explode(":", $cd);
        $c = $z[0];
        $v = $z[1];
    }else{
        $c = $cd;
    }
    $q = "INSERT INTO   tbl_elg2_sensor_status_details( id_sensor_status, code, value) ".
        "VALUES($staID, '$c', '$v');";
    $rs = $DB->query( $q );
}



echo json_encode( array("res"=>"good", "msg" => "$msg","q"=>"$q") );
exit(0);




///END


$q = "SELECT D.description as descrip, S.code as code, S.value as value"
    ." FROM tbl_elg2_sensor_status_details S"
    ." LEFT JOIN cat_elg2_event_codes D ON CONVERT(D.code USING utf8)=CONVERT(S.code USING utf8)"
    ." WHERE id_sensor_status=$staID";
error_log($q);
$rs = $DB->query( $q );
$tab = "<table>";
$tab .= "<tr>"
        ."<th>Message</th>"
        //."<th>code</th>"       
        ."<th>value</th>"
        ."</tr>";
while( $row = mysqli_fetch_assoc($rs) ) {
    $tab .= "<tr>";
    $tab .= "<td>". $row['descrip']."</td>";
    //$tab .= "<td>". $row['code']."</td>";
    $tab .= "<td>". $row['value']."</td>";
    $tab .= "</tr>";
}
$tab .= "</table>";

$MSG = "<br>".$tab;



//$rs = $DB->query( "SET group_concat_max_len = 2048;" );  GROUP_CONCAT(A.id_assoc SEPARATOR ', ')
$q = "SELECT U.user, U.f_name||' '||U.l_name nom, U.alias , C.user_mail, C.cell_1, C.home_1, U.send_sms, U.send_mail, U.id_user
            FROM tbl_elg2_users U 
            LEFT JOIN tbl_elg2_user_contact C ON U.id_user=C.id_user
            WHERE (U.send_sms=1 or U.send_mail=1) and 
                 ( U.id_account='$acco' or U.id_user in ( SELECT id_user FROM tbl_elg2_user_associations WHERE id_assoc='$anum'))
        GROUP BY U.user";
error_log($q);
$rs = $DB->query( $q );
while( $row = mysqli_fetch_assoc($rs) ) {
    if($row['send_mail']==1){
        //error_log("sending mail".$row['user_mail']);
        sendMail( $row['user_mail'], $row['nom'], "ELGMAP Status $date $time", $tab);
    }
    //Send SMS
    if($row['send_sms']==1){
        
        /*$toPhone = "";
        if($row['cell_1']!=""){
            $toPhone = $row['cell_1'];
        }else if($row['home_1']!=""){
            $toPhone = $row['home_1'];
        }
        //error_log("sending sms".$toPhone);
        if($toPhone != ""){
            $r = SendSMS($mSms, $toPhone);
            error_log("sms:".$r);
        }*/
    }
}


echo json_encode( array("res"=>"good", "msg" => "$msg","q"=>"$q") );
exit(0);
/*


$syst = $gr[3];
$prem = $gr[4];
$powe = $gr[5];
$sens = $gr[6];
$tran = $gr[7];
$gene = $gr[8];*/
/*
$q = "INSERT INTO tbl_elg2_message_events ".
        "(id_account, account_number, edate, etime, ".
        //"system_s, premise_s, power_s, sensor_s, transfer_s, generator_s, 
          "  msg)".
        " VALUES(".
        "'$acco','$anum','$date','$time',".
        //"'$syst','$prem','$powe','$sens','$tran','$gene',
        "'$msg'".
        ")";
$rs = $DB->query( $q );


$evtMessage = array("SS1", "SS2", "SS3", "SS4");
if(in_array($syst, $evtMessage)){
    $tySys = "";

    $q = "SELECT    M . * , C1.description AS dsystem, C2.description AS dpremise, C3.description AS dpower, 
                    C4.description AS dsensor, C5.description AS dtransfer, C6.description AS dgenerator
                FROM tbl_elg2_message_events M
                LEFT JOIN cat_elg2_event_codes C1 ON M.system_s = C1.code
                COLLATE utf8_unicode_ci
                LEFT JOIN cat_elg2_event_codes C2 ON M.premise_s = C2.code
                COLLATE utf8_unicode_ci
                LEFT JOIN cat_elg2_event_codes C3 ON M.power_s = C3.code
                COLLATE utf8_unicode_ci
                LEFT JOIN cat_elg2_event_codes C4 ON M.sensor_s = C4.code
                COLLATE utf8_unicode_ci
                LEFT JOIN cat_elg2_event_codes C5 ON M.transfer_s = C5.code
                COLLATE utf8_unicode_ci
                LEFT JOIN cat_elg2_event_codes C6 ON M.generator_s = C6.code
                COLLATE utf8_unicode_ci
            WHERE M.id_account='$acco' and M.account_number='$anum' ORDER BY id_message desc LIMIT 1";



    $rsM = $DB->query( $q );
    $MSG = mysqli_fetch_assoc($rsM);

    $mMai = $MSG['edate']." ".$MSG['etime'].
            "<br>Acc: $acco, Loc: $anum, ";
    $MStatus="<br>System status:<strong>" . $MSG['dsystem']."</strong>".
            "<br>Premise status:<strong>" . $MSG['dpremise']."</strong>".
            "<br>Power status:<strong>" . $MSG['dpower']."</strong>".
            "<br>Sensor status:<strong>" . $MSG['dsensor']."</strong>".
            "<br>Transfer Switch Gear Status:<strong>" . $MSG['dtransfer']."</strong>".
            "<br>Generator Status:<strong>" . $MSG['dgenerator']."</strong>";
    $mSms =  str_replace(", ","<br>",$mMai.$MStatus);
    $mSms =  str_replace(" ","<strong>",$mSms);
    $mSms =  str_replace("","</strong>",$mSms);

    $q = "SELECT U.user, U.f_name||' '||U.l_name nom, U.alias , C.user_mail, C.cell_1, C.home_1, U.send_sms, U.send_mail, U.id_user
            FROM tbl_elg2_users U LEFT JOIN tbl_elg2_user_contact C
            ON U.id_user=C.id_user
            WHERE U.id_account='$acco' and (U.send_sms=1 or U.send_mail=1)";
    $rs = $DB->query( $q );
    while( $row = mysqli_fetch_assoc($rs) ) {

        $q = "insert into tbl_elg2_notification(id_user, id_account, account_number, notification_type,message, id_message) ".
                    " values(".$row['id_user'].", '$acco', '$anum', '".$MSG['dsystem']."', '$MStatus', ".$MSG['id_message'].");";
        $rs2 = $DB->query($q);

        if($MSG['dsystem'] != "Ok"){
            //Send mail
            if($row['send_mail']==1){
                //error_log("sending mail".$row['user_mail']);
                sendMail( $row['user_mail'], $row['nom'], "ELGMAP - ".$MSG['dsystem'], $mMai . $MStatus);
            }
            //Send SMS
            if($row['send_sms']==1){
                
                $toPhone = "";
                if($row['cell_1']!=""){
                    $toPhone = $row['cell_1'];
                }else if($row['home_1']!=""){
                    $toPhone = $row['home_1'];
                }
                //error_log("sending sms".$toPhone);
                if($toPhone != ""){
                    $r = SendSMS($mSms, $toPhone);
                    error_log("sms:".$r);
                }
            }
        }
    }
}

echo json_encode( array("res"=>"good", "msg" => "$msg","q"=>"$q") );*/
?>
