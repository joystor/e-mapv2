<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	include_once('cDBCon.php');
	include_once('cfgVars.php');
	include_once('sMs.php');
	include_once('sMm.php');
	
	global $DB;
	global $VR;
	
	$arr = array();
	
	if(isset($_REQUEST['f1'])){
		switch ($_REQUEST['f1']){
			case "stColor":
				$id = $_REQUEST['idS'];
				$col = $_REQUEST['col'];
				$rs = $DB->query("update " . $VR->tbl_prefix . "stations set color= '$col' where id_station=$id;");
				$arr['res'] = "good";
				break;
			case "stScore":
				$id = $_REQUEST['idS'];
				$scr = $_REQUEST['scr'];
				$rs = $DB->query("update " . $VR->tbl_prefix . "stations set score= '$scr' where id_station=$id;");
				$arr['res'] = "good";
				break;
			case "stNeAl":
				$id = $_REQUEST['idS'];
				$me = $_REQUEST['sMe'];
				$ty = $_REQUEST['sTy'];
				
				$s = "select L.location_name, A.address1, city, state ".
					" from " . $VR->tbl_prefix . "location L".
					" left join " . $VR->tbl_prefix . "location_addres A on L.account_number=A.account_number ".
					" where L.account_number='$id'";
				$rs = $DB->query($s);
				$St = mysqli_fetch_assoc($rs);
				//$rs->free();
				
				/*$s = "select f_name || ' ' || l_name as name, mail, mobile, M.* from " . $VR->tbl_prefix . "users U ".
					" left join " . $VR->tbl_prefix . "user_map M ON U.id_user=M.id_user " .
					" where U.id_user in (select id_user from " . $VR->tbl_prefix . "user_stations where id_station=$id)";
				$rs = $DB->query($s);
				while( $row = mysqli_fetch_assoc($rs) ){*/
					$pincode = genPinCode();
					$meS = $St['location_name'] .", ". $ty . ": ". $me . ", code: " . $pincode;
					$sub = "ELGMAP Notification: ".$ty;
					$meSM = "Station: <strong>".$St['location_name'] ."</strong><br/> " .
							"Address: <strong>".$St['address1'] ."</strong><br/> " .
							"City: <strong>".$St['city'] ."</strong><br/> " .
							"State: <strong>".$St['state'] ."</strong><br/> " .
							"Location Name: <strong>".$St['location_name'] ."</strong><br/> " .
							"<br/>" . $ty . ": ". $me .  "<br/>" ;
							//"Code: <strong>" . $pincode . "</strong>";
							
					/*if($row['send_mail'] == 1){
						sendMail( $row['mail'], $row['name'], $sub, $meSM );
					}
					if($row['send_sms'] == 1){
						SendMSGJoys( $meS );
						if($row['id_user']==1){
							SendMSGRobinson( $meS );
						}
					}*/
					$q = "insert into " . $VR->tbl_prefix . "notification(id_user,id_account,notification_type,message,code) ".
									" values(0, '$id', '$ty', '$me', '$pincode');";
									// " values(".$row['id_user'].", $id, '$ty', '$me', '$pincode',".$row['send_mail'].",".$row['send_sms'].");";
					//$arr['q'] = "$q";
					$rs2 = $DB->query($q);
				//}
				$arr['res'] = "good";
				$arr['q'] = $q;
				break;
			case "sCFG":
				$mail = $_REQUEST['mail'];
				$sms = $_REQUEST['sms'];
				$q = "update " . $VR->tbl_prefix . "user_map set send_sms=$sms, send_mail=$mail where id_user=".$_SESSION['uID'];
				$rs = $DB->query($q);
				$arr['res'] = "good";
				break;
			case "stPinCOD":
				$mail = $_REQUEST['pin'];
				$id = $_REQUEST['id'];
				$q = "update " . $VR->tbl_prefix . "notification set date_confirm=now(), user_confirm=1 ".
					" where id_notification=".$id;  //id_user=".$_SESSION['uID']." and
				$rs = $DB->query($q);
				$arr['res'] = "good";
				//$arr['q'] = "$q";
				break;
            case "stSSStusWB":
                $idMD5 = $_POST['id'];
                $st = $_POST['st'];
                $q = "UPDATE tbl_elg2_sensor_status SET status_web=".$st." WHERE id_sensor_status=".$idMD5;
                $rs = $DB->query($q);
                $arr['res'] = "good";
                break;
		}
	}else{
		$arr['err'] = "notF1";
	}
	
	
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');
	echo json_encode( $arr ); 
?>
