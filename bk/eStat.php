<?php
error_reporting(E_ERROR | E_PARSE);
$fun2exec = $_POST['pG'];


include_once('cDBCon.php');
include_once('cfgVars.php');
global $DB;
global $VR;

switch ($fun2exec) {
    case 'gStaLoc':
            $idLoc = $_POST['loc'];
            $q = "SELECT count(*) as cnt_not,
                        (SELECT count(*) 
                            FROM tbl_elg2_sensor_status_details D
                            LEFT JOIN cat_elg2_event_codes C ON C.code=D.code
                            WHERE id_sensor_status=S.id_sensor_status and ev_type='notifications') as notif,
                        (SELECT count(*) 
                            FROM tbl_elg2_sensor_status_details D
                            LEFT JOIN cat_elg2_event_codes C ON C.code=D.code
                            WHERE id_sensor_status=S.id_sensor_status and ev_type='alert') as alert,
                        (SELECT count(*) 
                            FROM tbl_elg2_sensor_status_details D
                            LEFT JOIN cat_elg2_event_codes C ON C.code=D.code
                            WHERE id_sensor_status=S.id_sensor_status and ev_type='warnings') as warnings
                    FROM tbl_elg2_sensor_status S 
                    WHERE id_loc_account='$idLoc'
                    GROUP BY id_loc_account";
            $rs  = $DB->query( $q );
            $r = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($r, $row);
            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode( 
                    array("rows" => $r)
                    );
        break;
    case 'gStaAllLoc':
            $WHR = " WHERE id_loc_account in ( select id_assoc from tbl_elg2_user_associations where id_user=".$_SESSION['uID'].")
                        or id_loc_account in (select id_assoc from tbl_elg2_location where id_account='".$_SESSION['IDACCOUNT']."')
                    ";
            if($_SESSION['isAdmin']){
                $WHR = "";
            }
            $q = "SELECT count(*) as cnt_not,
                        (SELECT count(*) 
                            FROM tbl_elg2_sensor_status_details D
                            LEFT JOIN cat_elg2_event_codes C ON C.code=D.code
                            WHERE id_sensor_status=S.id_sensor_status and ev_type='notifications') as notif,
                        (SELECT count(*) 
                            FROM tbl_elg2_sensor_status_details D
                            LEFT JOIN cat_elg2_event_codes C ON C.code=D.code
                            WHERE id_sensor_status=S.id_sensor_status and ev_type='alert') as alert,
                        (SELECT count(*) 
                            FROM tbl_elg2_sensor_status_details D
                            LEFT JOIN cat_elg2_event_codes C ON C.code=D.code
                            WHERE id_sensor_status=S.id_sensor_status and ev_type='warnings') as warnings
                    FROM tbl_elg2_sensor_status S 
                    ". $WHR;
            $rs  = $DB->query( $q );
            $r = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($r, $row);
            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode( 
                    array("rows" => $r)
                    );
        break;
    default:
        break;
}
?>
