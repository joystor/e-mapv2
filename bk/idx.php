<?php
$phpErrFile = "php-error.log";
file_put_contents( $phpErrFile , "");
fclose($fh);

ini_set("log_errors", 1);
ini_set("error_log", $phpErrFile);
error_reporting(E_ALL);
//ini_set('display_errors', 1);
    
$fc = isset($_POST['fc'])?$_POST['fc']:"";

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
if($fc == ""){
    echo json_encode(array("res" => "NoParms"));
    exit(0);
}

include_once('cDBCon.php');
include_once('cfgVars.php');
include_once('sMs.php');
include_once('sMm.php');
include_once('intFunctions.php');
include_once 'bkFuns.php';


switch ($fc){
    /*#################################################################*/
    case "recMl": //Sent to mail User and password
            $ms = $_POST['ms'];
            
            $q = "SELECT title, msg FROM tbl_elg2_mess_sys WHERE id_mess_sys = 'FORGOT_ACCOUNT';";
            $rs = $DB->query( $q );
            $row = mysqli_fetch_assoc($rs);
            $TITLE = $row['title'];
            $MESSG = $row['msg'];
            
            $q = "SELECT U.user, U.f_name, U.l_name ".
                " FROM ".$VR->tbl_prefix."user_contact UC LEFT JOIN ".$VR->tbl_prefix."users U ON U.id_user=UC.id_user ".
                " WHERE UC.user_mail='".$ms."' or UC.personal_mail='".$ms."'";
            $rs = $DB->query( $q );
            $finds = 0;
            while( $row = mysqli_fetch_assoc($rs) ){
                $mes =  $MESSG;
                $mes =  str_replace("%FNAME%", $row['f_name'], $mes);
                $mes =  str_replace("%LNAME%", $row['l_name'], $mes);
                $mes =  str_replace("%USERNAME%", $row['user'], $mes);
                
                sendMail( $ms, $row['f_name'] ." ".$row['l_name'] , $TITLE, $mes );
                $finds++;
            }
            if($finds == 0){
                $arr['res'] = "noFound";
            }else{
                $arr['res'] = "snd2Mail";
            }
            echo json_encode( $arr );
            exit(0);
        break;
    /*#################################################################*/
    case "chkMl": //Check if Mail exist
            $ms = $_POST['ms'];
            $q = "SELECT U.user, U.f_name, U.l_name ".
                " FROM ".$VR->tbl_prefix."user_contact UC LEFT JOIN ".$VR->tbl_prefix."users U ON U.id_user=UC.id_user ".
                " WHERE UC.user_mail='".$ms."' or UC.personal_mail='".$ms."'";
            $rs = $DB->query( $q );
            $finds = 0;
            while( $row = mysqli_fetch_assoc($rs) ){
                $finds++;
            }
            if($finds == 0){
                $arr['res'] = "noFound";
            }else{
                $arr['res'] = "Found";
            }
            echo json_encode( $arr );
            exit(0);
        break;
    /*#################################################################*/
    case "chkMlChgPW": //Check if Mail exist
            $ms = $_POST['ms'];
            $q = "SELECT U.user, U.f_name, U.l_name, U.id_user, md5( CURDATE( ) ) dtod, md5( subdate( current_date, 1 ) ) dyes, md5(id_user_acc) id5 ".
                " FROM ".$VR->tbl_prefix."user_contact UC LEFT JOIN ".$VR->tbl_prefix."users U ON U.id_user=UC.id_user ".
                " WHERE UC.user_mail='".$ms."' or UC.personal_mail='".$ms."' LIMIT 1";
            $rs = $DB->query( $q );
            $finds = 0;
            $usr = "";
            $uF = "";
            $uL = "";
            $id = "";
            $dTOD = "";
            while( $row = mysqli_fetch_assoc($rs) ){
                $finds++;
                $usr = $row['user'];
                $uF = $row['f_name'];
                $uL = $row['l_name'];
                $id = $row['id5'];
                $dTOD = $row['dtod'];
            }
            if($finds == 0){
                $arr['res'] = "noFound";
            }else{
                $arr['res'] = "Found";
                
                $q = "SELECT title, msg FROM tbl_elg2_mess_sys WHERE id_mess_sys = 'CHANGE_PASSWORD';";
                $rs = $DB->query( $q );
                $row = mysqli_fetch_assoc($rs);
                $TITLE = $row['title'];
                $MESSG = $row['msg'];
                $mes =  $MESSG;
                $mes =  str_replace("%FNAME%", $uF, $mes);
                $mes =  str_replace("%LNAME%", $uL, $mes);
                $mes =  str_replace("%URL_OPEN%", "<a href='".$VR->urlSYS."index.php?chkPW-3r45=".$id."&grt6yhbr=".$dTOD."'>", $mes);
                $mes =  str_replace("%URL_CLOSE%", "</a>", $mes);

                sendMail( $ms, $uF ." ".$uL , $TITLE, $mes );
            }

            echo json_encode( $arr );
            exit(0);
        break;
    /*#################################################################*/
    case "chgPw": //Change password
            $ms = $_POST['ms'];
            $pw = $_POST['pw'];
            
            
            $q = "SELECT U.id_user, U.user, U.f_name, U.l_name ".
                " FROM ".$VR->tbl_prefix."user_contact UC LEFT JOIN ".$VR->tbl_prefix."users U ON U.id_user=UC.id_user ".
                " WHERE UC.user_mail='".$ms."' or UC.personal_mail='".$ms."'";
            $rs = $DB->query( $q );
            $row1 = mysqli_fetch_assoc($rs);

            
            $q = "Update ".
                " ".$VR->tbl_prefix."users ".
                " set pass='".$pw."' ".
                " WHERE id_user=".$row1['id_user'];
            $rs = $DB->query( $q );

            
            $sub = "ELGMAP Password reset";
            $mes = "Dear: <strong>" . $row1['f_name'] ." ".$row1['l_name']."</strong><br>".
                    " Your Password was reset";
            sendMail( $ms, $row1['f_name'] ." ".$row1['l_name'] , $sub, $mes );
            
            $arr['res'] = "ready";
            
            echo json_encode( $arr );
            exit(0);
        break;

    case 'verifyUserMail':
            $ms = $_POST['ms'];
            $us = $_POST['Us'];
            $rs  = $DB->query( "select count(*) as cc from ".$VR->tbl_prefix."users where user='".$us."'" );
            $row = mysqli_fetch_assoc($rs);
            if( $row['cc'] != "0"){
                echo json_encode( array("res" => "usrExist") );
                exit(0);
            }
            /*Mail exist*/
            $rs  = $DB->query( "select count(*) as cc from ".$VR->tbl_prefix."user_contact where user_mail='".$ms."' or personal_mail='".$ms."'" );
            $row = mysqli_fetch_assoc($rs);
            if( $row['cc'] != "0"){
                echo json_encode( array("res" => "mailExist") );
                exit(0);
            }
            echo json_encode( array("res"=>"good") );
            exit(0);
        break;
    /*#################################################################*/
    case "nwAcc": //Add new Account
            $ms = $_POST['ms'];
            $us = $_POST['uS'];
            $pw = $_POST['pw'];
            $uF = $_POST['uN'];
            $uL = $_POST['lN'];
            $cords = $_POST['cords'];
            $us = strtolower($us);
            $ms = strtolower($ms);

            if( isset($_POST['uNChkMl34'])){
                if($_POST['uNChkMl34'] == "1d4Rtg1"){
                    $CcA = $_POST['cd34A'];
                    $CcU = $_POST['cd34U'];
                    $CcL = $_POST['cd34L'];
                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."accounts where id_account='".$CcA."'" );
                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."users where id_user='".$CcU."'" );
                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."user_address where id_user='".$CcU."'" );
                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."user_contact where id_user='".$CcU."'" );

                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."location where account_number='".$CcL."'" );
                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."location_addres where account_number='".$CcL."'" );
                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."location_contact_info where account_number='".$CcL."'" );
                    $rs = $DB->query( "DELETE FROM ".$VR->tbl_prefix."sensor_info where account_number='".$CcL."'" );
                }
            }
            
            /*Username exist*/
            $rs  = $DB->query( "select count(*) as cc from ".$VR->tbl_prefix."users where user='".$us."'" );
            $row = mysqli_fetch_assoc($rs);
            if( $row['cc'] != "0"){
                echo json_encode( array("res" => "usrExist") );
                exit(0);
            }
            /*Mail exist*/
            $rs  = $DB->query( "select count(*) as cc from ".$VR->tbl_prefix."user_contact where user_mail='".$ms."' or personal_mail='".$ms."'" );
            $row = mysqli_fetch_assoc($rs);
            if( $row['cc'] != "0"){
                echo json_encode( array("res" => "mailExist") );
                exit(0);
            }
            
            /*Account*/
            $rs  = $DB->query( "select (count(*)+1) as cu, DATE_FORMAT(NOW(),'%Y%m%d') as dd from ".$VR->tbl_prefix."accounts where substr(id_account,1,8)=DATE_FORMAT(NOW(),'%Y%m%d')" );
            $row = mysqli_fetch_assoc($rs);
            $conse = sprintf("%04d", $row['cu']);
            $dd = sprintf("%04d", $row['dd']);
            $acc = "$dd-$conse";


            $TCA = "";
            $TIA = "";
            $TCLA = "";
            $TILA = "";
            $TCLI = "";
            $TILI = "";
            $TCL = "";
            $TIL = "";
            foreach ($_POST as $pname => $pval) {
                if (isset($pname) && $pname != "" 
                    && strpos($pname,'_') !== false 
                    && strpos($pname,'uNew') !== false 
                    && $pval != "") 
                {
                    list($pn1, $pn2) = explode('_', $pname, 2);
                    if( isset($pn2) && $pn2 != "" ){
                        if( $pn1 == "uNewA"){
                            $TCA .= " ".$pn2.",";
                            $TIA .= " '$pval',";
                        }elseif ($pn1 == 'uNewLA'){
                            $TCLA .= " ".$pn2.",";
                            $TILA .= " '$pval',";
                        }elseif ($pn1 == 'uNewLI'){
                            $TCLI .= " ".$pn2.",";
                            $TILI .= " '$pval',";
                        }elseif ($pn1 == 'uNewL'){
                            $TCL .= " ".$pn2.",";
                            $TIL .= " '$pval',";
                        }
                    }
                }
            }
            list($cord1, $cord2) = explode(', ', $cords, 2);
            $TCLA .= " lat, lon,";
            $TILA .= " $cord2, $cord1,";

            $TCL .= " id_account, location_name,";
            $TIL .= " '$acc', '".$_POST['uNewA_name']."',";

            $UCI = "address1, address2, city, state, zip, country,";
            $UII = "'".$_POST['uNewLA_address1']."', '".$_POST['uNewLA_address2']."',".
                    " '".$_POST['uNewLA_city']."', '".$_POST['uNewLA_state']."',".
                    " '".$_POST['uNewLA_zip']."', '".$_POST['uNewLA_country']."',";
            $TCLI .= $UCI;
            $TILI .= $UII;

            $TCA = rtrim($TCA, ",");
            $TIA = rtrim($TIA, ",");
            $TCLA = rtrim($TCLA, ",");
            $TILA = rtrim($TILA, ",");
            $TCLI = rtrim($TCLI, ",");
            $TILI = rtrim($TILI, ",");
            $TCL = rtrim($TCL, ",");
            $TIL = rtrim($TIL, ",");

            $UCI = rtrim($UCI, ",");
            $UII = rtrim($UII, ",");

/*error_log("tca:".$TCA);
error_log("tia:".$TIA);

error_log("tcla:".$TCLA);
error_log("tila:".$TILA);
*/

            if($TCA == ""){
                echo json_encode( array("res" => "misInfo") );
                exit(0);
            }
            //User id_user_acc
            $UsrCode = genAccoutCODE( substr($us,0,1).substr($uF) );
            $exist = true;
            while($exist){
                $rs = $DB->query("select count(*) as cnt from  " . $VR->tbl_prefix . "user where id_user_acc='$UsrCode'");
                $row = mysqli_fetch_array($rs);
                if( $row['cnt'] == 0){
                    $exist = false;
                }else{
                    $UsrCode = genAccoutCODE( substr($us,0,1).substr($uF) );
                }
            }
            $acode = str_replace(" ", "", genAccoutCODE($_POST['uNewLA_state']));
            $exist = true;
            while($exist){
                $rs = $DB->query("select count(*) as cnt from  " . $VR->tbl_prefix . "location where account_number='$acode'");
                $row = mysqli_fetch_array($rs);
                if( $row['cnt'] == 0){
                    $exist = false;
                }else{
                    $acode = str_replace(" ", "", genAccoutCODE($_POST['uNewLA_state']));
                }
            }

            
            
            /* INSERTS */
            /* Account */
            $ins = "insert into ".$VR->tbl_prefix."accounts".
                    " (id_account, $TCA, d_creation, d_update, user_creation, main_usr, main_loc)".
                    " values( '$acc', $TIA, now(), now(), 'NEW','$UsrCode', '$acode');";
            //error_log($ins);
            $rs  = $DB->query( $ins );

            /* User */
            $in_U = "insert into " . $VR->tbl_prefix . "users ("."
                `id_account`, `user`, f_name, l_name, `d_creation`, `pass`, id_u_type, id_user_acc) ".
                " VALUES ('".$acc."', '".$us."', '".$uF."', '".$uL."', now(), '".$pw."', 'NoREG', '$UsrCode' );";
            $rs = $DB->query( $in_U );
            $rs2 = $DB->query( 
                "select id_user from " . $VR->tbl_prefix . "users ".
                " where id_account='".$acc."' and user='".$us."' limit 1" );
            $id = "";
            $idUSSS = "";
            while( $row = mysqli_fetch_assoc($rs2) ) {
                $id = $row['id_user'];
                $idUSSS = $id;
                $in_UA = "insert into " . $VR->tbl_prefix . "user_address (`id_user`,  $UCI) VALUES ('$id', $UII);";
                $in_UC = "insert into " . $VR->tbl_prefix . "user_contact (`id_user`,user_mail,home_1,cell_1) VALUES ('$id','".$ms."','".$_POST['uCt']."','".$_POST['uCl']."');";
                error_log("UA:".$in_UA);
                error_log("UC:".$in_UC);
                $rs = $DB->query( $in_UA );
                $rs = $DB->query( $in_UC );
            }

            

            $ins = "insert into ".$VR->tbl_prefix."location(account_number, $TCL) ".
                    " values('$acode', $TIL);";
            error_log($ins);
            $rs  = $DB->query( $ins );

            $ins = "insert into ".$VR->tbl_prefix."location_addres(account_number, $TCLA) values('$acode', $TILA);";
            error_log($ins);
            $rs  = $DB->query( $ins );

            $ins = "insert into ".$VR->tbl_prefix."location_contact_info(account_number, $TCLI) values('$acode', $TILI);";
            error_log($ins);
            $rs  = $DB->query( $ins );

            $ins = "insert into " . $VR->tbl_prefix . "sensor_info (`account_number`, `mfg_name`, `mfg_model_name`, `mfg_model_no`, `serv_type`, `serv_desc`, `com1_type`, `com1_service`, `com1_device_mac`, `com1_warning`, `com1_notes`, `com2_type`, `com2_service`, `com2_device_mac`, `com2_warning`, `com2_notes`, `battery_part_no`, `battery_voltage`, `phase_setting`, `phase_setting_1`, `sensor_1_premise_temp`, `sensor_2_premise_warning`, `sensor_3_premise_line_a_current`, `sensor_4_premise_line_b_current`, `sensor_5_premise_line_c_current`, `sensor_6_generator_on`, `sensor_7_gen_rotation_alert`, `sensor_8_gen_temp`, `sensor_9_gen_fuel_level`, `sensor_10_gen_oil_low_warning`, `sensor_11_gen_line_a_voltage`, `sensor_12_gen_line_b_voltage`, `sensor_13_gen_line_c_voltage`, `sensor_14_unit_temp`, `sensor_15_unit_door_open`, `sensor_16_unit_been_striked`, `sensor_17_unit_low_battery`) VALUES ('$acode', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');";
            error_log($ins);
            $rs  = $DB->query( $ins );
            

            $q = "SELECT title, msg FROM tbl_elg2_mess_sys WHERE id_mess_sys = 'USER_REGISTRATION';";
            $rs = $DB->query( $q );
            $row = mysqli_fetch_assoc($rs);
            $TITLE = $row['title'];
            $MESSG = $row['msg'];
            
            $mes =  $MESSG;
            $mes =  str_replace("%FNAME%", $uF, $mes);
            $mes =  str_replace("%LNAME%", $uL, $mes);
            $mes =  str_replace("%USERNAME%", $us, $mes);
            $mes =  str_replace("%URL_OPEN%", "<a href='".$VR->urlSYS."index.php?uReg=".md5($id)."'>", $mes);
            $mes =  str_replace("%URL_CLOSE%", "</a>", $mes);
            
            sendMail( $ms, $uF ." ".$uL , $TITLE, $mes );
            
            echo json_encode( array("res"=>"good", "cU"=>"$idUSSS", "cL"=>"$acode", "cA"=>"$acc") );
            exit(0);
        break;
    /*#################################################################*/
    default:
            echo json_encode(array("res" => "NoParms"));
            exit(0);
        break;
}

?>
