<?php
/*$phpErrFile = "php-error.log";
file_put_contents( $phpErrFile , "");
fclose($fh);

ini_set("log_errors", 1);
ini_set("error_log", $phpErrFile);
error_reporting(E_ALL);*/
error_reporting(E_ERROR | E_PARSE);

	include_once('cDBCon.php');
	include_once('cfgVars.php');
	global $DB;
	global $VR;
	
	$arr = array();
	
	if(isset($_REQUEST['f1'])){
	}else{
		$arr['err'] = "notF1";
	}
	
	switch ($_REQUEST['f1']){
		case "gtSens":
			$WHR = "";
			$WHR2 = "";
			$WHR3 = "";
			$WHR4 = "";
			if($_SESSION["isAdmin"] == false){
				$WHR = " WHERE L.id_account='".$_SESSION['IDACCOUNT']."' ";
				$WHR2 = " WHERE L.account_number in (select account_number from " . $VR->tbl_prefix . "location LL  ".$WHR." )";
                $WHR4 = " WHERE id_account='".$_SESSION['IDACCOUNT']."' ";
				$WHR3 = " WHERE account_number in (select account_number from " . $VR->tbl_prefix . "location  ".$WHR4." )";
			}

			$qL  = "select L.*, A.name as accname, M.file_image from " . $VR->tbl_prefix . "location L left join ".$VR->tbl_prefix."accounts A on L.id_account=A.id_account left join tbl_elg2_map_flags_types M ON L.id_icon_type=M.flag_id $WHR order by account_number;";
			$qLA = "select * from " . $VR->tbl_prefix . "location_addres $WHR3 order by account_number;";
			$qLC = "select * from " . $VR->tbl_prefix . "location_contact_info $WHR3 order by account_number;";
			$qSi = "select * from " . $VR->tbl_prefix . "sensor_info $WHR3 order by account_number;";

			$rsL  = $DB->query( $qL );
			$rsLA = $DB->query( $qLA );
			$rsLC = $DB->query( $qLC );
			$rsSi = $DB->query( $qSi );
			$arD = array();
			$arD['L'] = $DB->RS2ARR($rsL);
			$arD['LA'] = $DB->RS2ARR($rsLA);
			$arD['LC'] = $DB->RS2ARR($rsLC);
			$arD['SI'] = $DB->RS2ARR($rsSi);
			
			$feats = array();
			foreach( $arD['L'] as $loc){
			    $feat = array();
			    $feat['id'] = $loc['account_number'];
			    $feat['L'] = $loc;
			    foreach( $arD['LA'] as $l){
			        if($l['account_number'] == $feat['id'] ){
			            $feat['LA'] = $l;
			            break;
			        }
			    }
			    foreach( $arD['LC'] as $l){
			        if($l['account_number'] == $feat['id'] ){
			            $feat['LC'] = $l;
			            break;
			        }
			    }
			    foreach( $arD['SI'] as $l){
			        if($l['account_number'] == $feat['id'] ){
			            $feat['SI'] = $l;
			            break;
			        }
			    }
			    array_push($feats, $feat);
			}
			
			$arr['rows'] = $feats;
            //$arr["q"] = $qL;
			break;
		case "gtNotif":
			$WHERE = "";
            if($_SESSION["isAdmin"] == false){
                $WHERE .= "WHERE SS.status_web=0 and
                            SS.id_loc_account in 
                			(SELECT account_number FROM tbl_elg2_location 
                				WHERE id_account='".$_SESSION['IDACCOUNT']."' 
                				OR id_account in 
                					(SELECT id_assoc FROM tbl_elg2_user_associations 
                					WHERE id_user='".$_SESSION['uID']."' and usrassoc_type='acc'))";
                $WHERE .= " OR SS.id_loc_account in 
                			(SELECT id_assoc FROM tbl_elg2_user_associations 
                				WHERE id_user='".$_SESSION['uID']."' and usrassoc_type='loc')";
            }else{
                $WHERE = " WHERE SS.status_web=0";
            }

            $q = "SELECT SS.id_sensor_status, SS.id_loc_account, SS.d_creation,
					       GROUP_CONCAT( CONCAT(D.d_alias,' ', S.value) SEPARATOR ', ') as msgs,
					       GROUP_CONCAT( D.code SEPARATOR ', ') as codes, SS.status_web
					FROM tbl_elg2_sensor_status SS
					LEFT JOIN tbl_elg2_sensor_status_details S ON SS.id_sensor_status=S.id_sensor_status
					LEFT JOIN cat_elg2_event_codes D ON CONVERT(D.code USING utf8)=CONVERT(S.code USING utf8)
					$WHERE
					GROUP BY SS.id_sensor_status, SS.id_loc_account, SS.d_creation
					ORDER BY SS.id_sensor_status desc";
                    // LIMIT 3
//error_log($q);
			$rs = $DB->query( $q );
			$arr['rows'] = $DB->RS2ARR($rs);
			//$arr['q'] = $q;
			break;
    case "gtNotifHISTOR":
            $lid = $_POST["lid"];
			$WHERE = "WHERE SS.id_loc_account='".$lid."'";

            $q = "SELECT SS.id_sensor_status, SS.id_loc_account, SS.d_creation,
					       GROUP_CONCAT( CONCAT(D.d_alias,' ', S.value) SEPARATOR ', ') as msgs,
					       GROUP_CONCAT( D.code SEPARATOR ', ') as codes, SS.status_web
					FROM tbl_elg2_sensor_status SS
					LEFT JOIN tbl_elg2_sensor_status_details S ON SS.id_sensor_status=S.id_sensor_status
					LEFT JOIN cat_elg2_event_codes D ON CONVERT(D.code USING utf8)=CONVERT(S.code USING utf8)
					$WHERE
					GROUP BY SS.id_sensor_status, SS.id_loc_account, SS.d_creation
					ORDER BY SS.id_sensor_status desc";
                    // LIMIT 3
//error_log($q);
			$rs = $DB->query( $q );
			$arr['rows'] = $DB->RS2ARR($rs);
			//$arr['q'] = $q;
			break;
		case "gtUcfg":
			$q = "select * from " .$VR->tbl_prefix."user_map".
						" WHERE id_user=0"; //.$_SESSION['uID'];
			$rs = $DB->query( $q );
			$arr['rows'] = $DB->RS2ARR($rs);
			break;
	}
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');
	echo json_encode( $arr ); 
?>
