<?php
/*$phpErrFile = "php-error.log";
file_put_contents( $phpErrFile , "");
fclose($fh);

ini_set("log_errors", 1);
ini_set("error_log", $phpErrFile);
error_reporting(E_ALL);*/
error_reporting(E_ERROR | E_PARSE);

include_once('cDBCon.php');
include_once('cfgVars.php');
global $DB;
global $VR;

if(isset($_POST['pG'])){
    switch ($_POST['pG']){
        case 'gStates':
            $rs  = $DB->query( "select state, state_code from states order by state_code" );
            echo '<option value="" selected="selected">Select a State</option>';
            while( $row = mysqli_fetch_assoc($rs) ) {
                echo '<option value="'.$row['state_code'].'">'.$row['state'].'</option>';
            }
            break;
        case "gCities":
            $rs  = $DB->query( "select city from cities_extended where state_code='".$_POST['state']."' group by city order by city" );
            echo '<option value="" selected="selected">Select a City</option>';
            while( $row = mysqli_fetch_assoc($rs) ) {
                echo '<option value="'.$row['city'].'">'.$row['city'].'</option>';
            }
            break;
        case "gUserTypes":
            $rs  = $DB->query( "select id_u_type, description from tbl_elg2_user_types order by id_u_type, description " );
            echo '<option value="" selected="selected">Select a Type</option>';
            while( $row = mysqli_fetch_assoc($rs) ) {
                echo '<option value="'.$row['id_u_type'].'">'.$row['description'].'</option>';
            }
            break;
        case "gLocsM2M":
            $rs  = $DB->query( "select id_u_type, description from tbl_elg2_user_types order by id_u_type, description " );
            echo '<option value="" selected="selected">Select a City</option>';
            while( $row = mysqli_fetch_assoc($rs) ) {
                echo '<option value="'.$row['id_u_type'].'">'.$row['description'].'</option>';
            }
            break;
        case 'gMapFlgs':
            $rs  = $DB->query( "select flag_id,  CONCAT(flag_name, ' -  ', description) as flag_name from  tbl_elg2_map_flags_types order by flag_id" );
            echo '<option value="" selected="selected">Select a Map type</option>';
            while( $row = mysqli_fetch_assoc($rs) ) {
                echo '<option value="'.$row['flag_id'].'">'.$row['flag_name'].'</option>';
            }
            break;

/*DATA USER*/
        case 'gUsrAllInfo':
            $idU = $_POST['id'];
            
            $rs  = $DB->query( "SELECT *, (select name from tbl_elg2_accounts where id_account=U.id_account limit 1) as company_name FROM tbl_elg2_users U WHERE id_user=$idU " );
            $U = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($U, $row);
            }

            $rs  = $DB->query( "SELECT * FROM tbl_elg2_user_address WHERE id_user=$idU " );
            $UA = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($UA, $row);
            }

            $rs  = $DB->query( "SELECT * FROM tbl_elg2_user_contact WHERE id_user=$idU " );
            $UC = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($UC, $row);
            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode( 
                    array("rows" => array(
                        "U" => $U,
                        "UA"=> $UA,
                        "UC"=> $UC)
                    )
                );
            break;

/*DATA USER*/
        case 'gLocAllInfo':
            $idA = $_POST['idA'];
            $idL = $_POST['idL'];
            
            $rs  = $DB->query( "SELECT * FROM tbl_elg2_location WHERE id_account='$idA' and account_number='$idL' " );
            $U = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($U, $row);
            }

            $rs  = $DB->query( "SELECT * FROM tbl_elg2_location_addres WHERE account_number='$idL' " );
            $UA = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($UA, $row);
            }

            $rs  = $DB->query( "SELECT * FROM tbl_elg2_location_contact_info WHERE account_number='$idL' " );
            $UC = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($UC, $row);
            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode( 
                    array("rows" => array(
                        "L" => $U,
                        "LA"=> $UA,
                        "LC"=> $UC)
                    )
                );
            break;
        //"Get spesific account"
        case "gAcc";
                $ac = $_POST['q'];
                $q = "select * from tbl_elg2_accounts where (id_account like '%".$ac."%' or name like '%".$ac."%') and user_creation<>'NEW';";
                error_log($q);
                $rs  = $DB->query( $q );
                $row_set = array();
                while( $row = mysqli_fetch_assoc($rs) ) {
                    $row['value']=htmlentities(stripslashes($row['id_account']. " - ".$row['name']));
                    $row['id']=$row['id_account'];
                    $row_set[] = $row;
                }
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode( $row_set );
            break;
        //Get location from id_account
        case "gLoc";
                $loc = $_POST['q'];
                $ac = $_POST['ac'];
                $q = "select L.*, A.lat, A.lon from tbl_elg2_location L ".
                        " left join tbl_elg2_location_addres A ".
                        " on L.account_number=A.account_number " .
                        " where L.id_account='".$ac."' and L.location_name like '%".$loc."%' ;";
                error_log($q);
                $rs  = $DB->query( $q );
                $row_set = array();
                while( $row = mysqli_fetch_assoc($rs) ) {
                    $row['value']=htmlentities(stripslashes($row['account_number']. " - ".$row['location_name']));
                    $row['id']=$row['account_number'];
                    $row_set[] = $row;
                }
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode( $row_set );
            break;

//MESSAGE SYSTEM
        case "gMSGSYS":
            $rs  = $DB->query( "select * from tbl_elg2_mess_sys order by id_mess_sys" );
            $r = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($r, $row);
            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode( 
                    array("rows" => $r)
                    );
            break;
        case "sMSGSYS":
                $id = $_POST['id'];
                $msg = $_POST['msg'];
                $tit = $_POST['tet'];
                $q = "update tbl_elg2_mess_sys set 
                        msg='$msg',
                        title='$tit'
                        where id_mess_sys='".$id."'";
                $rs  = $DB->query( $q );
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode( 
                        array("rows" => "ready")
                        );
                exit(0);
            break;

        case "gEvtCods":
            $rs  = $DB->query( "select * from cat_elg2_event_codes order by code" );
            $r = array();
            while( $row = mysqli_fetch_assoc($rs) ) {
                array_push($r, $row);
            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode( 
                    array("rows" => $r)
                    );
            break;
        case "uEvtCods":
            $code = $_POST['cd'];
            $even = $_POST['ev'];
            $mins = $_POST['mi'];
            $desc = $_POST['de'];
            $q = "update cat_elg2_event_codes set ".
                            " d_alias='".$desc."', ".
                            " ev_type='".$even."', ".
                            " ev_mins=".$mins.
                            " WHERE code='".$code."'";
            $rs  = $DB->query( $q );
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode( 
                    array("rows" => "good")
                    );
            break;
        case 'ckRt4ji':
            $pw = $_POST['ft1'];
            $nPw1 = $_POST['ft2'];
            $nPw2 = $_POST['ft3'];
            $q = "select pass from tbl_elg2_users where id_user=".$_SESSION['uID'];
            $rs  = $DB->query( $q );
            $row = mysqli_fetch_assoc($rs);
            if($row['pass'] == $pw){
                $q = "UPDATE tbl_elg2_users SET pass='".$nPw1."' where id_user=".$_SESSION['uID'];
                $rs  = $DB->query( $q );
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode( array("res" => "PwUpdated") );
            }else{
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode( array("res" => "PwNoCorrs") );
            }
            break;
    }
}
?>
