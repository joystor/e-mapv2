<?php
/*$phpErrFile = "php-error.log";
file_put_contents( $phpErrFile , "");
fclose($fh);

ini_set("log_errors", 1);
ini_set("error_log", $phpErrFile);
error_reporting(E_ALL);*/
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
session_start();


/*
$table = 'datatables_demo';
$primaryKey = 'id';
$columns = array(
    array('db' => 'first_name', 'dt' => 0
    ),
    array('db' => 'last_name', 'dt' => 1
    ),
    array('db' => 'position', 'dt' => 2
    ),
    array('db' => 'office', 'dt' => 3
    ),
    array('db' => 'start_date', 
          'dt' => 4,
          'formatter' => getDateFormat($d, $row)
    ),
    array('db' => 'salary', 
          'dt' => 5,
          'formatter' => getDateFormat($d, $row)
    )
);*/

$WHERE = "";

switch ($_REQUEST['tb']){
    case "gtAccounts":
        $table = 'tbl_elg2_accounts';
        $primaryKey = 'id_account';
        $columns = array(
            array('db' => 'id_account', 'dt' => 0
            ),
            array('db' => 'name', 'dt' => 1
            ),
            array('db' => 'd_creation', 'dt' => 2, 'formatter' => getDateTimeFormat($d, $row)
            ),
            array('db' => 'd_update', 'dt' => 3, 'formatter' => getDateTimeFormat($d, $row)
            )
        );
        if( $_SESSION['uType'] == "FV1" ){
            $WHERE = " id_account='".$_SESSION['IDACCOUNT']."' ";
        }
        break;
    case "gtSubAccounts":
        $table = 'tbl_elg2_location';
        $WHERE = " id_account='".$_POST['acc']."' ";
        $primaryKey = 'account_number';
        $columns = array(
            array('db' => 'account_number', 'dt' => 0
            ),
            array('db' => 'location_name', 'dt' => 1
            ),
            array('db' => 'd_update', 'dt' => 2, 'formatter' => getDateTimeFormat($d, $row)
            )
        );
        break;
    case "gtSubUsr":
        $table = 'tbl_elg2_users';
        $WHERE = " id_account='".$_POST['acc']."' ";
        $primaryKey = 'id_user';
        $columns = array(
            array('db' => 'id_user_acc', 'dt' => 0
            ),
            array('db' => 'user', 'dt' => 1
            ),
            array('db' => 'f_name', 'dt' => 2
            ),
            array('db' => 'l_name', 'dt' => 3
            ),
            array('db' => 'id_u_type', 'dt' => 4
            ),
            array('db' => 'd_update', 'dt' => 5, 'formatter' => getDateTimeFormat($d, $row)
            ),
            array('db' => 'id_user', 'dt' => 6
            ),
        );
        break;
    case "gtAdmOnlUsr":
            $table = '(SELECT U.id_account as id_account, U.id_user as id_user, '.
                     ' user, f_name, l_name, id_u_type, user_mail, d_update, id_user_acc '.
                     ' FROM tbl_elg2_users as U '.
                     ' LEFT JOIN tbl_elg2_user_contact as UC '.
                     ' ON U.id_user=UC.id_user'.
                     ') T ';
            $WHERE = " id_account<>'".$_SESSION['IDACCOUNT']."' ";
            /*$table = 'tbl_elg2_users as U '.
                     ' LEFT JOIN tbl_elg2_user_contact as UC '.
                     ' ON U.id_user=UC.id_user';*/
            $primaryKey = 'id_user';
            $columns = array(
                array('db' => 'id_account', 'dt' => 0
                ),
                array('db' => 'id_user_acc', 'dt' => 1
                ),
                array('db' => 'user', 'dt' => 2
                ),
                array('db' => 'f_name', 'dt' => 3
                ),
                array('db' => 'l_name', 'dt' => 4
                ),
                array('db' => 'id_u_type', 'dt' => 5
                ),
                array('db' => 'user_mail', 'dt' => 6
                ),
                array('db' => 'd_update', 'dt' => 7, 'formatter' => getDateTimeFormat($d, $row)
                ),
                array('db' => 'id_user', 'dt' => 8
                )
            );
        break;
    case "gtAdmOnlLocs":
            $table = 'tbl_elg2_location';
            /*$table = 'tbl_elg2_users as U '.
                     ' LEFT JOIN tbl_elg2_user_contact as UC '.
                     ' ON U.id_user=UC.id_user';*/
            $primaryKey = 'id_account';
            $columns = array(
                array('db' => 'id_account', 'dt' => 0
                ),
                array('db' => 'account_number', 'dt' => 1
                ),
                array('db' => 'location_name', 'dt' => 2
                ),
                array('db' => 'id_type_of_site', 'dt' => 3
                ),
                array('db' => 'id_icon_type', 'dt' => 4
                )
            );
            break;
    case 'gtUsrAssoc':
            $table = 'tbl_elg2_users U '.
                        ' LEFT JOIN tbl_elg2_user_types T ON U.id_u_type = T.id_u_type';
            $primaryKey = 'id_user';
            //id_user, user, f_name, l_name, U.id_u_type, id_user_acc, d_creation, description
            $columns = array(
                array('db' => 'id_user_acc', 'dt' => 0 ),
                array('db' => 'f_name', 'dt' => 1 ),
                array('db' => 'l_name', 'dt' => 2 ),
                array('db' => 'user', 'dt' => 3 ),
                array('db' => 'description', 'dt' => 4 ),
                array('db' => 'd_creation', 'dt' => 5, 'formatter' => getDateTimeFormat($d, $row) ),
                array('db' => 'id_user', 'dt' => 6 )
            );
            $WHERE = " U.id_u_type <> 'NoREG' ";
            break;
    case 'gtUsrAssocLoc':
            $table = "tbl_elg2_user_associations UA ".
                        " LEFT JOIN tbl_elg2_location L ON L.account_number = UA.id_assoc and UA.usrassoc_type='loc'";
            $primaryKey = 'L.id_account';
            $columns = array(
                array('db' => 'id_account', 'dt' => 0 ),
                array('db' => 'account_number', 'dt' => 1 ),
                array('db' => 'location_name', 'dt' => 2 ),
                array('db' => 'id_type_of_site', 'dt' => 3 ),
                array('db' => 'id_icon_type', 'dt' => 4 )
            );
            $WHERE = " UA.id_user = '".$_POST['idU']."' ";
            break;
    case 'gtUsrAssocAcc':
            $table = "(SELECT A.id_account, A.name ".
                        " FROM tbl_elg2_user_associations UA ".
                        " LEFT JOIN tbl_elg2_accounts A ON A.id_account=UA.id_assoc and UA.usrassoc_type='acc' ".
                        " WHERE UA.id_user = '".$_POST['idU']."' ".
                        " ) T1";
            $primaryKey = 'id_account';
            //id_user, user, f_name, l_name, U.id_u_type, id_user_acc, d_creation, description
            $columns = array(
                array('db' => 'id_account', 'dt' => 0 ),
                array('db' => 'name', 'dt' => 1 )
            );
            break;
    default:
        exit(0);
}


include_once('cDBCon.php');
global $DB;
// SQL server connection information
/*$sql_details = array(
    'user' => '',
    'pass' => '',
    'db' => '',
    'host' => ''
);*/
$sql_details = $DB->getSQLDetails();


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require('libs/ssp.class.php');
echo json_encode(SSP::simple($_REQUEST, $sql_details, $table, $primaryKey, $columns, $WHERE));





function getDateFormat($d, $row){
  return date('jS M y', strtotime($d));
}
          
function getDateTimeFormat($d, $row){
    //error_log("date:".$d);
    return date("m/d/Y H:i", strtotime($d));
}
?>
