<?php
#$phpErrFile = "bk/php-error.log";
#file_put_contents( $phpErrFile , "");
#fclose($phpErrFile);

ini_set("log_errors", 0);
#ini_set("error_log", $phpErrFile);
#error_reporting(E_ALL);

    $tLog = "";
	if(isset($_POST['uL'])){
		include('bk/cDBCon.php');
		include('bk/cfgVars.php');
		global $DB;
		global $VR;
		
		$q = "select count(*) as many from " . 
					$VR->tbl_prefix . "users ".
					"where user='".$_POST['uL']."' and pass='".$_POST['uP']."';";
		$rs = $DB->query( $q );
		$ar = $DB->RS2ARR($rs);
		
		if( $ar[0]["many"] > 0 ){
			$tLog = true;
			$rs = $DB->query( "SELECT * 
								FROM ".$VR->tbl_prefix."users U 
								WHERE U.user='".$_POST['uL']."' LIMIT 1" );
			//LEFT JOIN ".$VR->tbl_prefix."permission P ON P.id_user=U.id_user
			$ar = $DB->RS2ARR($rs);
            
            if($ar[0]["id_u_type"]=='NoREG'){
                $tLog = "ErrNoREG";
            }else{
                if(isset($_SESSION['uID'])){
                    session_destroy();
                    session_start();
                    //session_regenerate_id(FALSE);
                    //session_unset();
                }
                $_SESSION['user'] = $_POST['uL'];
                $_SESSION['uID'] = $ar[0]["id_user"];
                $_SESSION['IDACCOUNT'] = $ar[0]["id_account"];
                $_SESSION['uType'] = $ar[0]["id_u_type"];
                $_SESSION['uName'] = $ar[0]["f_name"];
                $_SESSION['uLname'] = $ar[0]["l_name"];
                $_SESSION['uMail'] = ""; //$ar[0]["mail"];
                $_SESSION['uMovil'] = ""; //$ar[0]["mobile"];
                if($ar[0]["id_u_type"] == "Maste"){
                    $_SESSION['isAdmin'] = true; //$ar[0]["isadmin"];
                }else{
                    $_SESSION['isAdmin'] = false;
                }
                $_SESSION['uMovil'] = "";
                header('Location:rmap.php');
                exit;
            }
		}else{
            $tLog = "ErrLogin";
        }
	}
    
    /*NEW USER*/
    if(isset($_GET['uReg'])){
        include('bk/cDBCon.php');
		include('bk/cfgVars.php');
        $rs = $DB->query( "UPDATE ".$VR->tbl_prefix."users ".
                            " set id_u_type='FV1'" .
                            " WHERE md5(id_user)='".$_GET['uReg']."'" );

        $rs = $DB->query( "UPDATE ".$VR->tbl_prefix."accounts ".
                            " set user_creation='REG'" .
                            " WHERE id_account = (select id_account from ".$VR->tbl_prefix."users where md5(id_user)='".$_GET['uReg']."')" );
        $tLog = "RegOK";
    }


    $Mail2Reset = "";
    /*CHANGE PASSWORD*/
    if(isset($_GET['chkPW-3r45']) && isset($_GET["grt6yhbr"])){
        include('bk/cDBCon.php');
        include('bk/cfgVars.php');
        global $DB;
        global $VR;

        $q = " SELECT A.user_mail umail, md5( CURDATE( ) ) dtod, md5( subdate( current_date, 1 ) ) dyes
                FROM tbl_elg2_users U
                LEFT JOIN tbl_elg2_user_contact A
                ON U.id_user=A.id_user 
                WHERE md5(U.id_user_acc)='".$_GET['chkPW-3r45']."';";
        $rs = $DB->query( $q );
        $rowMailCHK = mysqli_fetch_assoc($rs);
        $Mail2Reset = $rowMailCHK['umail'];
        $dTOD = $rowMailCHK['dtod'];
        $dYES = $rowMailCHK['dyes'];
        if( $_GET["grt6yhbr"] == $dTOD || $_GET["grt6yhbr"] == $dYES){
            $tLog = "ChangePWD";
        }else{
            $tLog = "ChangePWDOUT";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--link rel="shortcut icon" href="../../assets/ico/favicon.ico"-->

    <title>ELGMAP Login</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="js/libs/bootstrap-3.1.1/css/bootstrap.min.css">
    <link href="css/signin.css" rel="stylesheet">
    
    <script type='text/javascript' src='js/libs/md5.js'></script>
	<script type='text/javascript' src='js/libs/jquery-1.11.0.min.js'></script>
    <script type='text/javascript' src='js/libs/jquery-ui-1.10.4/js/jquery-ui-1.10.4.min.js'></script>
    <script src="js/libs/bootstrap-3.1.1/js/bootstrap.min.js"></script>
    <script type='text/javascript' src='js/idx.js'></script>

    <script type='text/javascript' src='js/libs/openlayers-2.13.1/OpenLayers.js'></script>
    <script type='text/javascript' src='js/libs/bootbox.min.js'></script>
    
    
</head>
<body>

        
        
<div class="container login-container" style="max-width:500px">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="login-title">Emergency Logistic Grid </h1>
        </div>
        
        <div id="dvLogCnt" class="panel-body">
            

            <form id="frmLogin" method="POST">
                <!--h2 class="form-signin-heading">Please sign in</h2-->
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                    </span>
                    <input name="uL" id="uL" type="text" class="form-control" placeholder="Username" value="elgadmin" required autofocus>
                </div>
                <h3 class="input-title"></h3>
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-ok-circle"></span>
                    </span>
                    <input name="uP" id="uP" type="password" class="form-control" value="elgadmin" placeholder="Password" required>
                </div>
                <hr>
                <!--label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                </label-->
                <button class="btn btn-lg btn-primary btn-block" type="submit" onclick="$('#uP').val( hex_md5($('#uP').val()) )">Log In</button>
                <hr class="divier">
                <div class="form-group pull-right">
                <?php 
                    if($tLog=="ErrLogin"){ 
                        ?>
                        <div id="divMSGERR" class="alert alert-danger" style="position: fixed; right: 30px; top: 30px;">
                            Error in user or password
                        </div>
                        <script>
                            setTimeout(function(){
                                $( "#dvLogCnt" ).effect( "highlight", {}, 1000, function(){
                                    $( "#dvLogCnt" ).effect( "highlight", {}, 1000, function(){});
                                });
                            }, 500);
                            setTimeout(function(){
                                $("#divMSGERR").fadeOut();
                            }, 6000);
                        </script>
                        <br>
                        <?php
                    }else if($tLog == "ErrNoREG"){
                        ?>
                        <div id="divMSGERR" class="alert alert-danger" style="position: fixed; right: 30px; top: 30px;">
                            Please confirm the Registration in your mail
                        </div>
                        <script>
                            setTimeout(function(){
                                $( "#dvLogCnt" ).effect( "highlight", {}, 1000, function(){
                                    $( "#dvLogCnt" ).effect( "highlight", {}, 1000, function(){});
                                });
                            }, 500);
                            setTimeout(function(){
                                $("#divMSGERR").fadeOut();
                            }, 6000);
                        </script>
                        <br>
                        <?php
                    ############################################################################################
                    }else if($tLog == "RegOK"){
                        ?>
                        <div id="divMSGERR" class="alert alert-success" style="position: fixed; right: 30px; top: 30px;">
                            Your Registration correct, please login with your new account
                        </div>
                        <br>
                        <script>
                            setTimeout(function(){
                                $( "#dvLogCnt" ).effect( "highlight", {}, 1000, function(){
                                    $( "#dvLogCnt" ).effect( "highlight", {}, 1000, function(){});
                                });
                            }, 500);
                            setTimeout(function(){
                                $("#divMSGERR").fadeOut();
                            }, 6000);
                        </script>
                    <?php
                    ############################################################################################
                    }else if(  $tLog == "ChangePWD"){
                        ?>
                            <div id="divMSGERR" class="alert alert-success" style="position: fixed; right: 30px; top: 30px;">
                                Please change your password
                            </div>
                            <br>
                            <script>
                            setTimeout(function(){
                                $("#uLM").val( "<?php echo $Mail2Reset; ?>" );
                                $("#gtChkPwdMsg").html("");
                                $("#modChkPwd").modal("show");
                            },500);
                            setTimeout(function(){
                                $("#divMSGERR").fadeOut();
                            }, 6000);
                            </script>
                    <?php
                    ############################################################################################
                    }else if(  $tLog == "ChangePWDOUT"){
                        ?>
                         <div id="divMSGERR" class="alert alert-warning" style="position: fixed; right: 30px; top: 30px;">
                                This link is expired
                            </div>
                            <br>
                            <script>
                            setTimeout(function(){
                                $("#divMSGERR").fadeOut();
                            }, 6000);
                            </script>
                    <?php
                    }
                    ?>
                    <a href="#" onclick="openNewUser();" data-toggle="modal" data-target="#divNewUsrFrm">Sign up now</a>
                    <br>
                    <a href="#" onclick="forgetUser()" data-toggle="modal" data-target="#modFogUsr">Forgot User/Password?</a>
                </div>
            </form>
			
        </div>
    </div>
</div>	
				<!--div class="col-md-4">
				</div-->

        <div id="divNewUsrFrm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="modSensTitle">New User</h4>
                    </div>
                    <div class="modal-body">
        
    <!--   USER INFO  -->
                        <div id="divNUPersonal" class="form-horizontal" role="form">


                            <div class="form-group">
                                <label for="uNewUN" class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-user"></span>
                                        </div>
                                        <input id="uNewUN" type="text" class="form-control" placeholder="First Name" required autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewUL" class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-user"></span>
                                        </div>
                                        <input id="uNewUL" type="text" class="form-control" placeholder="Last Name" required autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="uNewUs_home_1" class="col-sm-3 control-label">Contact Tel</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-phone-alt"></span>
                                        </div>
                                        <input id="uNewUs_home_1" type="tel" class="form-control" placeholder="000 000 0000" required autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewUs_cell_1" class="col-sm-3 control-label">Contact Cell</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-earphone"></span>
                                        </div>
                                        <input id="uNewUs_cell_1" type="tel" class="form-control" placeholder="000 000 0000" required autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="uNewUMl" class="col-sm-3 control-label">email</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-envelope"></span>
                                        </div>
                                        <input id="uNewUMl" type="email" class="form-control" placeholder="mail@server.com" required autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="uNewUSR" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-user"></span>
                                        </div>
                                        <input id="uNewUSR" type="text" class="form-control" placeholder="Username" required autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="uPwN1" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-certificate"></span>
                                        </div>
                                        <input id="uPwN1" type="password" class="form-control" placeholder="Password" required autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="uPwN2" class="col-sm-3 control-label">Repeat Password</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-certificate"></span>
                                        </div>
                                        <input id="uPwN2" type="password" class="form-control" placeholder="Repeat Password" required autofocus>
                                    </div>
                                </div>
                            </div>


                            
                        </div>

    <!--   MAP INFO  -->                    
                        <div id="divNUCompany" class="form-horizontal" role="form" style="display:none">


                            <hr>
                            <div id="mapContainer">
                            <h4>Click on the map for add a location</h4>
                                <div id="idxMap" style="width: 300px;height: 200px;"></div>
                                <input type="hidden" id="inpIdxCoordsLocs"></input>
                            </div>
                        </div>

    <!--   COMPANY INFO  -->
                        <div id="divNUCompanyInfo" class="form-horizontal" role="form" style="display:none">

                            <div class="form-group">
                                <label for="uNewA_name" class="col-sm-3 control-label">Company name</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-briefcase"></span>
                                        </div>
                                        <input id="uNewA_name" type="text" class="form-control" placeholder="Company name" required autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="uNewLA_address1" class="col-sm-3 control-label">Address1</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-home"></span>
                                        </div>
                                        <input id="uNewLA_address1" type="text" class="form-control" placeholder="Address1" required autofocus>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="uNewLA_address2" class="col-sm-3 control-label">Address2</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-home"></span>
                                        </div>
                                        <input id="uNewLA_address2" type="text" class="form-control" placeholder="Address2" autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewLA_city" class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-globe"></span>
                                        </div>
                                        <input id="uNewLA_city" type="text" class="form-control" placeholder="City" required autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewLA_state" class="col-sm-3 control-label">State</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-globe"></span>
                                        </div>
                                        <input id="uNewLA_state" type="text" class="form-control" placeholder="State" required autofocus>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="uNewLA_zip" class="col-sm-3 control-label">Zip</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-globe"></span>
                                        </div>
                                        <input id="uNewLA_zip" type="number" class="form-control" placeholder="Zip" required autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewLA_country" class="col-sm-3 control-label">Country</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-briefcase"></span>
                                        </div>
                                        <input id="uNewLA_country" type="text" class="form-control" placeholder="Country" required autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewLI_bus_tel_1" class="col-sm-3 control-label">Company Phone</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-phone-alt"></span>
                                        </div>
                                        <input id="uNewLI_bus_tel_1" type="tel" class="form-control" placeholder="000 000 0000" required autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewLI_fax_1" class="col-sm-3 control-label">Company Fax</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-print"></span>
                                        </div>
                                        <input id="uNewLI_fax_1" type="tel" class="form-control" placeholder="Company Fax" autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewLI_bus_mail" class="col-sm-3 control-label">Company email</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-envelope"></span>
                                        </div>
                                        <input id="uNewLI_bus_mail" type="email" class="form-control" placeholder="Company email" autofocus>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="uNewL_id_icon_type" class="col-sm-3 control-label">Location Type</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                        </div>
                                        <select id="uNewL_id_icon_type"  class="form-control">
                                            <option value="Retail">Retail</option>
                                            <option value="Commercial">Commercial</option>
                                            <option value="Medical Facility">Medical Facility</option>
                                            <option value="Municipal Facility">Municipal Facility</option>
                                            <option value="Cell Tower">Cell Tower</option>
                                        </select>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="uNewL_id_type_of_site" class="col-sm-3 control-label">Icon Type</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                        </div>
                                        <select id="uNewL_id_type_of_site"  class="form-control">
                                            <option value="Tear">Tear</option>
                                            <option value="Cross">Cross</option>
                                            <option value="GasPump">GasPump</option>
                                            <option value="Tower">Tower</option>
                                            <option value="Building">Building</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            
                        </div>
                        
                        <div id="gtNewAccMsg">
                        </div>
                        
                        
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <span class="pull-right">
                                <button id="btnNewUsrBack" type="button" class="btn btn-success" disabled>Back</button>
                                <button id="btnNewUsr" type="button" class="btn btn-success">Next</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        
        
        
        <div id="modFogUsr" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width:500px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="modSensTitle">Recover your Account</h4>
                    </div>
                    <div class="modal-body">
        
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </span>
                            <input name="uForM" id="uLM" type="email" class="form-control" placeholder="mail@server.com" required autofocus value="<?php echo $Mail2Reset;?>">
                        </div>
                        
                        <div id="gtAccMsg">
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <span class="pull-right">
                                <button id="btnForgGetAccoutn" type="button" class="btn btn-success">Get User Account</button>
                                <button id="btnForgPaswd" type="button" class="btn btn-warning">Reset Password</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        
        
        <div id="modChkPwd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width:500px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Change Password</h4>
                    </div>
                    <div class="modal-body">
        
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-certificate"></span>
                            </span>
                            <input id="uPw1" type="password" class="form-control" placeholder="Password" required autofocus>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-certificate"></span>
                            </span>
                            <input id="uPw2" type="password" class="form-control" placeholder="Repeat Password" required autofocus>
                        </div>
                        
                        <div id="gtChkPwdMsg">
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <span class="pull-right">
                                <button id="btnChkPaswd" type="button" class="btn btn-warning">Change Password</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
		</div>
</body>
</html>
