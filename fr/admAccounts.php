<div class="modal fade" id="modAdmAccounts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modSensTitle">Account Admin</h4>
			</div>
			<div class="modal-body">
                <h5>Master Account</h5>
                
                <hr class="divider"/>
                <table id="tblAdAcc" class="display" cellspacing="0" width="100%">
                    <thead>
                        <th>Account</th>
                        <th>Name</th>
                        <th>Date Created</th>
                        <th>Date Modified</th>
                    </thead>
                </table>
                <?php
                if($_SESSION['isAdmin'] || $_SESSION['uType'] != "FV1" ){
                ?>
                <hr class="divider"/>
                <div class="row">
                    <div class="col-xs-8">
                        <span class="pull-left">
                            <button id="btnAccDel" type="button" class="btn btn-danger">Delete</button>
                            <button id="btnAccEdi" type="button" class="btn btn-info">Edit</button>
                            <button id="btnAccNew" type="button" class="btn btn-success" data-toggle="modal" data-target="#modAccountDiv" >Add Account</button>
                        </span>
                    </div>
                </div>
                <?php
                }
                ?>
                <hr class="divider"/>
                
                
                <div id="divAccRelSubAcc" class="" style="display:none">
                    <div class="">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#aTbLocs" data-toggle="tab">Location Accounts</a></li>
                            <li><a href="#aTbUsrs" data-toggle="tab">Users Accounts</a></li>
                        </ul>
                        <div class="tab-content">

                            <!--# Locations #-->
                            <div class="tab-pane active" id="aTbLocs">
                                <hr class="divider"/>
                                <table id="tblAdLocAcc" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Date Modified</th>
                                    </thead>
                                </table>
                                <?php
                                //if($_SESSION['isAdmin'] || $_SESSION['uType'] != "FV1" ){
                                ?>
                                <hr class="divider"/>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <span class="pull-left">
                                            <button id="btnAdmLocDel" type="button" class="btn btn-danger">Delete</button>
                                            <button id="btnAdmLocEdt" type="button" class="btn btn-info" data-toggle="modal" data-target="#modSensDial">Edit</button>
                                            <button id="btnSubAccAddNew" type="button" class="btn btn-success" onclick="oELG.bInsNewLocbyADM=true;addNewSensor();" data-toggle="modal" data-target="#modSensDial">Add New Location</button>
                                        </span>
                                    </div>
                                </div>
                                <?php
                                //}
                                ?>
                            </div>
                            <!--# Users #-->
                            <div class="tab-pane" id="aTbUsrs">

                                <hr class="divider"/>
                                <table id="tblAdUsers" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Type</th>
                                        <th>Date Modified</th>
                                    </thead>
                                </table>
                                <hr class="divider"/>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <span class="pull-left">
                                            <button id="btnAdmUsrDel" type="button" class="btn btn-danger">Delete</button>
                                            <button id="btnAdmUsrEdt" type="button" class="btn btn-info">Edit</button>
                                            <button id="" type="button" class="btn btn-success" onclick="addNewUser();" data-toggle="modal" data-target="#modAccount">Add New User</button>
                                        </span>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-3">
                        <span class="pull-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
