<div id="navMenusTop" class="navbar navbar-default navbar-fixed-top" style="top:3px;display:none">
	<!--div class="container-fluid"-->

	<div class="navbar-header" id="logoLeft" style="display:none">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">
			<span>ELG</span>
		</a>
	</div>

	


	<div class="navbar-collapse collapse navbar-responsive-collapse">	
		<ul class="nav navbar-nav">
			<!--######################################################################################-->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="toggleControl('line');">Measure distance</a>
					</li>
					<li><a href="#" onclick="toggleControl('polygon');">Measure area</a>
					</li>
				</ul>
			</li>
			<!--######################################################################################-->
			<li class="dropdown notClose">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Map <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="olMap.zoomToExtent(vLstations.getDataExtent());">
						<span style="font-size: 1.2em;" class="glyphicon glyphicon-globe" ></span>
						Zoom Extend</a>
					</li>
					<li class="divider"></li>
					<li id="lstMapLayers" style="padding-left: 15px;"></li>
				</ul>
			</li>
			<!--######################################################################################-->
			<li class="dropdown notClose">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Status View <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li style="padding-left: 15px;">
						<strong>Permise locations</strong><br>
						<input type="checkbox" name="rdPermLoc" value="Retail" checked>Retail<br>
						<input type="checkbox" name="rdPermLoc" value="Commercial" checked>Commercial<br>
						<input type="checkbox" name="rdPermLoc" value="Medical Facility" checked>Medical Facility<br>
						<input type="checkbox" name="rdPermLoc" value="Municipal Facility" checked>Municipal Facility<br>
						<input type="checkbox" name="rdPermLoc" value="Cell Tower" checked>Cell Tower<br>
						<strong>Window View</strong><br>
						<!--input type="radio" name="rdWView">Alert / Notification<br-->
						<input type="checkbox" name="rdWView" value="alert">Alert Only<br>
						<input type="checkbox" name="rdWView" value="notif">Notification Only<br>
						<!--input type="radio" name="rdWView" value="">Filter bar<br>
						<input type="radio" name="rdWView" value="">Filter window<br-->
						<input type="checkbox" name="rdWView" value="chart">Chart window<br>
					</li>
				</ul>
			</li>
			
		</ul>


		<!--######################################################################################-->
		<div style="display:none"> <!-- class='visible-lg'  -->
			<div class="navbar-form navbar-left" role="search">
				
				<div class="form-group">
					<input id="inpFindLoc" type="text" class="form-control" placeholder="Search">
				</div>
				<button id="btnFindIt" type="submit" class="btn btn-default">New Search</button>
			</div>

			<ul id="ulLastSearch" class="nav navbar-nav" style="display:none">
				<li>
					<a id="showLastFound" href="#">Last
					<span style="font-size: 1.2em;" class="glyphicon glyphicon-search" ></span>
			  		</a>
				</li>
			</ul>
		</div>
		<!--######################################################################################-->
		<div>
			<ul class="nav navbar-nav">
				<li id="menDroSearch" class="dropdown notClose">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span style="font-size: 1.2em;" class="glyphicon glyphicon-search"></span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu notClose" style="width:250px">
						<li>
							<input id="inpFindLocGG" type="text" class="form-control" placeholder="Search" style="width: 95%; margin: auto;">
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<!--######################################################################################-->


		
		<ul id="imgLoading" class="nav navbar-nav" style="disaplay:none">
			<!--######################################################################################-->
			<li >
				<span><img src="img/ajax-loader.gif"></span>
			</li>
		</ul>

		<!--############################### TITLE LOGO #######################################################-->
		<!--ul class="nav navbar-nav navbar-justified">
			<li>
				<a href="#" class="LogoTXT">
					<span class="LogoFirstLetterR">E</span>MERGENCY <span class="LogoFirstLetterR">L</span>OGISTICS <span class="LogoFirstLetterR">G</span>RID
				</a>
			</li>
		</ul-->
		
		
		
		
		
		<ul class="nav navbar-nav navbar-right">
			<!--################################ USER INFO ######################################################-->
			<li>
				<div id="timeSRV">
				<?php
					echo $_SESSION['user'];
				?>
				</div>
			</li>
			
			<!--############################### MAILS #######################################################-->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span style="font-size: 1.2em;" class="glyphicon glyphicon-envelope"></span>
				</a>
			</li>
			
			<!--############################### ALERTS #######################################################-->
			<li class="dropdown">
				<a id="lnk2Alerts" href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span style="font-size: 1.2em;" class="glyphicon glyphicon-fire"></span>
					<span id="bdgAlert" class="badge">0</span>
					<b class="caret"></b>
				</a>
			</li>
			
			<!--############################### NOTIFS #######################################################-->
			<li class="dropdown">
				<a id="lnk2Notif" href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span style="font-size: 1.2em;" class="glyphicon glyphicon-bell"></span>
					<span id="bdgNotif" class="badge">0</span>
					<b class="caret"></b>
				</a>
			</li>
			
			<!--############################### CONFIG #######################################################-->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span style="font-size: 1.2em;" class="glyphicon glyphicon-cog"></span>
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					

                    <?php
                    	/*ASSGIN MENU CONFIGURATION PERMITIONS FOR EVERY USER*/
							if($_SESSION['isAdmin']){
								?>
								<li><a href="#" onclick="tblAdAcc.fnDraw();" data-toggle="modal" data-target="#modAdmAccounts">Admin Accounts</a>
								<li><a href="#" onclick="" data-toggle="modal" data-target="#modUsrAssoc">Users Assosiations</a>
								<li><a href="#" onclick="fLoadEventCodes()" data-toggle="modal" data-target="#modAdmEvenCodes">Admin Event CODES</a>
								<li><a href="#" onclick="fOpenMESSSYSS();" data-toggle="modal" data-target="#modMessSYS">System Messages</a>
								<?php
							}
							if( $_SESSION['uType'] != "FV1" ){
								?>
								<li><a href="#" onclick="" data-toggle="modal" data-target="#modAdmOnlLocs">Admin Locations</a>
								<li><a href="#" onclick="" data-toggle="modal" data-target="#modAdmOnlUsers">Admin Users</a>
								<li><a href="#" onclick="oELG.bInsNewLocbyADM=false;addNewSensor();" data-toggle="modal" data-target="#modSensDial">Add New Location</a>
								<li><a href="#" onclick="tblAdAcc.fnDraw();" data-toggle="modal" data-target="#modAdmAccounts">Admin my Accounts</a>
								<?php
							}else{
								?>
								<li><a href="#" onclick="tblAdAcc.fnDraw();" data-toggle="modal" data-target="#modAdmAccounts">Admin my Accounts</a>
								<?php
							}
						?>

                    <!--li><a href="#" onclick="" data-toggle="modal" data-target="#modAdmLocationDial">Admin Locations</a-->
					<!--li><a href="#" onclick="oELG.bInsNewUserbyADM = false;" data-toggle="modal" data-target="#modUsrDial">Accounts</a-->
					<li><a data-toggle="modal" data-target="#modAccount" href="#" onclick="modifyMyAccountInfo()">My Account</a> 
					</li>   <!--  onclick="showUserCFG()" id="openUserConfig" -->
					<li class="divider"></li>
					<li><a href="bk/stLog.php">Logout</a>
					</li>
				</ul>
			</li>
			
			<!--############################### HELP #######################################################-->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					Help
					<b class="caret"> </b>
				</a>
				<ul class="dropdown-menu">
					</li>
					<li>
						<a href="<?php
							if($_SESSION['isAdmin']){
								echo "help/helpAdmin.php";
							}else{
								echo "help/helpUser.php";
							}
						?>" target="_blank">
							<?php
								if($_SESSION['isAdmin']){
									echo "Admin Help";
								}else{
									echo "User Help";
								}
							?>
						</a>
					</li>
				</ul>
			</li>
			<li>
			</li>
			
		</ul>

	</div>
	<!-- /.navbar-collapse -->



	<!--/div-->
</div>
<div class="LogoTXT" style="display:none">
	<span class="LogoFirstLetterR">E</span><span class="LogoText">MERGENCY</span> <span class="LogoFirstLetterR">L</span><span class="LogoText">OGISTICS</span> <span class="LogoFirstLetterR">G</span><span class="LogoText">RID</span>
</div>



<div id="divMeasuDial" style="display:none;position:absolute; left: 300px; z-index: 1; top:100px;background-color: #fff; width: 200px; height: 40px; border-radius: 10px; text-align: center;">
	<div id="inpValMeasure"></div>
	<button type="button" class="btn btn-default btn-xs" onclick="$('#divMeasuDial').fadeOut();toggleControl('nan');" style="position: absolute;top: -10px; right: -10px;">
	  <span class="glyphicon glyphicon-remove-circle"></span>
	</button>
</div>
