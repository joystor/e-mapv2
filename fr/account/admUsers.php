<div class="modal fade" id="modAdmOnlUsers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modSensTitle">Users Admin</h4>
            </div>
                <div class="modal-body">
                    <h5>Users Account</h5>

                    <table id="tblAdmOnlUsers" class="display" cellspacing="0" width="100%">
                        <thead>
                            <th>ID Account</th>
                            <th>ID User</th>
                            <th>User</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Type</th>
                            <th>User mail</th>
                            <th>Last update</th>
                        </thead>
                    </table>

                    <hr class="divider"/>
                    <div class="row">
                        <div class="col-xs-8">
                            <span class="pull-left">
                                <button id="btnAdmOnlyUsrDel" type="button" class="btn btn-danger">Delete</button>
                                <button id="btnAdmOnlyUsrEdt" type="button" class="btn btn-info">Edit</button>
                            </span>
                        </div>
                    </div>


                </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-3">
                        <span class="pull-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>