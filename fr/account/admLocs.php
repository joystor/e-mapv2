<div class="modal fade" id="modAdmOnlLocs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modSensTitle">Locations Admin</h4>
            </div>
                <div class="modal-body">
                    <h5>Locations Account</h5>

                    <table id="tblAdmOnlLocs" class="display" cellspacing="0" width="100%">
                        <thead>
                            <th>ID Account</th>
                            <th>ID Location</th>
                            <th>Location name</th>
                            <th>Icon</th>
                            <th>Type of site</th>
                        </thead>
                    </table>

                    <hr class="divider"/>
                    <div class="row">
                        <div class="col-xs-8">
                            <span class="pull-left">
                                <button id="btnAdmOnlyLocsDel" type="button" class="btn btn-danger">Delete</button>
                                <button id="btnAdmOnlyLocsEdt" type="button" class="btn btn-info">Edit</button>
                            </span>
                        </div>
                    </div>


                </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-3">
                        <span class="pull-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>