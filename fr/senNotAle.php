<!-- ALERTS AND NOTIFICATIONS -->
    <!-- ALERTS AND NOTIFICATIONS -->
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="#" class="ActivDeActivShow" data-target="cnLANalerts">Alerts</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLANnotif">Notifications</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <!--#########################################################################################################-->
            <div id="cnLANalerts" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_" class="col-md-4 control-label">Alerts:</label>
                        <div class="col-md-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_" placeholder=""/>
                        </div>
                    </div>
                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLANnotif" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_" class="col-md-4 control-label">Notifications:</label>
                        <div class="col-md-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_" placeholder=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
