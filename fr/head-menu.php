<div class="navbar navbar-default navbar-fixed-top" style="top:3px;">
	<!--div class="container-fluid"-->

	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">ELG</a>
	</div>




	<div class="navbar-collapse collapse navbar-responsive-collapse">
		<ul class="nav navbar-nav">
			<li class="active dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Map <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="olMap.zoomToExtent(vLstations.getDataExtent());">
						<span style="font-size: 1.2em;" class="glyphicon glyphicon-globe" ></span>
						Zoom Extend</a>
					</li>
					<li class="divider"></li>
				</ul>
			</li>
			<li><a href="#">Status View</a>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="addNewSensor();">Add New Sensor</a>
					</li>
					<li><a href="#">Another action</a>
					</li>
					<li><a href="#">Something else here</a>
					</li>
					<li class="divider"></li>
					<li><a href="#">Separated link</a>
					</li>
					<li class="divider"></li>
					<li><a href="#">One more separated link</a>
					</li>
				</ul>
			</li>
		</ul>

		<div class="navbar-form navbar-left" role="search">
			<div class="form-group">
				<input id="inpFindLoc" type="text" class="form-control" placeholder="Search">
			</div>
			<button id="btnFindIt" type="submit" class="btn btn-default">New Search</button>
		</div>

		<ul id="ulLastSearch" class="nav navbar-nav" style="display:none">
				<li><a id="showLastFound" href="#">
			Last Search
			<span style="font-size: 1.2em;" class="glyphicon glyphicon-search" ></span>
		  </a>
				</li>
		</ul>
		
		<ul id="imgLoading" class="nav navbar-nav" style="disaplay:none">
			<li >
				<span><img src="img/ajax-loader.gif"></span>
			</li>
		</ul>


		<ul class="nav navbar-nav navbar-right">
			<li>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				Welcome User
			  </a>
			</li>
		</ul>


		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span style="font-size: 1.2em;" class="glyphicon glyphicon-cog"></span>
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="#">Action</a>
					</li>
					<li><a href="#">Another action</a>
					</li>
					<li><a href="#">Something else here</a>
					</li>
					<li class="divider"></li>
					<li><a href="#">Logout</a>
					</li>
				</ul>
			</li>
		</ul>

		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span style="font-size: 1.2em;" class="glyphicon glyphicon-bell"></span>
					<span class="badge">0</span>
					<b class="caret"></b>
				</a>
				<!--ul class="dropdown-menu">
				<li><a href="#">Action</a></li>
				<li><a href="#">Another action</a></li>
				<li><a href="#">Something else here</a></li>
				<li class="divider"></li>
				<li><a href="#">Logout</a></li>
			  </ul-->
			</li>
		</ul>

	</div>
	<!-- /.navbar-collapse -->



	<!--/div-->
</div>
