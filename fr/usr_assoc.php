<div class="modal fade" id="modUsrAssoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="min-width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">User Assosiation</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <table id="tblUserAssoc" class="display" cellspacing="0" width="100%">
                        <thead>
                            <th>ID User</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>User name</th>
                            <th>Type</th>
                            <th>Creation</th>
                        </thead>
                    </table>
                </div>

                <div id="divUserAssocs" style="display:none">
                    <hr class="divider">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#pnlUsrAsoLoc" data-toggle="tab">Location</a></li>
                        <li><a href="#pnlUsrAsoAcc" data-toggle="tab">Accounts</a></li>
                    </ul>
                    <div class="tab-content">

                        <!--# Locations #-->
                        <div class="tab-pane active" id="pnlUsrAsoLoc">
                            <div class="row">
                                <table id="tblUserAssocLoc" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <th>Accound</th>
                                        <th>Location</th>
                                        <th>Name</th>
                                        <th>Icon</th>
                                        <th>Type</th>
                                    </thead>
                                </table>
                            </div>
                            <div class="row">
                                <button type="button" class="btn btn-danger" onclick="fAddUserAsocDelLocation();">Remove assossiation</button>
                                <button type="button" class="btn btn-success" onclick="fAddUserAsocLocation();">Add Location</button>
                            </div>
                        </div>

                        <!--# Accounts #-->
                        <div class="tab-pane" id="pnlUsrAsoAcc">
                            <div class="row">
                                <table id="tblUserAssocAcc" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <th>Account</th>
                                        <th>Name</th>
                                    </thead>
                                </table>
                            </div>
                            <div class="row">
                                <button type="button" class="btn btn-danger" onclick="fAddUserAsocDelAccount();">Remove assossiation</button>
                                <button type="button" class="btn btn-info" onclick="fAddUserAsocAccount();">Add Account</button>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="divUsrAssoListLocs" style="display:none;" class="oWindow">
                    <div class="row">
                        <table id="tblUsrAccLocsList" class="display" cellspacing="0" width="100%">
                            <thead>
                                <th>ID Account</th>
                                <th>ID Location</th>
                                <th>Location name</th>
                                <th>Icon</th>
                                <th>Type of site</th>
                            </thead>
                        </table>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-danger" onclick="$('#divUsrAssoListLocs').fadeOut();">Close</button>    
                        <button type="button" class="btn btn-success" id="btnAddUsrAsocLoc">Add</button>
                    </div>
                </div>

                <div id="divUsrAssoListAcco" style="display:none;" class="oWindow">
                    <div class="row">
                        <table id="tblUsrAccAccoList" class="display" cellspacing="0" width="100%">
                            <thead>
                                <th>Account</th>
                                <th>Name</th>
                                <th>Date created</th>
                            </thead>
                        </table>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-danger" onclick="$('#divUsrAssoListAcco').fadeOut();">Close</button>    
                        <button type="button" class="btn btn-success" id="btnAddUsrAsocAcc">Add</button>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>





<script type="text/javascript" src="js/usr_assoc.js"></script>