<div class="modal fade" id="modAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="min-width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">My Account</h4>
            </div>
            <div class="modal-body">

                <form class="form-horizontal" role="form">
                    <fieldset>
					
						<div id="UserGrpPhoto" >
                            <div class="col-md-6 form-group" style="display:none">
    							<span class="btn btn-default btn-file">
    								Browse <input type="file" id="inAcPhoUsr">
    							</span>
                                <div class="pull-right col-sm-4">
                                    <img id="photUser" src="#" alt="User Photo" style="width:80px;height:120px" />
                                </div>
                            </div>
    						
    						
    						<div class="col-md-12 form-group">
                                <label for="inAcCompName" class="col-sm-2 control-label">Company Name</label>
                                <div class="col-sm-9">
                                    <input class="form-control" id="inAcCompName" placeholder="Company Name" readonly/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <label for="inAcCompName" class="col-sm-2 control-label">Account</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcU_id_account" placeholder="Account" readonly/>
                            </div>
                        </div>
						
                        <legend>Personal Info</legend>
						
						<div class="col-md-6 form-group">
                            <label for="inAcFName" class="col-sm-3 control-label">First N.</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcU_f_name" placeholder="First Name" />
                            </div>
                        </div>
						<div class="col-md-6 form-group">
                            <label for="inAcLName" class="col-sm-3 control-label">Last N.</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcU_l_name" placeholder="Last Name" />
                            </div>
                        </div>
						
						
						<div class="col-md-6 form-group">
                            <label for="inAcAcType" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-9">
                                <!--input class="form-control" id="inAcAcType" placeholder="Account Type" readonly/-->
                                <select class="form-control" id="inAcU_id_u_type"></select>
                            </div>
                        </div>
						<div class="col-md-6 form-group">
                            <label for="inAcUName" class="col-sm-3 control-label">User</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcU_user" placeholder="User Name"/>
                            </div>
                        </div>
						
						<div class="col-md-6 form-group">
                            <label for="inAcUName" class="col-sm-3 control-label">Alias</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcU_alias" placeholder="Alias Name" />
                            </div>
                        </div>
						
						<legend>Contact info</legend>
						
						<div class="col-md-6 form-group">
                            <label for="inAcAddres1" class="col-sm-3 control-label">Address 1:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcUA_address1" placeholder="Addres 1" />
                            </div>
                        </div>
						<div class="col-md-6 form-group">
                            <label for="inAcAddres2" class="col-sm-3 control-label">Address 2:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcUA_address2" placeholder="Addres 2" />
                            </div>
                        </div>
						
						<div class="col-md-4 form-group">
                            <label for="inAcCity" class="col-sm-3 control-label">City:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcUA_city" placeholder="City" />
                            </div>
                        </div>
						<div class="col-md-4 form-group">
                            <label for="inAcState" class="col-sm-3 control-label">State:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcUA_state" placeholder="State" />
                            </div>
                        </div>
						<div class="col-md-4 form-group">
                            <label for="inAcState" class="col-sm-3 control-label">Zip:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcUA_zip" placeholder="Zip Code" />
                            </div>
                        </div>
						
						<div class="col-md-10 form-group">
                            <label for="inAcEmail" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="inAcUC_user_mail" placeholder="Email" />
                            </div>
                        </div>
						
						<div class="col-md-4 form-group">
                            <label for="inAcPhone" class="col-sm-3 control-label">Phone:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcUC_home_1" placeholder="Phone" />
                            </div>
                        </div>
						<!--div class="col-md-4 form-group">
                            <label for="inAcPExt" class="col-sm-3 control-label">Ext:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcPExt" placeholder="Extension" />
                            </div>
                        </div-->
						<div class="col-md-4 form-group">
                            <label for="inAcMobil" class="col-sm-3 control-label">Mobil:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="inAcUC_cell_1" placeholder="Mobil" />
                            </div>
                        </div>
						
						
						<div class="col-md-10 form-group">
							<div class="checkbox">
								<label>
								<input type="checkbox" id="inAcU_send_mail" value="1"> Send mail notifications
								</label>
							</div>
							<div class="checkbox">
								<label>
								<input type="checkbox" id="inAcU_send_sms" value="1"> Send SMS notifications
								</label>
							</div>
						</div>
						
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnMyAccChkPwd" class="btn btn-info" style="left: 20px;position: absolute;">Change Password</button>
                <input type="hidden" id="isUpdorIns" value=""/>
                <input type="hidden" id="idAccUsr" value=""/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="btnSaveUserAccount">Save changes</button>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="modMyAccChkPw" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="min-width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>
            <div class="modal-body">
                <div role="form">
                    <div class="form-group">
                       <label for="inpCHKmyAcActPwd">Actual Password</label>
                       <input type="password" class="form-control" id="inpCHKmyAcActPwd" placeholder="Password">
                    </div>
                    <div class="form-group">
                       <label for="inpCHKmyAcActPwd1">New Password</label>
                       <input type="password" class="form-control" id="inpCHKmyAcActPwd1" placeholder="Password">
                    </div>
                    <div class="form-group">
                       <label for="inpCHKmyAcActPwd2">Repeat new password</label>
                       <input type="password" class="form-control" id="inpCHKmyAcActPwd2" placeholder="Password">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="btnProcNewChkPwd">Change Password</button>
            </div>
        </div>
    </div>
</div>