<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbpLeftmenu" style="border-right-style: double;">
	<div id="secMLtAlt">
		<div class="text-center">
			<span id="hideLeftMenuA" class="ui-icon ui-icon-carat-1-w" style="position: absolute;right: 8px;"><a href="#"></a></span>
			<h5 style="color: rgb(200,10,10);">
				<span style="font-size: 1.2em;" class="glyphicon glyphicon-fire"></span>
				Alerts
			</h5>
		</div>
		<hr class="divider">
		<div>
			<div id="listAlerts" class="list-group notif-content" style="overflow-x: auto;">
				
			</div>
		</div>
	</div>

<hr class="divider" id="dividLmenu">
	<div id="secMLtNot">
		<div class="text-center">
			<span id="hideLeftMenuN" class="ui-icon ui-icon-carat-1-w" style="position: absolute;right: 8px;"><a href="#"></a></span>
			<h5 style="color: rgb(200,10,10);">
				<span style="font-size: 1.2em;" class="glyphicon glyphicon-bell"></span>
				Notifications
			</h5>
		</div>
		<hr class="divider">
		<div>
			<div id="listNotifs" class="list-group notif-content" style="overflow-x: auto;">
				
			</div>
		</div>
	</div>
</nav>
