<div class="modal fade" id="modAccountDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modSensTitle">Account</h4>
			</div>
			<div class="modal-body">
                
                
                <div class="form-group">
                    <label for="InAccName" class="col-xs-2 control-label">Name:</label>
                    <div class="col-xs-8">
                        <input type="text" class="InpNonBords form-control" id="InAccName" placeholder="Account name"/>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-8">
                        <span class="pull-left">
                            <button id="btnSaveNewAcc" type="button" class="btn btn-success">Save</button>
                        </span>
                    </div>
                    <div class="col-xs-3">
                        <span class="pull-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
