<div class="modal fade" id="modAdmEvenCodes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Administrate Even Codes</h4>
            </div>
            <div class="modal-body">
                
                <div id="lstEvenCodes" class="list-group">
                </div>
                
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-8">
                        <span class="pull-left">
                        </span>
                    </div>
                    <div class="col-md-3">
                        <span class="pull-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/admEvenCodes.js"></script>