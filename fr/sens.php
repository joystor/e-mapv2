<div class="modal fade" id="modSensDial" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="min-width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modSensTitle">Location Sensor</h4>
			</div>
			<div class="modal-body">
                <ul class="nav nav-tabs"><!-- SENSOR NAME -->
                    <li class="active"><a href="#Sen" data-toggle="tab" id="aTabLocName">Location Name</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="Sen">
                        
                        
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="SensACCOUNT" class="col-xs-2 control-label">Account:</label>
                                <div class="col-xs-8">
                                    <input type="text" class="InpNonBords form-control" id="SensACCOUNT" placeholder="" readonly/>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <label for="InpSenTL_id_location" class="col-xs-2 control-label">Location ID:</label>
                                <div class="col-md-8">
                                    <input type="text" class="InpNonBords form-control" id="InpSenTL_id_location" placeholder="" readonly/>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <label for="InpSenTL_location_name" class="col-xs-2 control-label">Name:</label>
                                <div class="col-md-8">
                                    <input type="text" class="InpNonBords form-control" id="InpSenTL_location_name" placeholder=""/>
                                </div>
                            </div>
                        </div>

                        


                        <hr/>
                        <div id="divShowSensName">
                            <div class="row">
                                <!-- ACCOUNT INFO  -->
                                <!-- ACCOUNT INFO  -->
                                <!-- ACCOUNT INFO  -->
                                <!-- ACCOUNT INFO  -->
                                <!--div id="SensACCOUNT" class="col-md-6">Account: 20140524-001</div>
                                <div id="SensSubACCOUNT" class="col-md-6"></div-->  <!--Sub Account: XXXXX-->
                            </div>
                            <hr class="divider"/>
                            <div class="row" style="display:none">
                                <!-- STATE AND LOCATION  -->
                                <!-- STATE AND LOCATION  -->
                                <!-- STATE AND LOCATION  -->
                                <!-- STATE AND LOCATION  -->
                                <div id="SensSTATUS" class="col-md-6">State: GOOD</div>
                                <div class="col-md-6" style="display:none">Location:
                                    
                                </div>
                            </div>
                        </div>
                        <hr class="divider"/>
                        <ul class="nav nav-tabs">
                            <!-- TAB DIVIDER MENU -->
                            <!-- TAB DIVIDER MENU -->
                            <!-- TAB DIVIDER MENU -->
                            <!-- TAB DIVIDER MENU -->
                            <li class="active"><a href="#sTLocINf" data-toggle="tab">Location Info</a></li>
                            <li><a href="#sTAlerNot" data-toggle="tab">Alerts/Notifications</a></li>
                            <li><a href="#sTSenInf" data-toggle="tab">Sensor Info</a></li>
                            <!--li><a href="#sTAccounts" data-toggle="tab">Accounts</a></li-->
                        </ul>
                        <div class="tab-content">
                            <!--  -->
                            <div class="tab-pane active" id="sTLocINf">
                                <!-- LOCATION INFO -->
                                <!-- LOCATION INFO -->
                                <?php include('fr/sensor/senLocInf.php'); ?>
                            </div>
                            <!--  -->
                            <div class="tab-pane" id="sTAlerNot">
                                <!-- ALERTS AND NOTIFICATIONS -->
                                <!-- ALERTS AND NOTIFICATIONS -->
                                <?php include('fr/sensor/senNotAle.php'); ?>
                            </div>
                            <!--  -->
                            <div class="tab-pane" id="sTSenInf">
                                <!-- SENSOR INFOR  -->
                                <!-- SENSOR INFOR  -->
                                <?php include('fr/sensor/senSensInfo.php'); ?>
                            </div>

                            <!--div class="tab-pane" id="sTAccounts"-->
                                <!-- Accounts -->
                                <!-- Accounts -->
                                <?php //include('fr/sensor/senAccount.php'); ?>
                            <!--/div-->
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="inpSensFNC" value="ins"/>
            <input type="hidden" id="LocCODE" value=""/>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btnSensDo" type="button" class="btn btn-success" style="display:none">Insert</button>
                <button id="btnSensUpd" type="button" class="btn btn-success" style="display:none">Update</button>
                    <button id="btnSimulInsertAlert"
                             type="button" class="btn btn-warning" style="display:none"
                             onclick="bootbox.hideAll();$('#modSensDial').modal('hide');addAlertMrk($('#SensACCOUNT').html().replace('Account: ',''))"
                             >Insert Alert</button>
            </div>
        </div>
    </div>
</div>
