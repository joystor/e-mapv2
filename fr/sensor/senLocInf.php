    <!-- LOCATION INFO -->
    <!-- LOCATION INFO -->
    <div class="row">
        <div class="col-xs-3">
            <ul class="nav nav-pills nav-stacked" style="max-width: 166px;">
                <li class="active"><a href="#" class="ActivDeActivShow" data-target="cnLILa">Location Addres</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLILT">Location Type</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLIEq">Equipment</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLIMi">Manager Info</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLIUi">Utility Info</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLIRenS">Rental Service</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLIFS">Fuel Service</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLIRepS">Repair Service</a></li>
            </ul>
        </div>
        <div class="col-xs-9">
            <div id="cnLILa">
                <!--#########################################################################################################-->
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTLA_address1" class="col-xs-2 control-label">Address:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLA_address1" placeholder="Address"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="InpSenTLA_state" class="col-xs-2 control-label">State:</label>
                        <div class="col-xs-8">
                            <!--input type="text" class="InpNonBords form-control" id="InpSenTLA_state" placeholder="State"/-->
                            <select class="InpNonBords form-control"  id="InpSenTLA_state"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLA_city" class="col-xs-2 control-label">City:</label>
                        <div class="col-xs-8">
                            <!--input type="text" class="InpNonBords form-control" id="InpSenTLA_city" placeholder="City"/-->
                            <select class="InpNonBords form-control"  id="InpSenTLA_city"></select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="InpSenTLA_zip_4" class="col-xs-2 control-label">Zip + 4:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLA_zip_4" placeholder="Zip"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLA_country" class="col-xs-2 control-label">Country:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLA_country" placeholder="Country"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLA_lat" class="col-xs-2 control-label">Lat:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLA_lat" placeholder="Lat"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLA_lon" class="col-xs-2 control-label">Long:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLA_lon" placeholder="Long"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLA_lon" class="col-xs-2 control-label"></label>
                        <div class="col-xs-8">
                            <button class="btn btn-success" id="btnGetClickMap">get coordinates from click</button>
                        </div>
                    </div>


                </div>

            </div>
            <!--#########################################################################################################-->
            <div id="cnLILT" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_security_id" class="col-xs-5 control-label">Security ID:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_security_id" placeholder="Security ID"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_id_type_of_site" class="col-xs-5 control-label">Icon Type:</label>
                        <div class="col-xs-7">
                            <select class="InpNonBords form-control" id="InpSenTL_id_type_of_site" >
                                <option value="Tear">Tear</option>
                                <option value="Cross">Cross</option>
                                <option value="GasPump">GasPump</option>
                                <option value="Tower">Tower</option>
                                <option value="Building">Building</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_id_icon_type" class="col-xs-5 control-label">Type of Site:</label>
                        <div class="col-xs-7">
                            <!--input type="text" class="InpNonBords form-control" id="InpSenTL_id_icon_type" placeholder="Icon Type:"/-->
                            <select class="InpNonBords form-control"  id="InpSenTL_id_icon_type">
                                <option value="Retail">Retail</option>
                                <option value="Commercial">Commercial</option>
                                <option value="Medical Facility">Medical Facility</option>
                                <option value="Municipal Facility">Municipal Facility</option>
                                <option value="Cell Tower">Cell Tower</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_buildpow_type" class="col-xs-5 control-label">Building Power Type:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_buildpow_type" placeholder="Building Power Type"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_buildpow_size_amp" class="col-xs-5 control-label">Building Power Size AMPs:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_buildpow_size_amp" placeholder="Building Power Size AMPs"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_build_voltage" class="col-xs-5 control-label">Building Voltage:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_build_voltage" placeholder="Building Voltage"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_site_emergency_equipment" class="col-xs-5 control-label">Onsite emergency Equipment:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_site_emergency_equipment" placeholder="Onsite emergency Equipment"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_emergency_pow" class="col-xs-5 control-label">Mandated to have Emergency Power:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_emergency_pow" placeholder="Mandated to have Emergency Power"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_emergency_pow_supply_to" class="col-xs-5 control-label">Building Emergency Power Supply to:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_emergency_pow_supply_to" placeholder="Building Emergency Power Supply to"/>
                        </div>
                    </div>

                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLIEq" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_swt_gear_located_onsite" class="col-xs-5 control-label">Switch Gear located on Site:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_swt_gear_located_onsite" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_swt_gear_mfg" class="col-xs-5 control-label">Switch Gear MFG:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_swt_gear_mfg" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_swt_gear_model" class="col-xs-5 control-label">Switch Gear Model:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_swt_gear_model" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_swt_gear_mgf_date" class="col-xs-5 control-label">Switch Gear MFG date:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_swt_gear_mgf_date" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_swt_gear_size_kw" class="col-xs-5 control-label">Switch Gear Size  KiloWat:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_swt_gear_size_kw" placeholder=""/>
                        </div>
                    </div>

                    <hr class="divider"/>

                    <div class="form-group">
                        <label for="InpSenTL_gen_located_on_site" class="col-xs-5 control-label">Generator Located on Site:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_located_on_site" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_mfg" class="col-xs-5 control-label">Generator MFG:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_mfg" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_model" class="col-xs-5 control-label">Generator Model:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_model" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_mfg_date" class="col-xs-5 control-label">Generator MFG Date:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_mfg_date" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_size_kw" class="col-xs-5 control-label">Generator Size  KiloWat:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_size_kw" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_engine_site" class="col-xs-5 control-label">Generator Engine Size:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_engine_site" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_fuel_type" class="col-xs-5 control-label">Generator Fuel Type:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_fuel_type" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_fuel_tank_unit" class="col-xs-5 control-label">Generator Fuel Tank Unit:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_fuel_tank_unit" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_fuel_tank_size" class="col-xs-5 control-label">Generator Fuel Tank Size:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_fuel_tank_size" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_fuel_tank_level" class="col-xs-5 control-label">Generator Fuel Tank Level:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_fuel_tank_level" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_fuel_last_date_filled" class="col-xs-5 control-label">Generator Fuel Last Date Filled:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_fuel_last_date_filled" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_last_date_inspection" class="col-xs-5 control-label">Generator Last Date Inspection:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_last_date_inspection" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_last_date_inspection" class="col-xs-5 control-label">Generator Hours Ran from Last Inspection:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_last_date_inspection" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_gen_total_run_hours" class="col-xs-5 control-label">Generator Total Run Hours:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_gen_total_run_hours" placeholder=""/>
                        </div>
                    </div>

                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLIMi" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTLCI_fname" class="col-xs-5 control-label">First Name:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_fname" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_lname" class="col-xs-5 control-label">Last Name:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_lname" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_titlename" class="col-xs-5 control-label">Title:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_titlename" placeholder=""/>
                        </div>
                    </div>

                    <hr class="divider"/>

                    <div class="form-group">
                        <label for="InpSenTLCI_bus_tel_1" class="col-xs-5 control-label">LocCont Bus Tel No. 1:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_bus_tel_1" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_fax_1" class="col-xs-5 control-label">LocCon Fax No. 1:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_fax_1" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_misc_1" class="col-xs-5 control-label">LocCon Misc No. 1:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_misc_1" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_cell_1" class="col-xs-5 control-label">LocCon Cell No. 1:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_cell_1" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_bus_mail" class="col-xs-5 control-label">LocCon Bus email 1:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_bus_mail" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_misc_mail" class="col-xs-5 control-label">LocCon Misc email 1:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_misc_mail" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_address1" class="col-xs-5 control-label">LocCon Address1:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_address1" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_address2" class="col-xs-5 control-label">LocCon Address2:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_address2" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_city" class="col-xs-5 control-label">LocCon City:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_city" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_state" class="col-xs-5 control-label">LocCon State:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_state" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_zip_4" class="col-xs-5 control-label">LocCon Zip+4:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_zip_4" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTLCI_country" class="col-xs-5 control-label">LocCon Country:</label>
                        <div class="col-xs-7">
                            <input type="text" class="InpNonBords form-control" id="InpSenTLCI_country" placeholder=""/>
                        </div>
                    </div>


                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLIUi" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_pow_sup_co_name " class="col-xs-4 control-label">Power Supplier Co Name:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_name" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_pow_sup_co_address1 " class="col-xs-4 control-label">Power Supplier Co "Address1":</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_address1" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_pow_sup_co_address2" class="col-xs-4 control-label">Power Supplier Co " Address2":</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_address2" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_pow_sup_co_city" class="col-xs-4 control-label">Power Supplier Co "City":</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_city" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_pow_sup_co_state" class="col-xs-4 control-label">Power Supplier Co "State":</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_state" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_pow_sup_co_zip" class="col-xs-4 control-label">Power Supplier Co "Zip":</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_zip" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTL_pow_sup_co_contact_phone" class="col-xs-4 control-label">Power Supplier Co "Contact Telephone":</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_contact_phone" placeholder=""/>
                        </div>
                    </div>
                    <!--div class="form-group">
                        <label for="InpSenTL_pow_sup_co_contact_phone" class="col-xs-4 control-label">Power Supplier Co "Contact Person":</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_pow_sup_co_contact_phone" placeholder=""/>
                        </div>
                    </div-->
                    
                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLIRenS" style="display:none">
                <!--div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_" class="col-xs-4 control-label">Address:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_" placeholder=""/>
                        </div>
                    </div>
                </div-->
            </div>
            <!--#########################################################################################################-->
            <div id="cnLIFS" style="display:none">
                <!--div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_" class="col-xs-4 control-label">Address:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_" placeholder=""/>
                        </div>
                    </div>
                </div-->
            </div>
            <!--#########################################################################################################-->
            <div id="cnLRepSI" style="display:none">
                <!--div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTL_" class="col-xs-4 control-label">Address:</label>
                        <div class="col-xs-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTL_" placeholder=""/>
                        </div>
                    </div>
                </div-->
            </div>
        </div>
    </div>
