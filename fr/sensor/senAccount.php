<div class="row">
    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="#" class="ActivDeActivShow" data-target="cnAcc">Accounts</a></li>
            <li><a href="#" class="ActivDeActivShow" data-target="cnLiAcc">List Accounts</a></li>
        </ul>
    </div>
    <div class="col-md-9">
        <!--#########################################################################################################-->
        <div id="cnAcc">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="InpSenTLA_address1" class="col-md-2 control-label">Address:</label>
                    <div class="col-md-8">
                        <input type="text" class="InpNonBords form-control" id="InpSenTLA_address1" placeholder="Address"/>
                    </div>
                </div>
            </div>
        </div>

        <!--#########################################################################################################-->
        <div id="cnLiAcc">
            <div class="col-md-12">
                <table class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Extn.</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                    </thead>
                    
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Extn.</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>