<!-- SENSOR INFOR  -->
    <!-- SENSOR INFOR  -->
    <div class="row">
        <div class="col-sm-3">
            <ul class="nav nav-pills nav-stacked" style="max-width: 166px;">
                <li><a href="#" class="ActivDeActivShow" data-target="cnLSIsensor">Sensor</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLSIgpsloc">GPS Location</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLSIservpr">Service Provider</a></li>
                <li><a href="#" class="ActivDeActivShow" data-target="cnLSIstatus">Status</a></li>
            </ul>
        </div>
        <div class="col-sm-9">
            <!--#########################################################################################################-->
            <div id="cnLSIsensor" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTSI_mfg_name" class="col-sm-4 control-label">MFG Name:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_mfg_name" placeholder=""/>
                        </div>
                    </div>

                <hr class="divider"/>

                    <div class="form-group">
                        <label for="InpSenTSI_mfg_model_name" class="col-sm-4 control-label">MFG Model Name:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_mfg_model_name" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_mfg_model_no" class="col-sm-4 control-label">MFG Model No.:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_mfg_model_no" placeholder=""/>
                        </div>
                    </div>

                <hr class="divider"/>

                    <div class="form-group">
                        <label for="InpSenTSI_serv_type" class="col-sm-4 control-label">Service Type :</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_serv_type" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_serv_desc" class="col-sm-4 control-label">Service Description:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_serv_desc" placeholder=""/>
                        </div>
                    </div>

                <hr class="divider"/>

                
                    <div class="form-group">
                        <label for="InpSenTSI_battery_part_no" class="col-sm-4 control-label">Battery Part No:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_battery_part_no" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_battery_voltage" class="col-sm-4 control-label">Battery Voltage:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_battery_voltage" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_phase_setting" class="col-sm-4 control-label">Phase Setting:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_phase_setting" placeholder=""/>
                        </div>
                    </div>
                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLSIgpsloc" style="display:none">
                <h2>GPS location</h2>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLSIservpr" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTSI_com1_type" class="col-sm-4 control-label">Com 1 Type:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com1_type" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com1_service" class="col-sm-4 control-label">Com 1 Service Provider:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com1_service" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com1_device_mac" class="col-sm-4 control-label">Com 1 Device MAC:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com1_device_mac" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com1_warning" class="col-sm-4 control-label">Com1 Warning:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com1_warning" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com1_notes" class="col-sm-4 control-label">Com1 Notes:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com1_notes" placeholder=""/>
                        </div>
                    </div>

                    <hr class="divider">

                    <div class="form-group">
                        <label for="InpSenTSI_com2_type" class="col-sm-4 control-label">Com 2 Type:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com2_type" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com2_service" class="col-sm-4 control-label">Com 2 Service Provider:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com2_service" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com2_device_mac" class="col-sm-4 control-label">Com 2 Device MAC:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com2_device_mac" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com2_warning" class="col-sm-4 control-label">Com 2 Warning:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com2_warning" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_com2_notes" class="col-sm-4 control-label">Com 2 Notes:</label>
                        <div class="col-sm-8">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_com2_notes" placeholder=""/>
                        </div>
                    </div>
                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLSIstatus" style="display:none">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_1_premise_temp" class="col-sm-7 control-label">Sensor 1 Premise Temp:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_1_premise_temp" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_2_premise_warning" class="col-sm-7 control-label">Sensor 2 Premise Warning:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_2_premise_warning" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_3_premise_line_a_current" class="col-sm-7 control-label">Sensor 3 Premise Line A Current:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_3_premise_line_a_current" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_4_premise_line_b_current" class="col-sm-7 control-label">Sensor 4 Premise Line B Current:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_4_premise_line_b_current" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_5_premise_line_c_current" class="col-sm-7 control-label">Sensor 5 Premise Line C Current:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_5_premise_line_c_current" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_6_generator_on" class="col-sm-7 control-label">Sensor 6 Generator On:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_6_generator_on" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_7_gen_rotation_alert" class="col-sm-7 control-label">Sensor 7 Gen Rotation Alert:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_7_gen_rotation_alert" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_8_gen_temp" class="col-sm-7 control-label">Sensor 8 Gen Temp:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_8_gen_temp" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_9_gen_fuel_level" class="col-sm-7 control-label">Sensor 9 Gen Fuel level:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_9_gen_fuel_level" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_10_gen_oil_low_warning" class="col-sm-7 control-label">Sensor 10 Gen Oil Low Warning:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_10_gen_oil_low_warning" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_11_gen_line_a_voltage" class="col-sm-7 control-label">Sensor 11 Gen Line A Voltage:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_11_gen_line_a_voltage" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_12_gen_line_b_voltage" class="col-sm-7 control-label">Sensor 12 Gen Line B Voltage:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_12_gen_line_b_voltage" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_13_gen_line_c_voltage" class="col-sm-7 control-label">Sensor 13 Gen Line C Voltage:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_13_gen_line_c_voltage" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_14_unit_temp" class="col-sm-7 control-label">Sensor 14 Unit Temp:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_14_unit_temp" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_15_unit_door_open" class="col-sm-7 control-label">Sensor 15 Unit Door Open:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_15_unit_door_open" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_16_unit_been_striked" class="col-sm-7 control-label">Sensor 16 Unit Been Striked:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_16_unit_been_striked" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InpSenTSI_sensor_17_unit_low_battery" class="col-sm-7 control-label">Sensor 17 Unit Low Battery:</label>
                        <div class="col-sm-5">
                            <input type="text" class="InpNonBords form-control" id="InpSenTSI_sensor_17_unit_low_battery" placeholder=""/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
