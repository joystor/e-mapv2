<!-- ALERTS AND NOTIFICATIONS -->
    <!-- ALERTS AND NOTIFICATIONS -->
    <div class="row">
        <div class="col-xs-3">
            <ul class="nav nav-pills nav-stacked" style="max-width: 166px;">
                <li><a href="#" onclick="loadNotsHistory('alert')" class="ActivDeActivShow" data-target="cnLANalerts">Alerts</a></li>
                <li><a href="#" onclick="loadNotsHistory('notif')" class="ActivDeActivShow" data-target="cnLANnotif">Notifications</a></li>
            </ul>
        </div>
        <div class="col-xs-9">
            <!--#########################################################################################################-->
            <div id="cnLANalerts" style="display:none">
                <div>
                    <div id="divSecAlerts">
                    </div>
                </div>
            </div>
            <!--#########################################################################################################-->
            <div id="cnLANnotif" style="display:none">
                <div>
                    <div  id="divSecNotifs">
                    </div>
                </div>
            </div>
        </div>
    </div>
