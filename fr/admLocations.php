<div class="modal fade" id="modAdmLocationDial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modSensTitle">Add and Edit Locations</h4>
			</div>
			<div class="modal-body">
                
                
                <div id="divInsSensName" class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="" class="col-md-3 control-label">Account:</label>
                            <div class="col-md-8">
                                <label class="InpNonBords form-control" id="" placeholder="">
                                    2014035TD
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="" class="col-md-3 control-label">Account Name:</label>
                            <div class="col-md-8">
                                <label class="InpNonBords form-control" id="" placeholder="">
                                    H2 GAS inc
                                </label>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                 <h5>Master Account</h5>
                <hr class="divider"/>
                    <table id="tblAdLocs" class="display" cellspacing="0" width="100%">
                        <thead>
                            <th>SubAccount</th>
                            <th>Name</th>
                            <th>Date Created</th>
                            <th>Date Modified</th>
                        </thead>
                        <tr>
                            <td>2014035TD</td>
                            <td>H2 GAS Loc 1</td>
                            <td>04-01-2014</td>
                            <td>04-01-2014</td>
                        </tr>
                        <tr>
                            <td>2014035TD2</td>
                            <td>H2 GAS Loc 2</td>
                            <td>04-01-2014</td>
                            <td>04-01-2014</td>
                        </tr>
                        <tr>
                            <td>2014035TD3</td>
                            <td>H2 GAS Loc 3</td>
                            <td>04-01-2014</td>
                            <td>04-01-2014</td>
                        </tr>
                    </table>
                
            </div>
            <input type="hidden" id="inpSensFNC" value="ins"/>
            <input type="hidden" id="LocCODE" value=""/>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-8">
                        <span class="pull-left">
                            <button id="" type="button" class="btn btn-danger">Delete</button>
                            <button id="" type="button" class="btn btn-info">Edit</button>
                            <button id="" type="button" class="btn btn-success">Add Account</button>
                        </span>
                    </div>
                    <div class="col-md-3">
                        <span class="pull-right">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
