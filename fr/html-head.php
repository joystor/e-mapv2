<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="js/libs/jquery-ui-1.10.4/development-bundle/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="js/libs/bootstrap-3.1.1/css/bootstrap.min.css">

    <script type='text/javascript' src='js/libs/md5.js'></script>
    
    <link rel="stylesheet" href="js/libs/bootstrap-3.1.1/themes/spacelab/bootstrap.min.css">
    <link rel="stylesheet" href="js/libs/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="js/libs/bootstrap3-editable/css/bootstrap-editable.css">
    <link rel="stylesheet" href="js/libs/dataTables.bootstrap/dataTables.bootstrap.css">
    <link rel="stylesheet" href="js/libs/SlidePushMenus/css/component.css">
    <link href='css/olMapStyle.css' rel='stylesheet' type='text/css'>
    <!--link href='http://fonts.googleapis.com/css?family=Pathway+Gothic+One' rel='stylesheet' type='text/css'-->
    <style>html, head, body { width: 100%; height: 100% }</style>

    <script type="text/javascript" src="js/libs/require.js" data-main="js/main"></script>
</head>
<body>
