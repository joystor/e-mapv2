<script type='text/javascript' src='js/menuBottom.js'></script>
<nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-bottom cbp-spmenu-open" 
    id="cbpBottommenu" 
    style="border-right-style: double; display:none; background-color: rgba(255,255,255,0.4); left: 240px; right: 0px; height: 200px;">

    <div class="olPopupCloseBox" style="width: 17px; height: 17px; position: absolute; right: 13px; top: 14px; z-index: 1;"
        onclick="classie.remove( document.getElementById( 'cbpBottommenu' ), 'cbp-spmenu-open' );$('#cbpBottommenu').css('height','')">
    </div>

    <div class="row" style="text-align:center">
        <div class="col-sm-4">
            <h4>All locations</h4>
        </div>
        <div class="col-sm-4">
            <h4>Selected location</h4>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <canvas id="chartLocAll"></canvas>
        </div>
        <div class="col-sm-4">
            <canvas id="chartLoc1"></canvas>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
</nav>
