var imgLD = '<img id="imgLD" src="img/load-blue.gif" height="10" width="32">';
var oMap;
var geolocate;
var RegPross = 0;
var addrFound;
var sendItReg = false;
var regCDS = {
    cU:"",cL:"",cA:""
};

function openNewUser(){
    $("#btnNewUsr").prop('disabled', false);
	$("#gtNewAccMsg").html("");
    $("#divNUCompany").hide();
    $("#divNUPersonal").show();
    $("input[id^='uNewL']").val("");
    RegPross = 0;
    $("#modSensTitle").html("User information");
    $("#divNUPersonal").fadeIn();
    $("#divNUCompany").hide();
    $("#divNUCompanyInfo").hide();
    $("#divNewUsrFrm input").val("");
}


function forgetUser(){
    $("#btnForgGetAccoutn").prop('disabled', false);
    $("#gtAccMsg").html("");
}


$().ready(function(){
    loadMap();

    addRequiere('divNewUsrFrm');
    addRequiere('modFogUsr');
    addRequiere('modChkPwd');
    /*##################################################################
     * Get User
     * ###############################################################*/
    $("#btnForgGetAccoutn").on("click",function(){
        if($("#uLM").val()==""){
            $("#gtAccMsg").html("<p class='text-danger'>Write a mail</p>");
            return false;
        }
        $("#btnForgGetAccoutn").prop('disabled', true);
        $("#gtAccMsg").html(imgLD);
        $.post("bk/idx.php",{fc:"recMl", ms:$("#uLM").val()},function(j){
            $("#imgLD").remove();
            $("#btnForgGetAccoutn").prop('disabled', false);
            if(j.res=="noFound"){
                $("#gtAccMsg").html("<p class='text-danger'>Your mail isn't in the system</p>");
            }else if(j.res=="snd2Mail"){
                $("#gtAccMsg").html("<p class='text-success'>Your Account information is sendit to your mail account</p>");
            }
        });
    });
    
    /*##################################################################
     * Verify mail
     * ###############################################################*/
    $("#btnForgPaswd").on("click",function(){
        if($("#uLM").val()==""){
            $("#gtAccMsg").html("<p class='text-danger'>Write a mail</p>");
            return false;
        }
        $("#btnForgPaswd").prop('disabled', true);
        $("#gtAccMsg").html(imgLD);
        $.post("bk/idx.php",{fc:"chkMl", ms:$("#uLM").val()},function(j){
            $("#imgLD").remove();
            $("#btnForgPaswd").prop('disabled', false);
            if(j.res=="noFound"){
                $("#gtAccMsg").html("<p class='text-danger'>Your mail isn't in the system</p>");
            }else if(j.res=="Found"){
                $("#gtChkPwdMsg").html("");
                $("#modChkPwd").modal("show");
            }
        });
    });
    
    /*##################################################################
     * change pwd
     * ###############################################################*/
    $("#btnChkPaswd").on("click",function(){
        if( $("#uPw1").val() != $("#uPw2").val() || $("#uPw1").val() == "" ){
            $("#gtChkPwdMsg").html("<p class='text-danger'>The passwords are't the same</p>");
            return false;
        }
        
        $("#btnChkPaswd").prop('disabled', true);
        $("#gtChkPwdMsg").html(imgLD);
        $.post("bk/idx.php",{fc:"chgPw", ms:$("#uLM").val(), pw: hex_md5( $("#uPw1").val() )},function(j){
            $("#imgLD").remove();
            $("#btnChkPaswd").prop('disabled', false);
            $("#gtChkPwdMsg").html("<p class='text-success'>Password is changed</p>");
            $("#modFogUsr").modal("hide");
            $("#modChkPwd").modal("hide");
        });
    });
    
    
    
    /*##################################################################
     * Register USer
     * ###############################################################*/
    $("#btnNewUsr").on("click",function(){

        if(RegPross == 0){
            if( $("#uNewUN").val() == "" || $("#uNewUL").val() == "" || $("#uNewmail").val() == "" 
                || $("#uPwN1").val() == "" || $("#uPwN2").val() == "" ||
                ($("#uPwN1").val() != $("#uPwN2").val()))
            {
                $("#gtNewAccMsg").html("<p class='text-danger'>Verify your data</p>");
                return false;
            }

            $("#btnNewUsr").prop("disabled", true);
            $.post("bk/idx.php",{fc:"verifyUserMail",ms: $("#uNewUMl").val() ,Us:$("#uNewUSR").val()},
                function(j){
                    var msg = "";
                    if(j.res=="usrExist"){
                        msg = "<p class='text-danger'>The username exist</p>";
                    }else if(j.res=="mailExist"){
                        msg = "<p class='text-danger'>The email exist</p>";
                    }
                    $("#gtNewAccMsg").html( msg );
                    if(msg != ""){
                        $("#btnNewUsr").prop("disabled", false);
                        return false;
                    }

                    if(j.res != "NoParms"){
                        msg = "";
                        RegPross = 1;
                        $("#btnNewUsrBack").prop("disabled", false);
                        $("#btnNewUsr").prop("disabled", false).trigger("click");
                        $("#gtNewAccMsg").html( msg );
                    }
                    $("#btnNewUsr").prop("disabled", false);
            });
            
        }
        $("#gtNewAccMsg").html("");
        if(RegPross == 1){

            if( $("#divNUPersonal").is(":visible")){
                $("#divNUCompanyInfo").hide();
                $("#divNUPersonal").hide({ 
                        complete: function(){
                            $("#divNUCompany").fadeIn();
                            $("#btnNewUsrBack").prop("disabled", false);
                            //$("#mapContainer").fadeIn();
                            $("#modSensTitle").html("Location information<br>"+
                                "<h5>Click on map to add your location, verify that your browser can allow your position</h5>");
                        }
                    });
            }
            geolocate.activate();
            if( $("#inpIdxCoordsLocs").val() == ""){
                 $("#gtNewAccMsg").html("<p class='text-danger'>Click on Map</p>");
                return false;
            }
            RegPross = 2;
        }

        //if(RegPross == 2){
            if( $("#divNUCompany").is(":visible")){
                $("#divNUCompany").hide({ 
                    complete:function(){
                        $("#divNUCompanyInfo").fadeIn();
                        $("#modSensTitle").html("Company information");
                    }
                });
                RegPross = 3;
                return false;
            }
        //}

        //if(RegPross == 3){
        if( $("#divNUCompanyInfo").is(":visible")){
            var missing = false;
            $.each( $("#divNUCompanyInfo input[id^='uNewL'][required]"), function(i,v){
                if( $(v).val() == "" ){
                    missing = true;
                }
            });
            if(missing){
                if( $("#divNUCompany").is(":visible")){
                    $("#divNUCompany").hide({ 
                        complete:function(){
                            $("#divNUCompanyInfo").fadeIn();
                            $("#modSensTitle").html("Company information");
                        }
                    });
                    RegPross = 2;
                    return false;
                }
                 $("#gtNewAccMsg").html("<p class='text-danger'>Please complete the form</p>");
                return false;
            }
        }

        if(RegPross != 3){
            return false;
        }

        
        /*$.each( $("input[id^='uNewL']"), function(i,v){
            var nom = $(v).attr("id").replace('uNewL','');
            var val = $(v).val();
            opc[nom] = val;
        });

        $.each( $("select[id^='uNewL']"), function(i,v){
            var nom = $(v).attr("id").replace('uNew','');
            var val = $(v).val();
            opc[nom] = val;
        });

        $.each( $("input[id^='uNewU']"), function(i,v){
            var nom = $(v).attr("id").replace('uNewU','');
            var val = $(v).val();
            opc[nom] = val;
        });*/

        bootbox.dialog(
            {
                message:
                        'In this mail we send the access information, please press the button to send it'+
                        '<div class="input-group">'+
                        '  <div class="input-group-addon">'+
                        '      <span class="glyphicon glyphicon-envelope"></span>'+
                        '  </div>'+
                        '  <input id="uNewMAILCHANGE" onchange="sendSignUP()" type="email" class="form-control inpChgMail" required="" autofocus="" value="'+$('#uNewUMl').val()+'">'+
                        '</div>'+
                        '<br>'+
                        '<button id="btnSendRegAgain" class="btn btn-success btnResendInfoUsr">Send</button>',
                title: "Send registration to",
                buttons: {
                    success:{
                        label: "Ready I recibe the mail!",
                        className: "btn-success",
                        callback: function() {
                            $("#divNewUsrFrm").modal("hide");
                            return true;
                        }
                    },
                    change:{
                        label: "Change information!",
                        className: "btn-warning",
                        callback: function() {
                            return true;
                        }
                    }
                }
            }); 


                    

    });
    
    
    $("#btnNewUsrBack").on("click", function(){
            if( $("#divNUCompanyInfo").is(":visible")){
                $("#divNUCompanyInfo").hide({ complete:$("#divNUCompany").fadeIn()});
                $("#modSensTitle").html("Location information<br>"+
                                "<h5>Click on map to add your location, verify that your browser can allow your position</h5>");
                RegPross = 2;
            }else
            if( $("#divNUCompany").is(":visible")){
                $("#modSensTitle").html("User information");
                $("#divNUCompany").hide({ complete:$("#divNUPersonal").fadeIn()});
                $("#btnNewUsrBack").prop("disabled", true);
                RegPross = 1;
            }

    });

    /*##################################################################
     * Phone or Cel
     * ###############################################################*/
    $("#uNewUs_home_1").on("change", function(e){
        if( $("#uNewUs_home_1").val() != "" ){
            $("#uNewUs_cell_1").removeAttr("required");
            $("#uNewUs_cell_1").css("background-color","");
        }else{
            $("#uNewUs_cell_1").attr("required","required");
        }
    });
    $("#uNewUs_cell_1").on("change", function(e){
        if( $("#uNewUs_cell_1").val() != "" ){
            $("#uNewUs_home_1").removeAttr("required");
            $("#uNewUs_home_1").css("background-color","");
        }else{
            $("#uNewUs_home_1").attr("required","required");
        }
    });
    
});


function sendSignUP(){
    if($("#btnSendRegAgain").length > 0 && $("#btnSendRegAgain").is(":visible") ){
        $("#btnSendRegAgain").html("Wait please");
        $("#btnSendRegAgain").prop("disabled","disabled");
    }
    var opc = {};
    $.each( $("#divNewUsrFrm input").not("[id^=uPwN]") , function(i,v){
        var nom = $(v).attr("id");
        var val = $(v).val();
        opc[nom] = val;
    });

    $.each( $("#divNewUsrFrm select") , function(i,v){
        var nom = $(v).attr("id");
        var val = $(v).val();
        opc[nom] = val;
    });

    opc["cords"] = $("#inpIdxCoordsLocs").val();
    opc["pw"] = $("#uPwN1").val();
    opc["cords"] = $("#inpIdxCoordsLocs").val();

    opc['fc'] = "nwAcc";
    opc['ms'] = $("#uNewUMl").val();
    opc['uN'] = $("#uNewUN").val();
    opc['lN'] = $("#uNewUL").val();
    opc['uS'] = $("#uNewUSR").val();
    opc['pw'] =  hex_md5( $("#uPwN1").val() );

    opc['uCt'] = $("#uNewUs_home_1").val();
    opc['uCl'] = $("#uNewUs_cell_1").val();
    if(sendItReg==true){
        opc['uNChkMl34'] = "1d4Rtg1";
        opc['cd34U'] = regCDS.cU;
        opc['cd34L'] = regCDS.cL;
        opc['cd34A'] = regCDS.cA;
    }
    if(sendItReg==false){
        sendItReg=true;
    }

    $("#btnNewUsr").prop('disabled', true);
    $("#gtNewAccMsg").html(imgLD);
    $.post("bk/idx.php",
            opc
        ,function(j){
            if($("#btnSendRegAgain").length > 0 && $("#btnSendRegAgain").is(":visible") ){
                $("#btnSendRegAgain").html("Please check your mail, if isnt' recibed Send Again");
                $("#btnSendRegAgain").prop("disabled","");
            }
            $("#imgLD").remove();
            $("#btnNewUsr").prop('disabled', false);
            var msg = "";
            if(j.res=="usrExist"){
                msg = "<p class='text-danger'>The username exist</p>";
                $("#gtNewAccMsg").html( msg );
            }else if(j.res=="mailExist"){
                msg = "<p class='text-danger'>The email exist</p>";
                $("#gtNewAccMsg").html( msg );
            }else{
                regCDS = {
                    cU:j.res.cU,
                    cL:j.res.cL,
                    cA:j.res.cA
                };
                msg = "<p class='text-success'>You're account is ready, please confirm in your mail</p>";
                //$("#divNewUsrFrm").modal("hide");
                bootbox.alert("We sent the confirmation link, please please verify your mail");
            }

    });
}

function loadMap(){
    var maxExtent = new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508),
        restrictedExtent = maxExtent.clone(),
        maxResolution = 156543.0339;
    var options = {
        projection: new OpenLayers.Projection("EPSG:900913"),
        displayProjection: new OpenLayers.Projection("EPSG:4326"),
        units: "m",
        numZoomLevels: 18,
        restrictedExtent: restrictedExtent,
        center: new OpenLayers.LonLat(-11050908.488492, 4684046.4671457),
        zoom: 3
    };
    oMap = new OpenLayers.Map();
    oMap.options = options;

    oMap = new OpenLayers.Map('idxMap');
    var layer = new OpenLayers.Layer.OSM( "Simple OSM Map");
    var vector = new OpenLayers.Layer.Vector('vector');
    oMap.addLayers([layer, vector]);

    /*oMap.setCenter(
            new OpenLayers.LonLat(-99.27200000000401, 38.73684358310057).transform(
                new OpenLayers.Projection("EPSG:4326"),
                oMap.getProjectionObject()
            ), 3
        );*/

    geolocate = new OpenLayers.Control.Geolocate({
        bind: false,
        geolocationOptions: {
            enableHighAccuracy: false,
            maximumAge: 0,
            timeout: 7000
        }
    });
    oMap.addControl(geolocate);

    geolocate.events.register("locationupdated",geolocate,function(e) {
        //oMap.zoomToExtent( e.point.getCentroid());
        oMap.setCenter(
                new OpenLayers.LonLat( e.point.x, e.point.y), 16
            );
    });
    geolocate.events.register("locationfailed",this,function() {
        oMap.setCenter(
            new OpenLayers.LonLat(-11050908.488492, 4684046.4671457), 3
        );
    });

    var markers = new OpenLayers.Layer.Markers( "Markers" );
    oMap.addLayer(markers);

    oMap.events.register("click", oMap, function(evt) {
        var pos = oMap.getLonLatFromPixel(evt.xy);
        console.log(pos);
        var pos2 = new OpenLayers.LonLat(pos.lon, pos.lat)
            .transform(oMap.getProjectionObject() , new OpenLayers.Projection("EPSG:4326") );
        $("#inpIdxCoordsLocs").val(pos2.lon + ", "+pos2.lat);
        var size = new OpenLayers.Size(31,31);
        var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
        var icon = new OpenLayers.Icon('img/marks/green_tear.png',size,offset);
        marker = new OpenLayers.Marker(pos, icon);
        markers.clearMarkers(); 
        markers.addMarker(marker);

        //var url ="http://nominatim.openstreetmap.org/reverse?"+
                //"&addressdetails=1"+
                //"&format=json"+
                //"&zoom=18";
        var url = "http://api.geonames.org/findNearbyJSON?";
        var par="&username=joystor"+
                "&maxRows=1"+
                "&lat="+$("#inpIdxCoordsLocs").val().split(",")[1]+
                "&lng="+$("#inpIdxCoordsLocs").val().split(",")[0];
        
        var addr;
        $.get(url+par, {}, function(j){
                geon = j.geonames[0];
                /*console.log(addr.city);
                console.log(addr.country);
                console.log(addr.state);
                console.log(addr.postcode);*/
                $("#uNewLA_country").val(geon.countryName);
                $("#uNewLA_state").val(geon.adminName1);
                $("#uNewLA_city").val(geon.name);
        });

        url = "http://api.geonames.org/findNearbyPostalCodesJSON?";
        $.get(url+par, {}, function(j){
                var pc = j.postalCodes[0];
                $("#uNewLA_zip").val(pc.postalCode);
        });

    });

    $("#idxMap .olControlAttribution").css("bottom","10px");
}




function addRequiere(div){
    $.each( $("#"+div).find("input"), function(i, v){
        $(v).addClass('iRequiereIMG');
        if( $(v).attr("type") == "tel" ){
            $(v).on("change",function(){
                if( $(this).is(":required") ){
                    phonenumber( $(v) );
                }else{
                    $(this).css("background-color","");
                }
            });
        }
    });
}


function phonenumber( $inp ){  
    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
    if( $inp.val().match(phoneno) ){  
        $inp.css("background-color","");
        return true;       
    }else{
        bootbox.alert("Not a valid Phone Number");  
        $inp.css("background-color","rgba(255, 0, 0, 0.2)");
        return false;  
    }  
} 