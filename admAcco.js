
var tblAdAcc;
var tblAdLocAcc;
var tblAdUsers;
var tblAdOnlyUsers;
var tblAdOnlyLocs;

var isAddOrEdi = "";
var isSubAddOrEdi = "";

$().ready(function(){
    
    /* TABLE Acc */
    tblAdAcc = makeDatTable('tblAdAcc',
        (function(d){
            d.tb = "gtAccounts";
        }));
    $('#tblAdAcc tbody').on( 'click', 'tr', function () {
        if( tblAdAcc.DataTable().row('.selected').length > 0){
            $('#divAccRelSubAcc').fadeIn();
            tblAdLocAcc.fnDraw();
            tblAdUsers.fnDraw();
        }else{
            $('#divAccRelSubAcc').fadeOut();
        }
    });
    /* TABLE SubAcc */
    tblAdLocAcc = makeDatTable('tblAdLocAcc',
        (function(d){
            d.tb = "gtSubAccounts";
            var val = getDTselected(tblAdAcc);
            d.acc = val[0];
        }));

    /* TABLE Users */
    tblAdUsers = makeDatTable('tblAdUsers',
        (function(d){
            d.tb = "gtSubUsr";
            var val = getDTselected(tblAdAcc);
            d.acc = val[0];
        }));

    /*Table Adm Users*/
    tblAdOnlyUsers = makeDatTable('tblAdmOnlUsers',
        (function(d){
            d.tb = "gtAdmOnlUsr";
        }));
    /*Table Adm Locs*/
    tblAdOnlyLocs = makeDatTable('tblAdmOnlLocs',
        (function(d){
            d.tb = "gtAdmOnlLocs";
        }));



    $("#btnAdmOnlyUsrDel").on("click", function(){
        var val = getDTselected(tblAdOnlyUsers);
        bootbox.confirm("Are you sure to delete this user permanently "+val[2]+": "+val[3]+" "+val[4], function(result) {
                if(result == true){
                    $.post("bk/sUsr.php", {"func":"del","idU":val[8]}, function(json){
                        bootbox.alert("User is deleted");
                        tblAdOnlyUsers.fnDraw();
                    });
                }
            }); 
    });

    /*EDIT USER ADM*/
    $("#btnAdmOnlyUsrEdt").on("click", function(){
        var val = getDTselected(tblAdOnlyUsers);
        $("#modAdmOnlUsers").modal("hide");
        addNewUser();
        oELG.bInsNewUserbyADM = false;
        var val = getDTselected(tblAdOnlyUsers)
        $.post("bk/funs.php",
            {  "pG" : "gUsrAllInfo",
                "id": val[8]
            },function(j){

                $.each( j.rows.U[0], function(i,v){
                    $("#inAcU_"+i).val(v);
                    if( $("#inAcU_"+i).attr("type") == "checkbox" && v == "1"){
                        $("#inAcU_"+i).prop('checked', true);
                    }
                });
                if(j.rows.UA != null){
                    $.each( j.rows.UA[0], function(i,v){
                        $("#inAcUA_"+i).val(v);
                    });
                }
                if(j.rows.UC != null){
                    $.each( j.rows.UC[0], function(i,v){
                        $("#inAcUC_"+i).val(v);
                    });
                }
                $("#isUpdorIns").val("isUpdAdmin");
                $("#idAccUsr").val(val[1]);
                $("#inAcCompName").val( j.rows.U[0].company_name );
                if(!oUsr.isAdmin){
                    $("#inAcU_id_u_type").prop("disabled",true);
                }
            }
        );
    });

    /*Delete Location AdmOnly*/
    $("#btnAdmOnlyLocsDel").on("click", function(){
        var val = getDTselected(tblAdOnlyLocs);
        bootbox.confirm("Are you sure to delete this Location permanently "+val[2]+": "+val[3]+" "+val[4], function(result) {
                if(result == true){
                    $.post("bk/sSens.php", {"func":"delLoc","id":val[1]}, function(json){
                        //bootbox.alert("Location is deleted");
                        tblAdOnlyLocs.fnDraw();
                        getLocationsSens();
                    });
                }
            }); 
    });

    /*Editar Location Adm*/
    $("#btnAdmOnlyLocsEdt").on("click", function(){
        var idAcc = getDTselected(tblAdOnlyLocs)[0];
        var idLoc = getDTselected(tblAdOnlyLocs)[1];
        $("#btnSensDo").hide();
        $("#btnSensUpd").show();
        
        $("#aTabLocName").html( getDTselected(tblAdOnlyLocs)[2] );
        $("#SensACCOUNT").val( getDTselected(tblAdOnlyLocs)[0] );
        $("#LocCODE").val( getDTselected(tblAdOnlyLocs)[1] );
        $("#InpSenTL_id_location").val( idLoc );
        $("#InpSenTL_location_name").val( getDTselected(tblAdOnlyLocs)[2] );
        $.post("bk/funs.php",
            {  "pG" : "gLocAllInfo",
                "idA": idAcc,
                "idL": idLoc
            },function(j){
                $.each( j.rows.L[0], function(i,v){
                    $("#inAcU_"+i).val(v);
                    if( $("#inAcU_"+i).attr("type") == "checkbox" && v == "1"){
                        $("#InpSenTL_"+i).prop('checked', true);
                    }
                });
                if(j.rows.LA != null){
                    $.each( j.rows.LA[0], function(i,v){
                        $("#InpSenTLA_"+i).val(v);
                    });
                }
                if(j.rows.LC != null){
                    $.each( j.rows.LC[0], function(i,v){
                        $("#InpSenTSI"+i).val(v);
                    });
                }$("#modSensDial").modal("show");
            }
        );
    });

        
    $("#btnSaveNewAcc").on("click", function(){
        if(isAddOrEdi == "add"){
            $.post("bk/sS.php",{sT:"sAcc",nam:$("#InAccName").val()},
                function(j){
                    $('#modAccountDiv').modal('hide');
                    tblAdAcc.fnDraw();
                });
        }else if(isAddOrEdi == "edi"){
            var val = getDTselected(tblAdAcc);
            $.post("bk/sS.php",{sT:"eAcc",nam:$("#InAccName").val(),acc:val[0]},
                function(j){
                    $('#modAccountDiv').modal('hide');
                    tblAdAcc.fnDraw();
                });
        }
        
    });
    
    $("#btnAccNew").on("click", function(){
        isAddOrEdi = "add";
        $('#divAccRelSubAcc').fadeOut();
    });
    
    $("#btnAccDel").on("click", function(){
        var val = getDTselected(tblAdAcc);
        if( val.length > 0){
            bootbox.confirm("Are you sure to delete ( "+val[0]+" "+val[1]+") account?", function(result){
                if(result == true){
                    $.post("bk/sS.php",{sT:"dAcc",acc: val[0]},
                        function(j){
                            if(j.res="1"){
                                bootbox.alert("Account is deleted");
                            }else if(j.res="1"){
                            }
                            tblAdAcc.fnDraw();
                        });
                }
            });
            
        }else{
            bootbox.alert("Please select one account");
        }
        $('#divAccRelSubAcc').fadeOut();
    });
    
    $("#btnAccEdi").on("click", function(){
        var val = getDTselected(tblAdAcc);
        if( val.length > 0){
            isAddOrEdi = "edi";
            $("#InAccName").val(val[1]);
            $('#modAccountDiv').modal('show');
        }else{
            bootbox.alert("Please select one account");
        }
        $('#divAccRelSubAcc').fadeOut();
    });
    
    
    
    $("#btnSubAccAddNew").on("click",function(){
        var val = getDTselected(tblAdAcc);
        $("#btnSensDo").show();
        $("#btnSensUpd").hide();
        $("#InpSenTL_id_account").val( "Location Sensor" );
        if( val.length > 0){
            $("#InpSenTL_id_account").val(val[0]);
        }else{
            $("#InpSenTL_id_account").val("");
        }
    });
    
    
    /* #################################################################
     * Sub Accounts Btns
     * #################################################################
     * */
     $("#btnAccEdi").on("click", function(){
        var val = getDTselected(tblAdLocAcc);
        if( val.length > 0){
            isSubAddOrEdi = "edi";
            //$("#InAccName").val(val[1]);
            //$('#modAccountDiv').modal('show');
        }else{
            bootbox.alert("Please select one sub-account");
        }
    });
    //delete location
    $("#btnAdmLocDel").on("click", function(){
        var val = getDTselected(tblAdLocAcc);
        if( val.length > 0){
            bootbox.confirm("Are you want to delete this Location: "+val[0]+" "+val[1]+"?", function(result) {
                if(result == true){
                    $.post("bk/sSens.php", {"func":"del","id":val[0]}, function(json){
                        bootbox.alert("Location is deleted");
                        tblAdLocAcc.fnDraw();
                    });
                }
            }); 
            
        }else{
            bootbox.alert("Please select one sub-account");
        }
    });


     $("#btnAdmLocEdt").on("click", function(){
        var locAcc =  getDTselected(tblAdLocAcc);
        if( locAcc.length == 0 ){
            bootbox.alert("Please select one location");
            return false;
        }

        var idAcc = getDTselected(tblAdAcc)[0];
        var idLoc = getDTselected(tblAdLocAcc)[0];
        $("#btnSensDo").hide();
        $("#btnSensUpd").show();
        //$("#modSensTitle").val( getDTselected(tblAdLocAcc)[1] );
        $("#aTabLocName").html( getDTselected(tblAdAcc)[1] );
        $("#SensACCOUNT").val( getDTselected(tblAdAcc)[0] );

        $("#LocCODE").val( getDTselected(tblAdAcc)[0] );

        $("#InpSenTL_id_location").val( idLoc );
        $("#InpSenTL_location_name").val( getDTselected(tblAdLocAcc)[1] );
        $.post("bk/funs.php",
            {  "pG" : "gLocAllInfo",
                "idA": idAcc,
                "idL": idLoc
            },function(j){
                $.each( j.rows.L[0], function(i,v){
                    $("#inAcU_"+i).val(v);
                    if( $("#inAcU_"+i).attr("type") == "checkbox" && v == "1"){
                        $("#InpSenTL_"+i).prop('checked', true);
                    }
                });
                if(j.rows.LA != null){
                    $.each( j.rows.LA[0], function(i,v){
                        $("#InpSenTLA_"+i).val(v);
                    });
                }
                if(j.rows.LC != null){
                    $.each( j.rows.LC[0], function(i,v){
                        $("#InpSenTSI"+i).val(v);
                    });
                }
                //$("#isUpdorIns").val("isUpd");
                //$("#idAccUsr").val(val[0]);
            }
        );
    });


    $("#modAccount").on("click", function(){
        
    });

    $('#modAccount').on('hidden.bs.modal', function() {
        if(oELG.bInsNewUserbyADM == true){
            $("#modAdmAccounts").modal("show");
        }
    });

    $('#modSensDial').on('hidden.bs.modal', function() {
        if(oELG.bInsNewLocbyADM == true){
            $("#modAdmAccounts").modal("show");
        }
    });


    /* #################################################################
     * Add New User
     * #################################################################
     * */
    $("#btnSaveUserAccount").on("click",function(){
        var inp = $("#modAccount").find('[id^="inAcU"]:not(:checkbox)');
        var urlP = "";
        $.each(inp, function(i,v){
            urlP += $(v).attr("id")+"="+$(v).val().trim()+"&";
        });

        inp = $("#modAccount").find('[id^="inAcU"]:checkbox');
        $.each(inp, function(i,v){
             urlP += $(v).attr("id")+"=";
            if( $(v).is(":checked") ){
                urlP += "1&";
            }else {
                urlP += "0&";
            }
        });

        if( $("#isUpdorIns").val() == "isUpd" || $("#isUpdorIns").val() == "myAcc" || $("#isUpdorIns").val() == "isUpdAdmin"){
            urlP += "&func=upd&idU="+$("#idAccUsr").val();
        }else{
            urlP += "&func=ins";
        }
        
        $.post("bk/sUsr.php",urlP,function(json){
            //getLocationsSens();
            //console.log("resp:" + json);
            $('#modAccount').modal('hide');
            tblAdUsers.fnDraw();
            if( $("#isUpdorIns").val() != "myAcc" && $("#isUpdorIns").val() != "isUpdAdmin" ){
                $("#modAdmAccounts").modal("show");
            }
            if($("#isUpdorIns").val() == "isUpdAdmin" ){
                $("#modAdmOnlUsers").modal("show");
            }
        });
    });

    $("#btnAdmUsrDel").on("click", function(){
        var val = getDTselected(tblAdUsers);
        if( val.length > 0){
            bootbox.confirm("Are you want to delete the user: "+val[2]+" "+val[3]+"?", function(result) {
                if(result == true){
                    $.post("bk/sS.php",{sT:"dUs",iu: val[6]},
                        function(j){
                            if(j.res="1"){
                                bootbox.alert("User is deleted");
                            }else if(j.res="1"){
                            }
                            tblAdUsers.fnDraw();
                        });
                }
            }); 
            
        }else{
            bootbox.alert("Please select one user");
        }
    });

    /*
        EDIT USER
    */
    $("#btnAdmUsrEdt").on("click", function(){
        var val = getDTselected(tblAdUsers);

        if( val.length == 0 ){
            bootbox.alert("Please choose one User");
            return false;
        }
        addNewUser();
        oELG.bInsNewUserbyADM = false;
        var val = getDTselected(tblAdUsers)
        $.post("bk/funs.php",
            {  "pG" : "gUsrAllInfo",
                "id": val[6]
            },function(j){
                $.each( j.rows.U[0], function(i,v){
                    $("#inAcU_"+i).val(v);
                    if( $("#inAcU_"+i).attr("type") == "checkbox" && v == "1"){
                        $("#inAcU_"+i).prop('checked', true);
                    }
                });
                if(j.rows.UA != null){
                    $.each( j.rows.UA[0], function(i,v){
                        $("#inAcUA_"+i).val(v);
                    });
                }
                if(j.rows.UC != null){
                    $.each( j.rows.UC[0], function(i,v){
                        $("#inAcUC_"+i).val(v);
                    });
                }
                $("#isUpdorIns").val("isUpd");
                $("#idAccUsr").val(val[0]);
                $("#inAcCompName").val( j.rows.U[0].company_name );
                if(!oUsr.isAdmin){
                    $("#inAcU_id_u_type").prop("disabled",true);
                }
            }
        );
    });

    /*
    Account Edit dialog close
    */
    $('#modAccount').on('hidden.bs.modal', function() {
        if( $("#isUpdorIns").val() == "isUpd"){
            $("#modAdmAccounts").modal("show");
        }
    });

});



function addNewUser(){
    $("#inAcU_id_account").val();
    oELG.bInsNewUserbyADM = true;
    //$("#UserGrpPhoto").hide();
    var inp = $("#modAccount").find('[id^="inAcU"]');
    $.each(inp, function(i,v){
        $(v).val("");
        $(v).removeAttr("checked");
    });

    var val = getDTselected(tblAdAcc);
    $("#inAcU_id_account").val(val[0]);
    $("#isUpdorIns").val("isNew");
    $("#inAcCompName").val(oUsr.accName);

    $("#inAcU_id_u_type").val(oUsr.ty);

    $("#modAdmAccounts").modal("hide");
    $("#modAccount").modal("show");
}
