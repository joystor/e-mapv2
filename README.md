# ELGMAP

Project to show in map all sensor data recibed by API and report warnings by SMS with twilio

## Back end
 - PHP
 - Twilio API
 - PHPMailer

## DB
 - MySQL

## Front end
 - jquery
 - Bootstrap
 - Openlayers
 - Google Map geolocation API
 - Chart.js


### Example Demo

 http://dev.joystor.xyz/ELGMAPv2
 - user: elgadmin
 - pwd: elgadmin
