<?php
session_start();
if(!isset($_SESSION['uName']) || is_null( $_SESSION['uName'] || $_SESSION['uName'] == "" )){
    header( 'Location: index.php' );
}
include_once "fr/html-head.php";
?>

	<link rel="stylesheet" href="js/libs/extendbootstrap/css/bootstrap-timepicker.css">
	<link rel="stylesheet" href="js/libs/extendbootstrap/css/bootstro.css">
	<link rel="stylesheet" href="js/libs/extendbootstrap/css/datepicker.css">
	<link rel="stylesheet" href="js/libs/extendbootstrap/css/bootstrap-timepicker.css" />
	
	<link rel="stylesheet" href="js/libs/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.min.css" />

<style>
body {
	background-color:#E0E0E0;
}
.divContOpt {
	background-color: rgb(202, 204, 214);
	border-radius: 20px;
	border-color: #222;
	border-width: 2px;
	border-style: dashed;
}
</style>


<div class="container">

	<h1 style="text-align: center;">ELG Sensor Test GUI</h1>

	<hr class="divider"/>

	<div class="row">
	    <div class="col-sm-3 col-md-3">
	    	<input class="form-control" id="inpM2acc" placeholder="Account number" />
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<input class="form-control" id="inpM2loc" placeholder="Location name" />
	    </div>
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<div class="input-group">
				<input type="text" class="form-control" placeholder="Date" id="inSTdate">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
				</span>
			</div>
			<div class="input-group bootstrap-timepicker">
				<input type="text" class="form-control" placeholder="Time" id="inSTtime">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-time form-control-feedback"></span>
				</span>
			</div>
	    </div>
	</div>


	<div class="row">
	    <div class="col-sm-3 col-md-3">
	    	<input class="form-control" placeholder="Mac address" id="inMMac"/>
	    </div>
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<input class="form-control" id="inpMlat" placeholder="lat" />
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<input class="form-control" id="inpMlon" placeholder="long" />
	    </div>
	</div>

	<hr class="divider"/>

	<div class="row">
		<div class="col-sm-4 divContOpt">
	    	<h3>System status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="OKO" data-gr="SS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="NOT" data-gr="SS">Notification</a></li>
				<li class="selectListOpt"><a href="#" data-code="WAR" data-gr="SS">Warning</a></li>
				<li class="selectListOpt"><a href="#" data-code="ALE" data-gr="SS">Alert</a></li>
			</ul>
	    </div>

	    <div class="col-sm-4 divContOpt">
	    	<h3>Premise status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="PrS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="2" data-gr="PrS">Verifying Power Status "outage"</a></li>
				<li class="selectListOpt"><a href="#" data-code="3" data-gr="PrS">Verifying Sensor</a></li>
				<li class="selectListOpt"><a href="#" data-code="4" data-gr="PrS">Verifying Switch Gear</a></li>
				<li class="selectListOpt"><a href="#" data-code="5" data-gr="PrS">Verifying Generator</a></li>
			</ul>
	    </div>

	    <div class="col-sm-4 divContOpt">
	    	<h3>Power status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="PoS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="2" data-gr="PoS">Power Outage</a></li>
				<li class="selectListOpt"><a href="#" data-code="3" data-gr="PoS">Start Date</a></li>
				<li class="selectListOpt"><a href="#" data-code="4" data-gr="PoS">Time</a></li>
				<li class="selectListOpt"><a href="#" data-code="5" data-gr="PoS">Outage Duration</a></li>
			</ul>
	   	</div>
	</div>

	<div class="row from">
	    <div class="col-sm-4 divContOpt">
	    	<h3>Sensor status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">On Battery</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Battery State - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Battery State - Low</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Battery Condition - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Battery Condition - Low</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Com Signal - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Com Signal - Low</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Temp - Low</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Temp - High</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Tamper - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Tamper - open</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Heater - On</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Heater - Off</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Fan - On</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="SeS">Fan - Off</a></li>
			</ul>
	    </div>



	    <div class="col-sm-4 divContOpt">
	    	<h3>Transfer Switch Gear Status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Switch State Off</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Switch State On</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Switch State On Date Time</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Switch State Off Date Time</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Voltage Rotation - Clock</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Voltage Rotation - Counter Clock</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Util Voltage A  Voltage</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Util Voltage B  Voltage</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Util Voltage C  Voltage</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Current A  Current</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Current B  Current</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Premise Current C  Current</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Gen Side Voltage A  Voltage</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Gen Side Voltage B  Voltage</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="TSG">Gen Side Voltage C  Voltage</a></li>
			</ul>
	    </div>


	    <div class="col-sm-4 divContOpt">
	    	<h3>Generator Status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen OK Gen Status</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Status - On</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Status - Off</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen State Date Time - On Date Time</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen State Date Time - Off Date Time</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen On Duration</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Voltage - A  Voltage</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Voltage - B  Voltage</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Voltage - C  Voltage</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Rotation - Clock</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Rotation - Counter Clock</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Fuel Level - 1/8</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Fuel Level - 1/4</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Fuel Level - ½</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Fuel Level - ¾</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Fuel Level - Full</a></li>
				<hr class="divider"/>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Temp - Normal</a></li>
				<li class="selectListOpt"><a href="#" data-code="1" data-gr="GeS">Gen Temp - Hot</a></li>
			</ul>
	    </div>

	</div>

	<hr class="divider"/>

	<div class="row">
	    <div class="col-sm-12 col-md-12">
			<div class="input-group">
				<span class="input-group-addon">#</span>
				<input type="text" class="form-control" placeholder="M2M STRING ATOMATIC GENERATOR" readonly>
			</div>
	    </div>
	</div>
	<hr class="divider"/>

	<div class="row">
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<button type="button" class="btn btn-warning btn-lg btn-block">Clear</button>
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<button type="button" class="btn btn-primary btn-lg btn-block">Send</button>
	    </div>
	</div>

</div>


<?php

include_once "fr/myAccount.php";

include_once "fr/html-foot.php";
?>
<script type="text/javascript" src="js/libs/extendbootstrap/js/bootstro.js"></script>
<script type="text/javascript" src="js/libs/extendbootstrap/js/date-time/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/libs/extendbootstrap/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="js/m2m.js"></script>


<?php
include_once "fr/html-foot-close.php";
?>