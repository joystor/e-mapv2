<?php
	include_once('cDBCon.php');
	include_once('cfgVars.php');
	global $DB;
	global $VR;
	
	$arr = array();
	
	if(isset($_REQUEST['f1'])){
	}else{
		$arr['err'] = "notF1";
	}
	
	switch ($_REQUEST['f1']){
		case "gtSens":
			$WHR = "";
			$WHR2 = "";
			$WHR3 = "";
			if($_SESSION["isAdmin"] == false){
				$WHR = " WHERE L.id_account='".$_SESSION['IDACCOUNT']."' ";
				$WHR2 = " WHERE L.account_number in (select account_number from " . $VR->tbl_prefix . "location  ".$WHR." )";
				$WHR3 = " WHERE account_number in (select account_number from " . $VR->tbl_prefix . "location  ".$WHR." )";
			}

			$qL  = "select L.*, A.name as accname, M.file_image from " . $VR->tbl_prefix . "location L left join ".$VR->tbl_prefix."accounts A on L.id_account=A.id_account left join tbl_elg2_map_flags_types M ON L.id_icon_type=M.flag_id $WHR order by account_number;";
			$qLA = "select * from " . $VR->tbl_prefix . "location_addres $WHR3 order by account_number;";
			$qLC = "select * from " . $VR->tbl_prefix . "location_contact_info $WHR3 order by account_number;";
			$qSi = "select * from " . $VR->tbl_prefix . "sensor_info $WHR3 order by account_number;";

			$rsL  = $DB->query( $qL );
			$rsLA = $DB->query( $qLA );
			$rsLC = $DB->query( $qLC );
			$rsSi = $DB->query( $qSi );
			$arD = array();
			$arD['L'] = $DB->RS2ARR($rsL);
			$arD['LA'] = $DB->RS2ARR($rsLA);
			$arD['LC'] = $DB->RS2ARR($rsLC);
			$arD['SI'] = $DB->RS2ARR($rsSi);
			
			$feats = array();
			foreach( $arD['L'] as $loc){
			    $feat = array();
			    $feat['id'] = $loc['account_number'];
			    $feat['L'] = $loc;
			    foreach( $arD['LA'] as $l){
			        if($l['account_number'] == $feat['id'] ){
			            $feat['LA'] = $l;
			            break;
			        }
			    }
			    foreach( $arD['LC'] as $l){
			        if($l['account_number'] == $feat['id'] ){
			            $feat['LC'] = $l;
			            break;
			        }
			    }
			    foreach( $arD['SI'] as $l){
			        if($l['account_number'] == $feat['id'] ){
			            $feat['SI'] = $l;
			            break;
			        }
			    }
			    array_push($feats, $feat);
			}
			
			$arr['rows'] = $feats;
            $arr["q"] = $qLA;
			break;
		case "gtNotif":
			$q = "select * from " .$VR->tbl_prefix."notification"; //".$_SESSION['uID']."
            if($_SESSION["isAdmin"] == false){
                $q .= " WHERE id_user=".$_SESSION['uID'];
            }
            $q .= " order by creation_date desc";
			$rs = $DB->query( $q );
			$arr['rows'] = $DB->RS2ARR($rs);
			break;
		case "gtUcfg":
			$q = "select * from " .$VR->tbl_prefix."user_map".
						" WHERE id_user=0"; //.$_SESSION['uID'];
			$rs = $DB->query( $q );
			$arr['rows'] = $DB->RS2ARR($rs);
			break;
	}
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');
	echo json_encode( $arr ); 
?>
