<?php
session_start();
if(!isset($_SESSION['uName']) || is_null( $_SESSION['uName'] || $_SESSION['uName'] == "" )){
    header( 'Location: index.php' );
}
include_once "fr/html-head.php";
?>

	<link rel="stylesheet" href="js/libs/extendbootstrap/css/bootstrap-timepicker.css">
	<link rel="stylesheet" href="js/libs/extendbootstrap/css/bootstro.css">
	<link rel="stylesheet" href="js/libs/extendbootstrap/css/datepicker.css">
	<link rel="stylesheet" href="js/libs/extendbootstrap/css/bootstrap-timepicker.css" />
	
	<link rel="stylesheet" href="js/libs/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.min.css" />

<style>
body {
	background-color:#E0E0E0;
}
.divContOpt {
	background-color: rgb(202, 204, 214);
	border-radius: 20px;
	border-color: #222;
	border-width: 2px;
	border-style: dashed;
}
</style>


<div class="container">

	<h1 style="text-align: center;">ELG Sensor Test GUI</h1>

	<hr class="divider"/>

	<div class="row">
	    <div class="col-sm-3 col-md-3">
	    	<!--input class="form-control" id="inpM2acc" placeholder="Account number" /-->
	    	<select class="form-control" id="inpM2acc"></select>
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<!--input class="form-control" id="inpM2loc" placeholder="Location name" /-->
	    	<select class="form-control" id="inpM2loc"></select>
	    </div>
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<div class="input-group">
				<input type="text" class="form-control" placeholder="Date" id="inSTdate">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
				</span>
			</div>
			<div class="input-group bootstrap-timepicker">
				<input type="text" class="form-control" placeholder="Time" id="inSTtime">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-time form-control-feedback"></span>
				</span>
			</div>
	    </div>
	</div>


	<div class="row">
	    <div class="col-sm-3 col-md-3">
	    	<!--input class="form-control" placeholder="Mac address" id="inMMac"/-->
	    </div>
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<input class="form-control" id="inpMlat" placeholder="lat" />
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<input class="form-control" id="inpMlon" placeholder="long" />
	    </div>
	</div>

	<hr class="divider"/>
<div id="divContMenus">
	<div class="row">
		<div class="col-sm-4 divContOpt">
	    	<h3>System status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SS1" data-gr="SS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="SS2" data-gr="SS">Notification</a></li>
				<li class="selectListOpt"><a href="#" data-code="SS3" data-gr="SS">Warning</a></li>
				<li class="selectListOpt"><a href="#" data-code="SS4" data-gr="SS">Alert</a></li>
			</ul>
	    </div>

	    <div class="col-sm-4 divContOpt">
	    	<h3>Premise status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="PS1" data-gr="PrS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="PS2" data-gr="PrS">Verifying Power Status "outage"</a></li>
				<li class="selectListOpt"><a href="#" data-code="PS3" data-gr="PrS">Verifying Sensor</a></li>
				<li class="selectListOpt"><a href="#" data-code="PS4" data-gr="PrS">Verifying Switch Gear</a></li>
				<li class="selectListOpt"><a href="#" data-code="PS5" data-gr="PrS">Verifying Generator</a></li>
			</ul>
	    </div>

	    <div class="col-sm-4 divContOpt">
	    	<h3>Power status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="PW1" data-gr="PoS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="PW2" data-gr="PoS">Power Outage</a></li>
				<li class="selectListOpt"><a href="#" data-code="PW3" data-gr="PoS">Start Date</a></li>
				<li class="selectListOpt"><a href="#" data-code="PW4" data-gr="PoS">Time</a></li>
				<li class="selectListOpt"><a href="#" data-code="PW5" data-gr="PoS">Outage Duration</a></li>
			</ul>
	   	</div>
	</div>

	<div class="row">
	    <div class="col-sm-4 divContOpt">
	    	<h3>Sensor status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE1" data-gr="SeS">Ok</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE2" data-gr="SeS">On Battery</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE3" data-gr="SeS">Battery State - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE4" data-gr="SeS">Battery State - Low</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE5" data-gr="SeS">Battery Condition - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE6" data-gr="SeS">Battery Condition - Low</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE7" data-gr="SeS">Com Signal - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE8" data-gr="SeS">Com Signal - Low</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE9" data-gr="SeS">Temp - Low</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE10" data-gr="SeS">Temp - High</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE11" data-gr="SeS">Tamper - Good</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE12" data-gr="SeS">Tamper - open</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE13" data-gr="SeS">Heater - On</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE14" data-gr="SeS">Heater - Off</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="SE15" data-gr="SeS">Fan - On</a></li>
				<li class="selectListOpt"><a href="#" data-code="SE16" data-gr="SeS">Fan - Off</a></li>
			</ul>
	    </div>

	    <div class="col-sm-4 divContOpt">
	    	<h3>Transfer Switch Gear Status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="TS1" data-gr="TSG">Switch State Off</a></li>
				<li class="selectListOpt"><a href="#" data-code="TS2" data-gr="TSG">Switch State On</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="TS3" data-gr="TSG">Switch State On Date Time</a></li>
				<li class="selectListOpt"><a href="#" data-code="TS4" data-gr="TSG">Switch State Off Date Time</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="TS5" data-gr="TSG">Premise Voltage Rotation - Clock</a></li>
				<li class="selectListOpt"><a href="#" data-code="TS6" data-gr="TSG">Premise Voltage Rotation - Counter Clock</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS7" data-gr="TSG">Premise Util Voltage A  Voltage</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS8" data-gr="TSG">Premise Util Voltage B  Voltage</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS9" data-gr="TSG">Premise Util Voltage C  Voltage</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS10" data-gr="TSG">Premise Current A  Current</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS11" data-gr="TSG">Premise Current B  Current</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS12" data-gr="TSG">Premise Current C  Current</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS13" data-gr="TSG">Gen Side Voltage A  Voltage</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS14" data-gr="TSG">Gen Side Voltage B  Voltage</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="TS15" data-gr="TSG">Gen Side Voltage C  Voltage</a></li>
			</ul>
	    </div>

	    <div class="col-sm-4 divContOpt">
	    	<h3>Generator Status</h3>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="GS1" data-gr="GeS">Gen OK Gen Status</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS2" data-gr="GeS">Gen Status - On</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS3" data-gr="GeS">Gen Status - Off</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="GS4" data-gr="GeS">Gen State Date Time - On Date Time</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS5" data-gr="GeS">Gen State Date Time - Off Date Time</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="GS6" data-gr="GeS">Gen On Duration</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="GS7" data-gr="GeS">Gen Voltage - A  Voltage</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="GS8" data-gr="GeS">Gen Voltage - B  Voltage</a></li>
				<li class="selectListOpt slcAddTXVal"><a href="#" data-code="GS9" data-gr="GeS">Gen Voltage - C  Voltage</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="GS10" data-gr="GeS">Gen Rotation - Clock</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS11" data-gr="GeS">Gen Rotation - Counter Clock</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="GS12" data-gr="GeS">Gen Fuel Level - 1/8</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS13" data-gr="GeS">Gen Fuel Level - 1/4</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS14" data-gr="GeS">Gen Fuel Level - ½</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS15" data-gr="GeS">Gen Fuel Level - ¾</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS16" data-gr="GeS">Gen Fuel Level - Full</a></li>
			</ul>
			<hr class="divider"/>
			<ul class="nav nav-pills nav-stacked">
				<li class="selectListOpt"><a href="#" data-code="GS17" data-gr="GeS">Gen Temp - Normal</a></li>
				<li class="selectListOpt"><a href="#" data-code="GS18" data-gr="GeS">Gen Temp - Hot</a></li>
			</ul>
	    </div>

	</div>
</div>
	<hr class="divider"/>

	<div class="row">
	    <div class="col-sm-12 col-md-12">
			<div class="input-group">
				<span class="input-group-addon">#</span>
				<input type="text" class="form-control" id="mesGen" placeholder="M2M STRING ATOMATIC GENERATOR" readonly>
			</div>
	    </div>
	</div>
	<hr class="divider"/>

	<div class="row">
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<button type="button" class="btn btn-warning btn-lg btn-block"  id="btnM2MClear">Clear</button>
	    </div>
	    <div class="col-sm-3 col-md-3">
	    	<button type="button" class="btn btn-primary btn-lg btn-block" id="btnM2MSend">Send</button>
	    </div>
	</div>

</div>


<?php

include_once "fr/myAccount.php";

include_once "fr/html-foot.php";
?>
<script type="text/javascript" src="js/libs/extendbootstrap/js/bootstro.js"></script>
<script type="text/javascript" src="js/libs/extendbootstrap/js/date-time/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/libs/extendbootstrap/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="js/m2m.js"></script>


<?php
include_once "fr/html-foot-close.php";
?>